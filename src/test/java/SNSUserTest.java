import app.domain.model.SNSUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * The type Sns user test.
 */
public class SNSUserTest {

    private SNSUser snsUser;

    /**
     * The Exception rule.
     */
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * Set up.
     */
    @Before
    public void setUp(){
        this.snsUser = new SNSUser("João Alberto", "Male", "10/12/2003", "Rua do Verdinho Gaia", "999999999", "joaoalberto@gmail.com", "222222222", "11111111");
    }

    //============================================================================================= name

    /**
     * Check sns user name blank.
     */
    @Test
    public void checkSNSUserNameBlank (){
        System.out.println("Checking if SNS user name is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Name cannot be blank.");
        snsUser.checkName("");
    }

    //============================================================================================= sex


    /**
     * Check sns user sex format.
     */
    @Test
    public void checkSNSUserSexFormat(){
        System.out.println("Checking if SNS user sex has characters not allowed.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Invalid sex format.");
        snsUser.checkSex("11");
    }

    //============================================================================================= birth date

    /**
     * Check sns user birth date blank.
     */
    @Test
    public void checkSNSUserBirthDateBlank () {
        System.out.println("Checking if SNS user birth date is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Birth date cannot be blank.");
        snsUser.checkBirthDate("");
    }

    /**
     * Check sns user birth date format.
     */
    @Test
    public void checkSNSUserBirthDateFormat () {
        System.out.println("Checking if SNS user birth date has an invalid format");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Invalid birth date format.");
        snsUser.checkBirthDate("10/13/2007");
    }

    //============================================================================================= address

    /**
     * Check sns user address blank.
     */
    @Test
    public void checkSNSUserAddressBlank () {
        System.out.println("Checking if SNS user address is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Address cannot be blank.");
        snsUser.checkAddress("");
    }

    //============================================================================================= phone number

    /**
     * Check sns user phone number blank.
     */
    @Test
    public void checkSNSUserPhoneNumberBlank () {
        System.out.println("Checking if SNS user phone number is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Phone number cannot be blank.");
        snsUser.checkPhoneNumber("");
    }

    /**
     * Check sns user phone number components.
     */
    @Test
    public void checkSNSUserPhoneNumberComponents () {
        System.out.println("Checking if SNS user phone number is valid.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Phone number isn't valid.");
        snsUser.checkPhoneNumber("1234567890");
    }

    /**
     * Check sns user phone number components 2.
     */
    @Test
    public void checkSNSUserPhoneNumberComponents2 () {
        System.out.println("Checking if SNS user phone has invalid characters.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Phone number isn't valid.");
        snsUser.checkPhoneNumber("12345678d");
    }

    //============================================================================================= email

    /**
     * Check sns user email blank.
     */
    @Test
    public void checkSNSUserEmailBlank () {
        System.out.println("Checking if SNS user email is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Email cannot be blank.");
        snsUser.checkEmail("");
    }

    /**
     * Check sns user email components.
     */
    @Test
    public void checkSNSUserEmailComponents () {
        System.out.println("Checking if SNS user email is valid.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Email format is invalid.");
        snsUser.checkEmail("@aluno@gmail.com");
    }

    //============================================================================================= sns user number

    /**
     * Check sns user number blank.
     */
    @Test
    public void checkSNSUserNumberBlank () {
        System.out.println("Checking if SNS user number is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("SNS user number cannot be blank.");
        snsUser.checkSNSUserNumber("");
    }

    /**
     * Check sns user number components.
     */
    @Test
    public void checkSNSUserNumberComponents () {
        System.out.println("Checking if SNS user number is valid.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("SNS user number isn't valid.");
        snsUser.checkSNSUserNumber("1234567890");
    }

    /**
     * Check sns user number components 2.
     */
    @Test
    public void checkSNSUserNumberComponents2 () {
        System.out.println("Checking if SNS user number is valid.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("SNS user number isn't valid.");
        snsUser.checkSNSUserNumber("12345678d");
    }

    //============================================================================================= done

    /**
     * Check sns user citizen card number blank.
     */
    @Test
    public void checkSNSUserCitizenCardNumberBlank () {
        System.out.println("Checking if SNS user citizen card number is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Citizen card number cannot be blank.");
        snsUser.checkCitizenCardNumber("");
    }

    /**
     * Check sns user citizen card number components.
     */
    @Test
    public void checkSNSUserCitizenCardNumberComponents () {
        System.out.println("Checking if SNS user citizen card number is valid.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Citizen card number isn't valid.");
        snsUser.checkCitizenCardNumber("123456789");
    }

    /**
     * Check sns user citizen card number components 2.
     */
    @Test
    public void checkSNSUserCitizenCardNumberComponents2 () {
        System.out.println("Checking if SNS user citizen card number is valid.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Citizen card number isn't valid.");
        snsUser.checkCitizenCardNumber("1234567d");
    }

    //=============================================================================================

    /**
     * Get sns user name.
     */
    @Test
    public void getSNSUserName(){
        System.out.println("Checking if SNS user name retrieved is the same as the one entered.");
        String exp = "João Alberto";
        String actual = snsUser.getName();
        assertEquals(exp, actual);
    }

    /**
     * Get sns user sex.
     */
    @Test
    public void getSNSUserSex(){
        System.out.println("Checking if SNS user sex retrieved is the same as the one entered.");
        String exp = "Male";
        String actual = snsUser.getSex();
        assertEquals(exp, actual);
    }

    /**
     * Get sns user birth date.
     */
    @Test
    public void getSNSUserBirthDate(){
        System.out.println("Checking if SNS user birth date retrieved is the same as the one entered.");
        String exp = "10/12/2003";
        String actual = snsUser.getBirthDate();
        assertEquals(exp, actual);
    }

    /**
     * Get sns user address.
     */
    @Test
    public void getSNSUserAddress(){
        System.out.println("Checking if SNS user address retrieved is the same as the one entered.");
        String exp = "Rua do Verdinho Gaia";
        String actual = snsUser.getAddress();
        assertEquals(exp, actual);
    }

    /**
     * Get sns user phone number.
     */
    @Test
    public void getSNSUserPhoneNumber(){
        System.out.println("Checking if SNS user phone number retrieved is the same as the one entered.");
        String exp = "999999999";
        String actual = snsUser.getPhoneNumber();
        assertEquals(exp, actual);
    }

    /**
     * Get sns user email.
     */
    @Test
    public void getSNSUserEmail(){
        System.out.println("Checking if SNS user email retrieved is the same as the one entered.");
        String exp = "joaoalberto@gmail.com";
        String actual = snsUser.getEmail();
        assertEquals(exp, actual);
    }

    /**
     * Get sns user number.
     */
    @Test
    public void getSNSUserNumber(){
        System.out.println("Checking if SNS user number retrieved is the same as the one entered.");
        String exp = "222222222";
        String actual = snsUser.getSnsUserNumber();
        assertEquals(exp, actual);
    }

    /**
     * Get sns user citizen card number.
     */
    @Test
    public void getSNSUserCitizenCardNumber(){
        System.out.println("Checking if SNS user citizen card number retrieved is the same as the one entered.");
        String exp = "11111111";
        String actual = snsUser.getCitizenCardNumber();
        assertEquals(exp, actual);
    }
}