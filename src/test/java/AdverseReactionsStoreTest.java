import app.domain.model.AdverseReaction;
import app.domain.store.AdverseReactionsStore;
import app.domain.store.VaccineScheduleStore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AdverseReactionsStoreTest {

    private AdverseReactionsStore store = new AdverseReactionsStore();
    private AdverseReaction adverseReaction;
    private List<AdverseReaction> adverseReactionsList = new ArrayList<>();


    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();


    @Before
    public void setUp(){
        this.adverseReaction = new AdverseReaction("343434343", "Fever on the first 5 minutes.");
    }

    @Test
    void newAdverseReaction() {
        System.out.println("Creating an adverse reaction");
        this.adverseReaction = new AdverseReaction("343434343", "Fever on the first 5 minutes.");
        Assert.assertFalse(adverseReaction.equals(null));
    }

    @Test
    void addAdverseReaction() {
        System.out.println("Adding an adverse reaction");
        store.addAdverseReaction(adverseReaction);
        adverseReactionsList.contains(adverseReaction);
    }
}