import app.domain.model.VaccineSchedule;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.ParseException;
import java.util.InputMismatchException;

import static org.junit.Assert.*;

/**
 * The type Vaccine schedule test.
 */
public class VaccineScheduleTest {

    private VaccineSchedule vacSched;

    /**
     * The Exception rule.
     */
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * Set up.
     */
    @Before
  public void setUp(){
    this.vacSched = new VaccineSchedule("913853123","Porto","12/12/2022","18:03","Covid");
  }

    //=============================================================================================

    /**
     * Checksns user number blank.
     */
    @Test
  public void checksnsUserNumberBlank (){
      System.out.println("Checking if the SNS User Number is blank.");
      exceptionRule.expect(IllegalArgumentException.class);
      exceptionRule.expectMessage("Sns User Number cannot be blank.");
      vacSched.checksnsUserNumber("");

  }

    /**
     * Checksns user number size.
     */
    @Test
  public void checksnsUserNumberSize () {
      System.out.println("Checking if SNS User Number doesn't have the right size.");
      exceptionRule.expect(InputMismatchException.class);
      exceptionRule.expectMessage("Sns User Number must have 9 numbers.");
      vacSched.checksnsUserNumber("9889376540");
  }

    /**
     * Checksns user number components.
     */
    @Test
  public void checksnsUserNumberComponents (){
      System.out.println("Checking if SNS User Number has non numeric chars.");
      exceptionRule.expect(InputMismatchException.class);
      exceptionRule.expectMessage("Sns User Number cannot contain non chars.");
      vacSched.checksnsUserNumber("aaa1");
  }

  //=============================================================================================

    /**
     * Check vaccination center name blank.
     */
    @Test
    public void checkVaccinationCenterNameBlank (){
        System.out.println("Checking if Vaccination Center Name is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Vaccination Center Name cannot be blank.");
        vacSched.checkVaccinationCenterName("");
    }


    /**
     * Check vaccination center name components.
     */
    @Test
    public void checkVaccinationCenterNameComponents(){
        System.out.println("Checking if Vaccination Center Name has non alphabetic characters.");
        exceptionRule.expect(InputMismatchException.class);
        exceptionRule.expectMessage("Vaccination Center Name cannot contain numbers.");
        vacSched.checkVaccinationCenterName("11");
    }

  //=============================================================================================

    /**
     * Check date blank.
     *
     * @throws ParseException the parse exception
     */
    @Test
    public void checkDateBlank() throws ParseException {
        System.out.println("Checking if date is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Date cannot be blank.");
        vacSched.checkDate("");
    }

    /**
     * Check date components.
     *
     * @throws ParseException the parse exception
     */
    @Test
    public void checkDateComponents() throws ParseException {
        System.out.println("Checking if date is in the right format.");
        exceptionRule.expect(IllegalStateException.class);
        exceptionRule.expectMessage("Insert a date following the example: i.e., 18/12/2022");
        vacSched.checkDate("12-12-2020");
    }

    //=============================================================================================

    /**
     * Check time blank.
     */
    @Test
    public void checkTimeBlank(){
        System.out.println("Checking if time is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Time cannot be blank.");
        vacSched.checkTime("");
    }

    /**
     * Check time components.
     */
    @Test
    public void checkTimeComponents(){
        System.out.println("Checking if time is in the right format.");
        exceptionRule.expect(IllegalStateException.class);
        exceptionRule.expectMessage("Insert a time following the example: i.e., 18:03");
        vacSched.checkTime("27:30");
    }

    //=============================================================================================

    /**
     * Check vac type blank.
     */
    @Test
    public void checkVacTypeBlank(){
        System.out.println("Checking if Vaccine Type is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Vaccine Type cannot be blank");
        vacSched.checkVacType("");
    }

    //=============================================================================================

    /**
     * Get sns user number.
     */
    @Test
    public void getSNSUserNumber(){
        System.out.println("Checking if SNS User Number retrieved is the same as the one entered.");
        String exp = "913853123";
        String actual = vacSched.getSnsUserNumber();
        assertEquals(exp, actual);
    }

    /**
     * Gets vaccination center name.
     */
    @Test
    public void getVaccinationCenterName() {
        System.out.println("Checking if Vaccination Center Name retrieved is the same as the one entered.");
        String exp = "Porto";
        String actual = vacSched.getVaccinationCenterName();
        assertEquals(exp, actual);
    }

    /**
     * Gets date.
     */
    @Test
    public void getDate() {
        System.out.println("Checking if Date retrieved is the same as the one entered.");
        String exp = "12/12/2022";
        String actual = vacSched.getDate();
        assertEquals(exp, actual);
    }

    /**
     * Get time.
     */
    @Test
    public void getTime(){
        System.out.println("Checking if Time retrieved is the same as the one entered.");
        String exp = "18:03";
        String actual = vacSched.getTime();
        assertEquals(exp, actual);
    }

    /**
     * Gets vac type.
     */
    @Test
    public void getVacType() {
        System.out.println("Checking if Vaccine Type retrieved is the same as the one entered.");
        String exp = "Covid";
        String actual = vacSched.getVacType();
        assertEquals(exp, actual);
    }
}