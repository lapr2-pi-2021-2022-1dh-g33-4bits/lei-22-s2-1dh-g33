import app.domain.model.Employee;
import app.domain.store.EmployeeStore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * The type Employee store test.
 */
public class EmployeeStoreTest {

    private Employee employee;
    private EmployeeStore es = new EmployeeStore();
    private static int employeeNumber;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
    }

    /**
     * Create employee.
     */
    @Test
    public void createEmployee() {
        System.out.println("Creating a employee.");
        Employee employee1 = es.createEmployee("Nurse", "Mariana ", "rua flores 71", "91922536712", "miguel@gmail.com", "10-0000", "31070041", "4415030");
        Assert.assertTrue(!employee1.equals(null));
    }

    /**
     * Not save employee.
     */
    @Test
    public void notSaveEmployee() {
        System.out.println("Removing number from employee id counter.");
        employeeNumber = 10;
        int expected = 9;
        es.notSaveEmployee();
        Assert.assertEquals(expected, employeeNumber);
    }

    /**
     * Validate employee null.
     */
    @Test
    public void validateEmployeeNull() {
        System.out.println("Checking if employee is null");
        Employee employeeNull = null;
        Assert.assertFalse(es.validateEmployee(employeeNull));
    }

    /**
     * Validate employee already registed.
     */
    @Test
    public void validateEmployeeAlreadyRegisted(){
        System.out.println("Checking if the employee was already registered");
        es.saveEmployee(employee);
        Assert.assertFalse(es.validateEmployee(employee));
    }

    /**
     * Validate employee correct.
     */
    @Test
    public void validateEmployeeCorrect(){
        System.out.println("Checking if employee isn't null");
        Assert.assertTrue(es.validateEmployee(employee));
    }

    /**
     * Same employee phone number.
     */
    @Test
    public void sameEmployeePhoneNumber(){
        System.out.println("Checking if there is already a employee registered with the same phone number.");
        es.saveEmployee(employee);
        Employee employeePhoneNumber = new Employee("Nurse", "Mariana", "rua doutor nº72", "m23@gmail.com", "91922536712", "10-0000", "31070041", "123456789");
        Assert.assertFalse(es.sameEmployee(employeePhoneNumber));
    }

    /**
     * Same employee address.
     */
    @Test
    public void sameEmployeeAddress(){
        System.out.println("Checking if there is already a employee registered with the same address.");
        es.saveEmployee(employee);
        Employee employeeSameAddress = new Employee("Nurse", "Mariana", "rua doutor nº72", "m23@gmail.com", "91922536712", "10-0000", "31070041", "123456789");
        Assert.assertFalse(es.sameEmployee(employeeSameAddress));
    }

    /**
     * Same employee email adress.
     */
    @Test
    public void sameEmployeeEmailAdress(){
        System.out.println("Checking if there is already a employee registered with the same email address.");
        es.saveEmployee(employee);
        Employee employeeSameEmailAddress = new Employee("Nurse", "Mariana", "rua doutor nº72", "m23@gmail.com", "91922536712", "10-0000", "31070041", "123456789");
        Assert.assertFalse(es.sameEmployee(employeeSameEmailAddress));
    }

    /**
     * Same employee nif.
     */
    @Test
    public void sameEmployeeNIF(){
        System.out.println("Checking if there is already a employee registered with the same NIF.");
        es.saveEmployee(employee);
        Employee employeeSameEmailAddress = new Employee("Nurse", "Mariana", "rua doutor nº72", "m23@gmail.com", "91922536712", "10-0000", "31070041", "123456789");
        Assert.assertFalse(es.sameEmployee(employeeSameEmailAddress));
    }

    /**
     * Same id.
     */
    @Test
    public void sameID(){
        System.out.println("Checking if there is already a employee registered with the same ID.");
        es.saveEmployee(employee);
        Employee employeeSameEmailAddress = new Employee("Nurse", "Mariana", "rua doutor nº72", "m23@gmail.com", "91922536712", "10-0000", "31070041", "123456789");
        Assert.assertFalse(es.sameEmployee(employeeSameEmailAddress));
    }

    /**
     * Same employee correct.
     */
    @Test
    public void sameEmployeeCorrect(){
        System.out.println("Checking if there isn't already a employee registered with the same info.");
        es.saveEmployee(employee);
        Employee employee1 = new Employee("Nurse", "Mariana", "rua doutor nº72", "m23@gmail.com", "91922536712", "10-0000", "31070041", "123456789");;
        Assert.assertTrue(es.sameEmployee(employee1));
    }

    /**
     * Save employee null.
     */
    @Test
    public void saveEmployeeNull(){
        System.out.println("Checking if a null employee can be added to the system.");
        Employee employee1 = null;
        Assert.assertFalse(es.saveEmployee(employee1));
    }

    /**
     * Save employee already registed.
     */
    @Test
    public void saveEmployeeAlreadyRegisted(){
        System.out.println("Checking if a repeated employee can be added to the system.");
        es.saveEmployee(employee);
        Assert.assertFalse(es.saveEmployee(employee));
    }

    /**
     * Save employee correct.
     */
    @Test
    public void saveEmployeeCorrect(){
        System.out.println("Checking if a employee can be added to the system.");
        Assert.assertTrue(es.saveEmployee(employee));
    }
}