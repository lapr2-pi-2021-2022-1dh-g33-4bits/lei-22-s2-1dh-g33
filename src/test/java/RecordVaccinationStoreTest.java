import app.domain.model.RecordVaccine;
import app.domain.store.RecordVaccineStore;
import app.mappers.dto.SNSUserDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RecordVaccinationStoreTest {
    private RecordVaccineStore store = new RecordVaccineStore();
    private RecordVaccine recordVaccine;
    private SNSUserDTO snsUser;
    private final List<RecordVaccine> recordVaccineList = new ArrayList<>();

    /**
     * The Exception rule.
     */
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * Set up.
     */
    @Before
    public void setUp(){
        this.recordVaccine = new RecordVaccine("12/12/2022","COVID-19","Astra","12345-12","111412341");
        this.snsUser = new SNSUserDTO("mario", "M", "12/12/1990", "maia", "123456999","mari0maia1@gmail.com", "111412341", "11145678" );
    }

    //=============================================================================================

    @Test
    public void recordVaccine(){
        System.out.println("Recording a vaccination");
        Assert.assertFalse(recordVaccine.equals(null));
    }

    @Test
    public void addRecordVaccine(){
        System.out.println("Adding a Vaccine that was recorded");
        store.addRecordVaccine(recordVaccine, 0);
        recordVaccineList.contains(recordVaccine);
    }

    @Test
    public void turnBirthDateToAge(){
        System.out.println("Turning Birth Date to Age");
        int exp = 31;
        int actual = store.turnBirthDateToAge(snsUser.getBirthDate());
        Assert.assertEquals(exp,actual);
    }
}
