import app.domain.model.VaccinationData;
import app.domain.store.VaccinationDataStore;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

class VaccinationDataStoreTest {

    private VaccinationDataStore store = new VaccinationDataStore();

    private VaccinationData vaccinationData;

    private List<VaccinationData> vaccinationDataList = new ArrayList<>();
   @Rule
    public ExpectedException expectedRule = ExpectedException.none();

    @Test
    void getDataList() {

    }

    @Test
    void addDataSchedule() {
        System.out.println("Adding a new date schedule");
        store.addDataSchedule(2, "29/12/2022", "9:17","moderna");
        vaccinationDataList.contains(vaccinationData);
    }

    @Test
    void addDataArrival() {
        System.out.println("Adding a new date arrival");
        store.addDataSchedule(2, "17/12/2022", "9:27","pzizer");
        vaccinationDataList.contains(vaccinationData);
    }

    @Test
    void addDataVaccinationTime() {
        System.out.println("Adding a new data vaccination time");
        store.addDataVaccinationTime(2,"10:55","22/02/2022","primeira","21C16-05");
        vaccinationDataList.contains(vaccinationData);
    }

    @Test
    void addLeavingTime() {
        System.out.println("Adding a new data vaccination leaving time");
        store.addLeavingTime(2,"10:55");
        vaccinationDataList.contains(vaccinationData);
    }
    @Test
    void addImportData() {
        System.out.println("Adding a new import");
        store.addImportData(2,"17:22","2", "segunda", "21C16-04", "22/02/2022", "13:40", "moderna", "13:55", "15:01");
        vaccinationDataList.contains(vaccinationData);
    }

    @Test
    void getIndex() {
        System.out.println("Getting the index");
        store.getIndex(vaccinationDataList,"161593121");
    }

    @Test
    void add() throws ParseException {
        store.add("161593120","Spikevax","Primeira","21C16-05","5/30/2022", "8:00","5/30/2022" ,"8:24", "5/30/2022" ,"9:11", "5/30/2022" ,"9:43");
    }
}