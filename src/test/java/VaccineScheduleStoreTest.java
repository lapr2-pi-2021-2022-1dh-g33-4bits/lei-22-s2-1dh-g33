import app.domain.model.VaccineSchedule;
import app.domain.store.VaccineScheduleStore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Vaccine schedule store test.
 */
public class VaccineScheduleStoreTest {

    private VaccineScheduleStore store = new VaccineScheduleStore();
    private VaccineSchedule vacSched;
    private final List<VaccineSchedule> vaccineScheduleList = new ArrayList<>();

    /**
     * The Exception rule.
     */
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * Set up.
     */
    @Before
    public void setUp(){
        this.vacSched = new VaccineSchedule("912343999","Porto","12/12/2022","18:03","Covid");
    }

    //=============================================================================================

    /**
     * Schedule vaccine.
     */
    @Test
    public void scheduleVaccine(){
        System.out.println("Scheduling a vaccine");
        Assert.assertFalse(vacSched.equals(null));
    }

    /**
     * Add vaccine schedule.
     */
    @Test
    public void addVaccineSchedule(){
        System.out.println("Adding a Vaccine that was scheduled");
        store.addVaccineSchedule(vacSched);
        vaccineScheduleList.contains(vacSched);
    }

    /**
     * Validate schedule vaccine.
     */
    @Test
    public void validateScheduleVaccine(){
        store.addVaccineSchedule(vacSched);
        System.out.println("Checking if a vaccine was already scheduled");
        exceptionRule.expect(IllegalStateException.class);
        exceptionRule.expectMessage("Vaccine already scheduled");
        store.validateScheduleVaccine("912343999","Gaia","12/10/2022","18:03","Covid");
    }
}
