import app.domain.model.RecordVaccine;
import app.domain.model.VaccinationStatistic;
import app.domain.store.VaccinationStatisticsStore;
import app.mappers.dto.SNSUserDTO;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class VaccinationStatisticsStoreTest {

    private VaccinationStatisticsStore vaccinationStatisticsStore = new VaccinationStatisticsStore();
    private final List<VaccinationStatistic> vaccinationStatisticsList = new ArrayList<>();
    private VaccinationStatistic vaccinationStatistic;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();


    @Before
    public void setUp(){
        vaccinationStatistic = new VaccinationStatistic("12/12/2022", "5");
    }

    @Test
    void getVaccinationStatisticsList() {

    }

    @Test
    void newVaccinationStatistic() {
        System.out.println("Creating a new vaccination statistic.");
        vaccinationStatistic = vaccinationStatisticsStore.newVaccinationStatistic("12/12/2022", "10");
        Assert.assertFalse(vaccinationStatistic.equals(null));
    }

    @Test
    void addVaccinationStatisticsDay() {
        System.out.println("Adding a new vaccination statistics day.");
        vaccinationStatisticsStore.addVaccinationStatisticsDay(vaccinationStatistic);
        vaccinationStatisticsList.contains(vaccinationStatistic);
    }

    @Test
    void clearVaccinationStatistics() {
        System.out.println("Clearing the list:");

    }
}