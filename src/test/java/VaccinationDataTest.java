import app.domain.model.VaccinationData;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.text.ParseException;
import java.util.InputMismatchException;

import static org.junit.Assert.assertEquals;

public class VaccinationDataTest {

    private VaccinationData vaccinationData;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Before
    public void setUp() throws ParseException {
        vaccinationData = new VaccinationData("161593120","Spikevax","Primeira","21C16-05","5/30/2022", "8:00","5/30/2022", "8:24","5/30/2022", "9:11","5/30/2022", "9:43");}

    @Test
    void getArrivalDateAndTime() {
        System.out.println("Checking if Arrival Time and Date are the same as the one entered.");
        String exp = "6/12/2022";
        String exp2 = "17:20";
        String actual = vaccinationData.getArrivalDateAndTime();
        assertEquals(exp,exp2,actual);
    }

    @Test
    void getLeavindDateandTime() {
        System.out.println("Checking if leaving Time and Date are the same as the one entered.");
        String exp = "20/12/2022";
        String exp2 = "9:10";
        String actual = vaccinationData.getLeavindDateandTime();
        assertEquals(exp,exp2,actual);
    }

    @Test
    void getLeavingDate() {
        System.out.println("Checking if leaving Date is the same as the one entered.");
        String exp = "6/12/2022";
        String actual = vaccinationData.getLeavingDate();
        assertEquals(exp,actual);
    }

    @Test
    void getArrivalDate() {
        System.out.println("Checking if arrival Date is the same as the one entered.");
        String exp = "19/12/2022";
        String actual = vaccinationData.getArrivalDate();
        assertEquals(exp,actual);

    }

    @Test
    void getSnsUserNumber() {
        System.out.println("Checking if Sns User Number is the same as the one entered.");
        String exp = "121212121";
        String actual = vaccinationData.getSnsUserNumber();
        assertEquals(exp,actual);
    }

    @Test
    void getVacineType() {
        System.out.println("Checking if vaccine type is the same as the one entered.");
        String exp = "portuguesa";
        String actual = vaccinationData.getVacineType();
        assertEquals(exp,actual);

    }
    @Test
    void getDose() {
        System.out.println("Checking if dose is the same as the one entered.");
        String exp = "quinta";
        String actual = vaccinationData.getDose();
        assertEquals(exp,actual);
    }
    @Test
    void getLoteNumber() {
        System.out.println("Checking if lote number is the same as the one entered.");
        String exp = "76C14-05";
        String actual = vaccinationData.getLoteNumber();
        assertEquals(exp,actual);
    }
    @Test
    void getScheduleDate() {
        System.out.println("Checking if schedule date is the same as the one entered.");
        String exp = "10/01/2020";
        String actual = vaccinationData.getScheduleDate();
        assertEquals(exp,actual);
    }


    @Test
    void getScheduleTime() {
        System.out.println("Checking if schedule time is the same as the one entered.");
        String exp = "18:20";
        String actual = vaccinationData.getScheduleTime();
        assertEquals(exp,actual);
    }

    @Test
    void getArrivalTime() {
        System.out.println("Checking if arrival time is the same as the one entered.");
        String exp = "14:43";
        String actual = vaccinationData.getArrivalTime();
        assertEquals(exp,actual);
    }

    @Test
    void getAdministrationDate() {
        System.out.println("Checking if administration date is the same as the one entered.");
        String exp = "18/03/2020";
        String actual = vaccinationData.getAdministrationDate();
        assertEquals(exp,actual);
    }
    @Test
    void getAdministrationTime() {
        System.out.println("Checking if administration time is the same as the one entered.");
        String exp = "16:27";
        String actual = vaccinationData.getAdministrationTime();
        assertEquals(exp,actual);
    }

    @Test
    void getLeavingTime() {
        System.out.println("Checking if leaving time is the same as the one entered.");
        String exp = "13:01";
        String actual = vaccinationData.getLeavingTime();
        assertEquals(exp,actual);
    }
    @Test
    void checkSNSUserNumber() {
        System.out.println("Checking if Sns User Number doesn't have the right size and the right format.");
        exceptionRule.expect(InputMismatchException.class);
        exceptionRule.expect(InputMismatchException.class);
        exceptionRule.expectMessage("Sns User Number cannot contain non chars.");
        vaccinationData.checkSNSUserNumber("222222");
    }

    @Test
    void checkIfSnsUserNumberExists() {
        System.out.println("Checking if Sns User Number doesn't exists");
        exceptionRule.expect(InputMismatchException.class);
        exceptionRule.expect(InputMismatchException.class);
        exceptionRule.expectMessage("Sns User Number cannot contain non chars.");
        vaccinationData.checkIfSnsUserNumberExists("22222222");
    }

    @Test
    void checkDateFormat() throws ParseException {
        System.out.println("Checking if Date Format doesn't have the right size and the right format.");
        exceptionRule.expect(InputMismatchException.class);
        exceptionRule.expect(InputMismatchException.class);
        vaccinationData.checkDateFormat("11.12/2002");
    }

    @Test
    void checkHourFormat() throws ParseException {
        System.out.println("Checking if Hour Format doesn't have the right size and the right format.");
        exceptionRule.expect(InputMismatchException.class);
        exceptionRule.expect(InputMismatchException.class);
        vaccinationData.checkDateFormat("12.4");
    }

    @Test
    void validateDose() {
        System.out.println("Checking if dose doesn't have the right size and the right format.");
        exceptionRule.expect(InputMismatchException.class);
        exceptionRule.expect(InputMismatchException.class);
        exceptionRule.expectMessage("Dose cannot contain non chars.");
        vaccinationData.validateDose("te112ceir");
    }

}
