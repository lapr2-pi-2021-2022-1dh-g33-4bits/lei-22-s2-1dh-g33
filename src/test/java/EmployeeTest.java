import app.domain.model.Employee;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * The type Employee test.
 */
public class EmployeeTest {

    /**
     * The Exception rule.
     */
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    private Employee employee;

    /**
     * Set up.
     */
    @Before
    public void setUp(){
        this.employee = new Employee("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check name blank.
     */
//=========================================================================================================================
    @Test
    public void checkNameBlank(){
        System.out.println("Checking if name is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Name cannot be blank.");
        employee.checkAttributes("Nurse", "", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check name size and components.
     */
    @Test
    public void checkNameSizeAndComponents(){
        System.out.println("Checking if name is too big and if it has numbers.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Name cannot have more than 35 chars nor contain numbers.");
        employee.checkAttributes("Nurse", "Mariana Almeida Da Costa Silva Pinto Ferreira Almeida 2", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check name size.
     */
    @Test
    public void checkNameSize(){
        System.out.println("Checking if name is too big.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Name cannot have more than 35 chars.");
        employee.checkAttributes("Nurse", "Mariana Almeida Da Costa Silva Pinto Ferreira Almeida", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check name components.
     */
    @Test
    public void checkNameComponents(){
        System.out.println("Checking if name has non alphabetic characters.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Name cannot contain numbers.");
        employee.checkAttributes("Nurse", "Mariana 1", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check name correct.
     */
    @Test
    public void checkNameCorrect(){
        System.out.println("Checking if name is correct.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    //=========================================================================================================================

    /**
     * Check address blank.
     */
    @Test
    public void checkAddressBlank(){
        System.out.println("Checking if address is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Address cannot be blank.");
        employee.checkAttributes("Nurse", "Mariana ", "", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check address size.
     */
    @Test
    public void checkAddressSize(){
        System.out.println("Checking if address is too big.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Address cannot have more than 35 chars.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores da esquina dali do lado onde tu nao ves e eu sim 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check address correct.
     */
    @Test
    public void checkAddressCorrect(){
        System.out.println("Checking if Address is correct.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    //=========================================================================================================================

    /**
     * Check phone number blank.
     */
    @Test
    public void checkPhoneNumberBlank(){
        System.out.println("Checking if phone number is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Phone number cannot be blank.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "", "10-0000", "31070041", "4415030");
    }

    /**
     * Check phone number size and components.
     */
    @Test
    public void checkPhoneNumberSizeAndComponents(){
        System.out.println("Checking if phone number doesn't have the right size and if it has non numeric chars.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Phone number must have 11 numbers and cannot contain non numeric chars.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536aaa", "10-0000", "31070041", "4415030");
    }

    /**
     * Check phone number size.
     */
    @Test
    public void checkPhoneNumberSize(){
        System.out.println("Checking if phone number doesn't have the right size.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Phone number must have 11 numbers.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "9192253671", "10-0000", "31070041", "4415030");
    }

    /**
     * Check phone number components.
     */
    @Test
    public void checkPhoneNumberComponents(){
        System.out.println("Checking if phone number has non numeric chars.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Phone number cannot contain non numeric chars.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "9192253671a", "10-0000", "31070041", "4415030");
    }

    /**
     * Check phone number correct.
     */
    @Test
    public void checkPhoneNumberCorrect(){
        System.out.println("Checking if phone number is correct.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    //=========================================================================================================================

    /**
     * Check email address blank.
     */
    @Test
    public void checkEmailAddressBlank(){
        System.out.println("Checking if email address is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Email cannot be blank.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check email address format invalid.
     */
    @Test
    public void checkEmailAddressFormatInvalid(){
        System.out.println("Checking if email address doesn't have a valid format.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmailcom", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check email address correct.
     */
    @Test
    public void checkEmailAddressCorrect(){
        System.out.println("Checking if email address is correct.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    //=========================================================================================================================

    /**
     * Check soc code blank.
     */
    @Test
    public void checkSocCodeBlank(){
        System.out.println("Checking if standard occupational classification code is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Standard occupational classification code cannot be blank.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "", "31070041", "4415030");
    }

    /**
     * Check soc code size format and components.
     */
    @Test
    public void checkSocCodeSizeFormatAndComponents(){
        System.out.println("Checking if standard occupational classification code doesn't have the right format, size and has non numeric chars.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Standard occupational classification code has a invalid format, cannot have non numeric chars and must have 7 chars");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "a0*00", "31070041", "4415030");
    }

    /**
     * Check soc code format and components.
     */
    @Test
    public void checkSocCodeFormatAndComponents(){
        System.out.println("Checking if standard occupational classification code doesn't have the right format and if has non numeric chars.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Standard occupational classification code has a invalid format and cannot have non numeric chars");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "112100a", "31070041", "4415030");
    }

    /**
     * Check soc code format and size.
     */
    @Test
    public void checkSocCodeFormatAndSize(){
        System.out.println("Checking if standard occupational classification code doesn't have the right format nor the right size.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Standard occupational classification code has a invalid format and cannot have non numeric chars");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "112100a", "31070041", "4415030");
    }

    /**
     * Check soc code components and size.
     */
    @Test
    public void checkSocCodeComponentsAndSize(){
        System.out.println("Checking if standard occupational classification code doesn't have the right size and if has non numeric chars.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Standard occupational classification code cannot have non numeric chars and must have 7 chars.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-00a", "31070041", "4415030");
    }

    /**
     * Check soc code format.
     */
    @Test
    public void checkSocCodeFormat(){
        System.out.println("Checking if standard occupational classification code doesn't have the right format.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Standard occupational classification code has a invalid format");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10 0000", "31070041", "4415030");
    }

    /**
     * Check soc code components.
     */
    @Test
    public void checkSocCodeComponents(){
        System.out.println("Checking if standard occupational classification code doesn't have non numeric chars.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Standard occupational classification code cannot contain non numeric chars.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-000a", "31070041", "4415030");
    }

    /**
     * Check soc code size.
     */
    @Test
    public void checkSocCodeSize(){
        System.out.println("Checking if standard occupational classification code doesn't have the right size.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Standard occupational classification code must have 7 chars.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-000", "31070041", "4415030");
    }

    /**
     * Check soc code correct.
     */
    @Test
    public void checkSocCodeCorrect(){
        System.out.println("Checking if standard occupational classification code is correct.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    //=========================================================================================================================

    /**
     * Check nif blank.
     */
    @Test
    public void checkNIFBlank(){
        System.out.println("Checking if NIF is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("NIF cannot be blank.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "", "10-0000", "31070041", "");
    }

    /**
     * Check nif size and components.
     */
    @Test
    public void checkNIFSizeAndComponents(){
        System.out.println("Checking if phone number doesn't have the right size and if it has non numeric chars.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Phone number must have 11 numbers and cannot contain non numeric chars.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536aaa", "10-0000", "31070041", "44150a");
    }

    /**
     * Check nif size.
     */
    @Test
    public void checkNIFSize(){
        System.out.println("Checking if phone number doesn't have the right size.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Phone number must have 11 numbers.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "9192253671", "10-0000", "31070041", "441503");
    }

    /**
     * Check nif components.
     */
    @Test
    public void checkNIFComponents(){
        System.out.println("Checking if phone number has non numeric chars.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Phone number cannot contain non numeric chars.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "9192253671a", "10-0000", "31070041", "44150aa");
    }

    /**
     * Check nif correct.
     */
    @Test
    public void checkNIFCorrect(){
        System.out.println("Checking if phone number is correct.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check role blank.
     */
//=========================================================================================================================
    @Test
    public void checkRoleBlank(){
        System.out.println("Checking if typed role is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Typed role cannot be blank.");
        employee.checkAttributes("", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check role exists.
     */
    @Test
    public void checkRoleExists(){
        System.out.println("Checking if typed role exists.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Typed role must be from the available roles list.");
        employee.checkAttributes("Nur", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check role administrator.
     */
    @Test
    public void checkRoleAdministrator(){
        System.out.println("Checking if typed role \"Administrator\" exists.");
        employee.checkAttributes("Administrator", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check role id nurse.
     */
    @Test
    public void checkRoleIdNurse(){
        System.out.println("Checking if typed role \"Nurse\" exists.");
        employee.checkAttributes("Nurse", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check role receptionist.
     */
    @Test
    public void checkRoleReceptionist(){
        System.out.println("Checking if typed role \"Receptionist\" exists.");
        employee.checkAttributes("Receptionist", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check role center coordinator.
     */
    @Test
    public void checkRoleCenterCoordinator(){
        System.out.println("Checking if typed role \"Center Coordinator\" exists.");
        employee.checkAttributes("Center Coordinator", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");
    }

    /**
     * Check role sns user.
     */
    @Test
    public void checkRoleSNSUser(){
        System.out.println("Checking if typed role \"SNS User\" exists.");
        employee.checkAttributes("SNS User", "Mariana ", "rua flores 71", "m23@gmail.com", "91922536712", "10-0000", "31070041", "4415030");

    }

    //=========================================================================================================================

    /**
     * Gets name.
     */
    @Test
    public void getName() {
        System.out.println("Checking if name retrieved is the same as the one entered.");
        String expected = "Mariana ";
        String actual = employee.getName();
        assertEquals(expected, actual);
    }

    /**
     * Gets email address.
     */
    @Test
    public void getEmailAddress() {
        System.out.println("Checking if email address retrieved is the same as the one entered.");
        String expected = "m23@gmail.com";
        String actual = employee.getEmailAddress();
        assertEquals(expected, actual);
    }

    /**
     * Gets role.
     */
    @Test
    public void getRole() {
        System.out.println("Checking if role retrieved is the same as the one entered.");
        String expected = "Nurse";
        String actual =employee.getRole();
        assertEquals(expected, actual);
    }

    /**
     * Gets phone number.
     */
    @Test
    public void getPhoneNumber() {
        System.out.println("Checking if phone number retrieved is the same as the one entered.");
        String expected = "91922536712";
        String actual = employee.getPhoneNumber();
        assertEquals(expected, actual);
    }

    /**
     * Gets address.
     */
    @Test
    public void getAddress() {
        System.out.println("Checking if address retrieved is the same as the one entered.");
        String expected = "rua doutor nº71";
        String actual = employee.getAddress();
        assertEquals(expected, actual);
    }

    /**
     * Gets soc code.
     */
    @Test
    public void getSocCode() {
        System.out.println("Checking if standard occupational classification code retrieved is the same as the one entered.");
        String expected = "10-0000";
        String actual = employee.getSocCode();
        assertEquals(expected, actual);
    }

    /**
     * Gets employee id.
     */
    @Test
    public void getEmployeeId() {
        System.out.println("Checking if employee id retrieved is the same as the one entered.");
        String expected = "31070041";
        String actual = employee.getId();
        assertEquals(expected, actual);
    }

    /**
     * Gets nif.
     */
    @Test
    public void getNIF() {
        System.out.println("Checking if employee NIF retrieved is the same as the one entered.");
        String expected = "4415030";
        String actual = employee.getNIF();
        assertEquals(expected,actual);
    }


}