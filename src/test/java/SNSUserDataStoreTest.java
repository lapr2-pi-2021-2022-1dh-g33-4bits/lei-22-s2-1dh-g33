import app.domain.model.VaccineSchedule;
import app.domain.store.SNSUserDataStore;
import app.domain.store.VaccineScheduleStore;
import app.mappers.dto.SNSUserDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Sns user data store test.
 */
public class SNSUserDataStoreTest {

    private SNSUserDataStore snsUserDataStore = new SNSUserDataStore();
    private SNSUserDTO snsUser;
    private final List<SNSUserDTO> snsUserList = new ArrayList<>();

    private VaccineScheduleStore store = new VaccineScheduleStore();
    private VaccineSchedule vacSched;
    private final List<VaccineSchedule> vaccineScheduleList = new ArrayList<>();

    /**
     * The Exception rule.
     */
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * Set up.
     */
    @Before
    public void setUp(){
        this.snsUser = new SNSUserDTO("João Alberto", "Male", "10/12/2003", "Rua do Verdinho Gaia", "999999999", "joaoalberto@gmail.com", "222222222", "11111111");
    }

    //=============================================================================================

    /**
     * Create sns user.
     */
    @Test
    public void createSNSUser(){
        System.out.println("Creating a SNS user");
        Assert.assertFalse(snsUser.equals(null));
    }

    /**
     * Add sns user.
     */
    @Test
    public void addSNSUser(){
        System.out.println("Adding a SNS user");
        snsUserDataStore.addSNSUser(snsUser);
        snsUserList.contains(snsUser);
    }

    /**
     * Validate sns user component 1.
     */
    @Test
    public void validateSNSUserComponent1(){
        snsUserDataStore.addSNSUser(snsUser);
        System.out.println("Checking if a SNS user was already registered with the same citizen card number.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("There is a SNS User with the same Citizen Card Number already created.");
        SNSUserDTO snsUser2 = new SNSUserDTO("Vitor Pinho", "Male", "14/05/2005", "Rua do Verdinho Pedroso", "999999929", "vitorpinho@gmail.com", "222222242", "11111111");
        snsUserDataStore.validateSNSUser(snsUser2);
    }

    /**
     * Validate sns user component 2.
     */
    @Test
    public void validateSNSUserComponent2(){
        snsUserDataStore.addSNSUser(snsUser);
        System.out.println("Checking if a SNS user was already registered with the same SNS user number.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("There is a SNS User with the same SNS user number already created.");
        SNSUserDTO snsUser2 = new SNSUserDTO("Vitor Pinho", "Male", "14/05/2005", "Rua do Verdinho Pedroso", "999999929", "vitorpinho@gmail.com", "222222222", "33333333");
        snsUserDataStore.validateSNSUser(snsUser2);
    }

    /**
     * Same register.
     */
    @Test
    public void sameRegister(){
        snsUserDataStore.addSNSUser(snsUser);
        System.out.println("Checking if a SNS user was already registered with the same SNS user number.");
        boolean exp = false;
        SNSUserDTO snsUser2 = new SNSUserDTO("Vitor Pinho", "Male", "14/05/2005", "Rua do Verdinho Pedroso", "999999929", "vitorpinho@gmail.com", "222222222", "11111111");
        Assert.assertEquals(exp, snsUserDataStore.sameRegister(snsUser2));
    }
}
