import auth.domain.model.UserRole;
import app.domain.store.OrganizationRolesStore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Organization roles store test.
 */
public class OrganizationRolesStoreTest {

   private List<UserRole> organizationRolesList = new ArrayList<UserRole>();
   private OrganizationRolesStore ors = new OrganizationRolesStore();

    /**
     * Set up.
     */
    @Before
   public void setUp(){
       organizationRolesList.add(new UserRole("Receptionist", "RECEPTIONIST"));
   }

    /**
     * Organization roles list.
     */
    @Test
   public void OrganizationRolesList() {
       System.out.println("Checking if the organization role list retrieved is the same as the one created");
       List<UserRole> expected = new ArrayList<>();
       expected.add(new UserRole("Receptionist", "RECEPTIONIST"));
       Assert.assertEquals(expected, organizationRolesList);
        }

    }
