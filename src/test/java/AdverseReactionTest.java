import app.domain.model.AdverseReaction;
import app.domain.model.VaccineSchedule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class AdverseReactionTest {

    private AdverseReaction adverseReaction;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();


    @Before
    public void setUp(){
        this.adverseReaction = new AdverseReaction("343434343", "Fever on the first 5 minutes.");
    }


    @Test
    void getSnsUserNumber() {
        System.out.println("Checking SNS User Number.");
        adverseReaction = new AdverseReaction("343434343", "Fever on the first 5 minutes.");
        String exp = "343434343";
        String actual = adverseReaction.getSnsUserNumber();
        assertEquals(exp, actual);
    }

    @Test
    void getAdverseReaction() {
        System.out.println("Checking adverse reaction.");
        adverseReaction = new AdverseReaction("343434343", "Fever on the first 5 minutes.");
        String exp = "Fever on the first 5 minutes.";
        String actual = adverseReaction.getAdverseReaction();
        assertEquals(exp, actual);
    }

    @Test
    void checkSNSUserNumber() {
        System.out.println("Checking if the SNS User Number is blank.");
        adverseReaction = new AdverseReaction("343434343", "Fever on the first 5 minutes.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("\n======================= Insert a valid SNS User Number =======================\n");
        adverseReaction.checkSNSUserNumber("");
    }

    @Test
    void checkAdverseReaction() {
        System.out.println("Checking if the adverse reaction is blank.");
        adverseReaction = new AdverseReaction("343434343", "Fever on the first 5 minutes.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("\n======================= Adverse reactions cannot be blank =======================\n");
        adverseReaction.checkSNSUserNumber("");


    }
}