import app.domain.model.RecordVaccine;
import app.domain.model.VaccineSchedule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.InputMismatchException;

import static org.junit.Assert.assertEquals;

/**
 * The type Record vaccination test.
 */
public class RecordVaccinationTest {

    private RecordVaccine recordVaccine;

    /**
     * The Exception rule.
     */
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * Set up.
     */
    @Before
    public void setUp(){this.recordVaccine = new RecordVaccine("12/12/2022","COVID-19","Astra","12345-78","111412341");}

    //=============================================================================================

    /**
     * Check lot number blank.
     */
    @Test
    public void checkLotNumberBlank (){
        System.out.println("Checking if the Lot Number is blank.");
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Sns User Number cannot be blank.");
        recordVaccine.checkLotNumber("");

    }

    /**
     * Check lot number sizeand components.
     */
    @Test
    public void checkLotNumberSizeandComponents () {
        System.out.println("Checking if Lot Number doesn't have the right size and the right format.");
        exceptionRule.expect(InputMismatchException.class);
        exceptionRule.expectMessage("Sns User Number cannot contain non chars.");
        recordVaccine.checkLotNumber("123123-13");
    }

    //=============================================================================================

    /**
     * Gets lot number.
     */
    @Test
    public void getLotNumber() {
        System.out.println("Checking if Lot Number retrieved is the same as the one entered.");
        String exp = "12345-78";
        String actual = recordVaccine.getLotNumber();
        assertEquals(exp, actual);
    }

    /**
     * Gets date.
     */
    @Test
    public void getDate() {
        System.out.println("Checking if Date retrieved is the same as the one entered.");
        String exp = "12/12/2022";
        String actual = recordVaccine.getDate();
        assertEquals(exp, actual);
    }

    /**
     * Gets vac type.
     */
    @Test
    public void getVacType() {
        System.out.println("Checking if Vaccine Type retrieved is the same as the one entered.");
        String exp = "COVID-19";
        String actual = recordVaccine.getVacType();
        assertEquals(exp, actual);
    }

    /**
     * Get sns user number.
     */
    @Test
    public void getSNSUserNumber(){
        System.out.println("Checking if SNS User Number retrieved is the same as the one entered.");
        String exp = "111412341";
        String actual = recordVaccine.getSnsUserNumber();
        assertEquals(exp, actual);
    }

    /**
     * Gets vac name.
     */
    @Test
    public void getVacName() {
        System.out.println("Checking if Vaccine Name retrieved is the same as the one entered.");
        String exp = "Astra";
        String actual = recordVaccine.getVacName();
        assertEquals(exp, actual);
    }
}
