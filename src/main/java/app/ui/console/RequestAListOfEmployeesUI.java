package app.ui.console;

import app.controller.RequestAListOfEmployeesController;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RequestAListOfEmployeesUI implements Runnable {


    static Scanner sc = new Scanner(System.in);

    public void run() {
        RequestAListOfEmployeesController ctrl = new RequestAListOfEmployeesController();
        System.out.println("\nCreate New List\n");

        // Asks the role of the employees
        System.out.println("\n\n************************************");
        System.out.println("Available roles: ");
        System.out.println("************************************");
        int index = 0;
        int i = 0;
        for (Object o :ctrl.getOrganizationRoles())
            {
                index++;
                System.out.println(index + ". " + ctrl.getOrganizationRoles().get(i).getId());
                i++;
            }
        System.out.println("************************************");
        System.out.println("0 - Cancel");

        int option = Utils.selectsIndex(ctrl.getOrganizationRoles());
        String employeeRole = ctrl.getOrganizationRoles().get(option).getId();

        List<String> list = new ArrayList<>();
        if (ctrl.searchEmployeesRoles(employeeRole, ctrl.getEmployeeList(), list) != 0 ){

            // Show the List
            for (int p = 0; p < list.size(); p++) {
                System.out.println(list.get(p));
            }

        } else {
            System.out.println("************************************");
            System.out.println("No employee available with this role.");
            System.out.println("************************************");
        }
    }
}