package app.ui.console;

import app.controller.VaccineTypeController;
import app.domain.model.VaccineType;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

public class VaccineTypeUI implements Runnable {

    static Scanner sc = new Scanner(System.in);
    static VaccineTypeController vacTypeCont = new VaccineTypeController();

    private String vaccineType;
    int check;

    public VaccineTypeUI(){

    }

    public void run() {
        VaccineType vacType = new VaccineType();

        System.out.println("\n===================================================");
        System.out.println("Type the following attribute to specify a new Vaccine Type.");

        check = 0;
        do {
            try {
                System.out.print("Vaccine Type (i.e. Covid-19):");
                this.vaccineType = sc.nextLine();
                vacType.setVaccineType(this.vaccineType);
                check = 1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Insert a valid Vaccine Type =======================\n");
            }
        } while (check == 0);

        try {
            vacTypeCont.registVaccineType(vaccineType);
            vacTypeCont.validateNewVaccineType(vaccineType);

            System.out.println(showData(vaccineType));
            System.out.println("\nConfirm data: (Y/N) ");
            String confirm = sc.nextLine().toUpperCase(Locale.ROOT);
            while (!confirm.equals("Y") & !confirm.equals("N")) {
                System.out.println("\n======================= Invalid character, type again =======================\n");
                System.out.println(showData(vaccineType));
                System.out.print("Confirm data: (Y/N) ");
                confirm = sc.nextLine().toUpperCase(Locale.ROOT);
            }
            if (confirm.equalsIgnoreCase("Y")) {
                vacTypeCont.saveVaccineType();
                System.out.print("\n*** SAVED SUCCESSFULLY ***");
            } else {
                System.out.print("\n*** UNSAVED ***");
            }
        } catch (IllegalStateException error ) {
            System.out.println("\n======================= This Vaccine Type already exists =======================\n");
        }

    }

    public String showData (String vaccineType){

        return ("\n-------------------------")+
                ("\n| Vaccine Type: " + vaccineType)+
                ("\n-------------------------\n");

    }
}
