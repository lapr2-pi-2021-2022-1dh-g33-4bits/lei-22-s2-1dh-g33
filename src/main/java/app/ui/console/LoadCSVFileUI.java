package app.ui.console;
import app.controller.LoadCSVFileController;
import app.mappers.dto.SNSUserDTO;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class LoadCSVFileUI implements Runnable {

    static Scanner readString = new Scanner(System.in);
    static LoadCSVFileController loadCSVFileController = new LoadCSVFileController();
    private List<SNSUserDTO> snsUserLoadingList = new ArrayList<>();

    private String name;
    private String sex;
    private String birthDate;
    private String address;
    private String phoneNumber;
    private String email;
    private String snsUserNumber;
    private String citizenCardNumber;
    int successfullImportCounter = 0;
    int failedImportCounter=0;
    int fileID=0; // if fileID=0 the csv has a wrong format, if 1 it has a header and separated with ";"
                  // if fileID=2 the file doesn't have a header and is separated with ","

    public LoadCSVFileUI () {
    }

    @Override
    public void run() {
        SNSUserDTO snsUser;
        String answer;

        System.out.println("\n===================================================");
        System.out.println("Type the file directory (i.e. on Mac: /Users/vitorpinho/Desktop/(file_name) and on Windows: C:/Users/vitorpinho/Desktop/(file_name) ):");
        String directory = readString.nextLine();
        System.out.println("Confirm the directory: \n (1) - Confirm. \n (2) - I want to change to change it.");
        answer = readString.nextLine();

        while (!Objects.equals(answer, "1")) {
            System.out.println("Type the file directory (i.e. on Mac: /Users/vitorpinho/Desktop/(file_name) and on Windows: C:/Users/vitorpinho/Desktop/(file_name) ):");
            directory = readString.nextLine();
            System.out.println("Confirm the directory: \n (1) - Confirm. \n (2) - I want to change to change it.");
            answer = readString.nextLine();
        }

        File setOfSNSUsersFile = new File(directory + ".csv");
        try {
            Scanner scan = new Scanner(setOfSNSUsersFile);

            String[] inputDataArray = scan.nextLine().split(";");
            if (inputDataArray.length == 8) {
                fileID=1;
            } else if (inputDataArray.length==1) {
                fileID=2;
            } else {
                System.out.println(" **** The file you entered isn't accepted **** ");
                throw new IOException();
            }
            int cont = 0;
            while (scan.hasNext()) {

                if (fileID==1) {
                    inputDataArray = scan.nextLine().split(";");
                } else if (fileID==2) {
                    if (cont==0) {
                        inputDataArray = inputDataArray[0].split(",");
                    } else {
                        inputDataArray = scan.nextLine().split(",");
                    }
                } else {
                    break;
                }

                snsUser = new SNSUserDTO();
                loadCSVFileController.addOrganizationRoles();

                name = inputDataArray[0];
                sex = inputDataArray[1];
                birthDate = inputDataArray[2];
                address = inputDataArray[3];
                phoneNumber = inputDataArray[4];
                email = inputDataArray[5];
                snsUserNumber = inputDataArray[6];
                citizenCardNumber = inputDataArray[7];

                try {
                    snsUser.setName(this.name);
                    snsUser.setSex(this.sex);
                    snsUser.setBirthDate(this.birthDate);
                    snsUser.setAddress(this.address);
                    snsUser.setPhoneNumber(this.phoneNumber);
                    snsUser.setEmail(this.email);
                    snsUser.setSnsUserNumber(this.snsUserNumber);
                    snsUser.setCitizenCardNumber(this.citizenCardNumber);

                    loadCSVFileController.LoadCSVFile(name, sex, birthDate, address, phoneNumber, email, snsUserNumber, citizenCardNumber);
                    loadCSVFileController.validateNewSNSUser(snsUser);
                    snsUserLoadingList.add(snsUser);
                    successfullImportCounter++;
                 } catch (IllegalArgumentException | IllegalStateException error) {
                    failedImportCounter++;
                    break;
                } catch (IllegalCallerException userAlreadyExists) {
                    break;
                }
                cont++;
            }

            if (failedImportCounter==0) {
                System.out.println("Do you want to see what users have been loaded? (Y/N)");
                String confirm = readString.nextLine().toUpperCase(Locale.ROOT);
                while (!confirm.equals("Y") & !confirm.equals("N")) {
                    System.out.println("\n======================= Invalid character, type again =======================\n");
                    System.out.println("Do you want to see what users have been loaded? (Y/N)");
                    confirm = readString.nextLine().toUpperCase(Locale.ROOT);
                }
                loadCSVFileController.saveSNSUserLoadingList(snsUserLoadingList, confirm);
                System.out.println("**** The file was saved successfully (" + successfullImportCounter + " SNS users imported)****");
            } else {
                System.out.println("\n======================= The file you entered wasn't loaded because some errors were found. =======================");
            }

        } catch (IOException e) {
            System.out.println("\n======================= The file you entered wasn't found. =======================");
        }
        snsUserLoadingList.clear();
        successfullImportCounter=0;
    }
}
