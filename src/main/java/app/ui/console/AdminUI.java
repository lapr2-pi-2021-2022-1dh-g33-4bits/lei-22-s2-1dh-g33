package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class AdminUI implements Runnable{
    public AdminUI()
    {
    }

    public void run()
    {
        List<MenuItem> options = new ArrayList<MenuItem>();
        options.add(new MenuItem("Register a SNS User",new RegisterSNSUserUI()));
        options.add(new MenuItem("Register a vaccination center ", new RegistVaccinationCenterUI()));
        options.add(new MenuItem("Register an employee", new CreateEmployeeUi()));
        options.add(new MenuItem("Get a list of employees", new RequestAListOfEmployeesUI()));
        options.add(new MenuItem("Specify a new vaccine and its administration process", new SpecifyNewVaccineUI()));
        options.add(new MenuItem("Load a set of users from a CSV file", new LoadCSVFileUI()));
        options.add(new MenuItem("Specify a new Vaccine Type", new VaccineTypeUI()));

        int option = 0;
        do
        {
            option = Utils.showAndSelectIndex(options, "\n\nAdmin Menu:");

            if ( (option >= 0) && (option < options.size()))
            {
                options.get(option).run();
            }
        }
        while (option != -1 );
    }
}
