package app.ui.console;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CenterCoordinatorUI implements Runnable {

    private final Company company;
    public CenterCoordinatorUI() {
        company = App.getInstance().getCompany();
    }

    public void run() {
        // Menu Vaccination Centers
        System.out.println("\n\n************************************");
        System.out.println("Available Vaccination Centers ");
        System.out.println("************************************");
        int index = 0;
        int i = 0;
        for (VaccinationCenter o : company.getVaccinationCenterStore().getVaccinationCenterList()) {
            index++;
            System.out.println(index + ". " + company.getVaccinationCenterStore().getVaccinationCenterList().get(i).getName());
            i++;
        }
        System.out.println("************************************");
        System.out.println("0 - Cancel");

        int vc = Utils.selectsIndex(company.getVaccinationCenterStore().getVaccinationCenterList());
        System.out.println("************************************");

        if (vc > -1) {
            List<MenuItem> options = new ArrayList<MenuItem>();
            options.add(new MenuItem("Check and export vaccination statistics", new CheckAndExportVaccinationStatisticsUI(vc)));
            options.add(new MenuItem("Analyze Center Performance", new OverviewCompanyUI()));
            options.add(new MenuItem("Import data from a legacy system", new ImportDataUI()));

            int option = 0;
            do {
                option = Utils.showAndSelectIndex(options, "\n\nCenter Coordinator Menu:");

                if ((option >= 0) && (option < options.size())) {
                    options.get(option).run();
                }
            }
            while (option != -1);
            System.out.println("************************************");
        }
    }
}