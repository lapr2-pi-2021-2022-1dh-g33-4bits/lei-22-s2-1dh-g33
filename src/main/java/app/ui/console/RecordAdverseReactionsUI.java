package app.ui.console;
import app.controller.RecordAdverseReactionsController;
import app.domain.model.AdverseReaction;
import app.mappers.dto.SNSUserDTO;

import java.util.Locale;
import java.util.Scanner;

/**
 * The type Record adverse reaction's ui.
 */
public class RecordAdverseReactionsUI implements Runnable {

    /**
     * The Record adverse reaction's controller.
     */
    static RecordAdverseReactionsController recordAdverseReactionsController = new RecordAdverseReactionsController();

    private int vc;
    private String snsUserNumber;
    private String adverseReactionText;
    private SNSUserDTO snsUser;

    int check;

    static Scanner readString = new Scanner(System.in);

    /**
     * Instantiates a new Record adverse reaction's ui.
     *
     * @param vc the vc
     */
    public RecordAdverseReactionsUI (int vc) {
        this.vc = vc;
    }

    @Override
    public void run() {
        AdverseReaction adverseReaction = new AdverseReaction();

        check = 0;
        do {
            try {
                System.out.println("\nChoose the intended SNS User to record an adverse reaction:");
                System.out.print("SNS User Number (9 Digits):");
                this.snsUserNumber = readString.nextLine();
                adverseReaction.setSnsUserNumber(this.snsUserNumber);
                snsUser = recordAdverseReactionsController.checkIfSNSUserExists(snsUserNumber);
                if (snsUser != null) {
                    System.out.println("Is this the SNS user you intended to select? ");
                    System.out.println(snsUser.toString()+"\n\nYes or no (Y/N)?");
                    String confirm = readString.nextLine().toUpperCase(Locale.ROOT);
                    while (!confirm.equals("Y") & !confirm.equals("N")) {
                        System.out.println("\n======================= Invalid character, type again =======================\n");
                        System.out.println("Do you want to see what users have been loaded? (Y/N)");
                        confirm = readString.nextLine().toUpperCase(Locale.ROOT);
                    }
                    if (confirm.toUpperCase(Locale.ROOT).equals("Y")) {
                        check = 1;
                    } else {
                        check = 0;
                    }
                } else {
                    System.out.println("\n======================= This SNS user doesn't exist =======================\n");
                }
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Insert a valid SNS User Number =======================\n");
            }
        } while (check == 0);

        check = 0;
        do {
            try {
                System.out.println("\nWrite the adverse reactions of the selected user:");
                this.adverseReactionText = readString.nextLine();
                adverseReaction.setAdverseReaction(this.adverseReactionText);
                check = 1;
            } catch (IllegalArgumentException error) {
                System.out.println(error.getMessage());
                if (error.getMessage().equals("\n======================= Adverse reactions cannot have more than 300 characters =======================\n")) {
                    int extraCharacters = adverseReactionText.length() - 300;
                    System.out.println("**** YOU WROTE " + extraCharacters +  " EXTRA WORDS ****");
                }
            }
        } while (check == 0);

            recordAdverseReactionsController.newAdverseReaction(snsUserNumber, adverseReactionText);

            showCase(snsUser, adverseReactionText);
            System.out.println("Confirm data: (Y/N) ");
            String confirm = readString.nextLine().toUpperCase(Locale.ROOT);
            while (!confirm.equals("Y") & !confirm.equals("N")) {
                System.out.println("\n======================= Invalid character, type again =======================\n");
                showCase(snsUser, adverseReactionText);
                System.out.print("Confirm data: (Y/N) ");
                confirm = readString.nextLine().toUpperCase(Locale.ROOT);
            }

            if (confirm.equalsIgnoreCase("Y")) {
                recordAdverseReactionsController.saveAdverseReaction();
                System.out.print("\n*** SAVED SUCCESSFULLY ***");
            } else {
                System.out.print("\n*** UNSAVED ***");
            }


    }

    /**
     * Shows all the data to be confirmed.
     *
     * @param snsUser             the sns user
     * @param adverseReactionText the adverse reaction text
     */
    public void showCase (SNSUserDTO snsUser, String adverseReactionText) {
        System.out.println("\n-------------------------");
        System.out.println(snsUser.toString());
        System.out.println("Adverse Reaction:\n" + adverseReactionText);
        System.out.println("-------------------------\n");
    }

}
