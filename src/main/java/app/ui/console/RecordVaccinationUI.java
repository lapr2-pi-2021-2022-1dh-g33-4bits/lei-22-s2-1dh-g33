package app.ui.console;

import app.controller.*;
import app.domain.model.*;
import app.mappers.dto.SNSUserDTO;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.util.*;

public class RecordVaccinationUI implements Runnable{

    static Scanner sc = new Scanner(System.in);
    static RecordVaccinationController recVacCont = new RecordVaccinationController();
    static ConsultWaitingRoomController ctrll = new ConsultWaitingRoomController();
    static VaccineScheduleController vaccineScheduleController = new VaccineScheduleController();

    private final Company company;
    private String vacType;
    private String vacName;
    private String lotNumber;
    private SNSUserDTO snsUser;
    private VaccineSchedule vaccineSchedule;
    private int vc;
    int check;

    public RecordVaccinationUI(int vc){
        this.vc = vc;
        company = App.getInstance().getCompany();
    }

    public void run() {
        RecordVaccine recVac = new RecordVaccine();
        Vaccine vaccine = new Vaccine();
        int index=0;

        List<SNSUserDTO> snsUserDTOList = ctrll.getWaitingRoom(vc);
        List<Vaccine> vaccineList = this.company.getVaccineStore().getVaccineList();
        List<VaccineSchedule> vaccineScheduleList = this.company.getVaccineScheduleStore().getVaccineScheduleList();
        List<RecordVaccine>  recordVaccineList = this.company.getRecordVaccineStore().getRecordVaccineList(vc);
        if (snsUserDTOList.isEmpty()){
            System.out.println("****************************************");
            System.out.println("Waiting Room is empty");
            System.out.println("****************************************");
        } else {
            System.out.println("****************************************");
            System.out.println("Available SNS Users in the Waiting Room.");
            System.out.println("****************************************");
            System.out.println();
            for (int i = 0; i < snsUserDTOList.size(); i++) {
                index++;
                System.out.println(index + ". " + snsUserDTOList.get(i).getName());
            }
            System.out.println();
            System.out.println("****************************************");

            System.out.println("Select the intended SNS User (i.e., 1):");

            String SNSUserOption = sc.nextLine();
            while (Integer.parseInt(SNSUserOption) > index + 1 || Integer.parseInt(SNSUserOption) < 0) {
                SNSUserOption = sc.nextLine();
            }

            this.snsUser = snsUserDTOList.get(Integer.parseInt(SNSUserOption) - 1);


            System.out.println("Name of the SNS User:" + snsUser.getName());
            String birthDate = snsUser.getBirthDate();
            LocalDateTime ldt;
            String[] separated = birthDate.split("/");
            if (separated.length==3) {
                if (separated[0].length() == 1 && separated[1].length() == 2) {
                    birthDate = "0" + separated[0] + "/" + separated[1] + "/" + separated[2];
                }
                if (separated[0].length() == 2 && separated[1].length() == 1) {
                    birthDate = separated[0] + "/0" + separated[1] + "/" + separated[2];
                }
                if (separated[0].length() == 1 && separated[1].length() == 1) {
                    birthDate = "0" + separated[0] + "/0" + separated[1] + "/" + separated[2];
                }
            }
            System.out.println("Age of the SNS User:" + recVacCont.turnBirthDateToAge(birthDate));
            System.out.print("Adverse Reactions of the SNS User:");
            if (recVacCont.checkAdverseReaction(snsUser.getSnsUserNumber()) == null) {
                System.out.println("There are no adverse reactions.");
            } else {
                System.out.println(recVacCont.checkAdverseReaction(snsUser.getSnsUserNumber()));
            }

            System.out.println("****************************************");
            System.out.println();
            System.out.println("The Vaccine Type chosen by the SNS User when the vaccine was scheduled was: " + this.company.getVaccineScheduleStore().getVaccineScheduleList().get(Integer.parseInt(SNSUserOption) - 1).getVacType());

            System.out.println("****************************************");
            System.out.println("Available Vaccine Name/Brand for " + this.company.getVaccineScheduleStore().getVaccineScheduleList().get(Integer.parseInt(SNSUserOption) - 1).getVacType() + " type:");
            System.out.println();

            for (int i = 0; i < vaccineList.size(); i++) {
                int cont1 = 0;
                int cont2 = 0;
                int cont3 = 0;
                for (int j = 0; j < recordVaccineList.size(); j++){
                    if (recVacCont.checkAgeGroup(vaccineList.get(i).getAgeGroup(), recVacCont.turnBirthDateToAge(snsUser.getBirthDate()))) {
                        if (cont3==0){

                            if (Objects.equals(vaccineList.get(i).getDoseNumber(), "1")) {
                                System.out.print(vaccineList.get(i).getName());
                                System.out.print("(Respective Vaccine Dosage: " + vaccineList.get(i).getVaccineDosage());
                                System.out.print(" ml)\n");
                            } else {
                                if (cont1==0){
                                    if (cont2==0){
                                        if (Objects.equals(vaccineList.get(i).getName(),recordVaccineList.get(j).getVacName())){
                                            System.out.print(vaccineList.get(i).getName() + " (Respective Dose: " + recVacCont.getRespectiveDose(snsUser.getSnsUserNumber(), i));
                                            System.out.print("º Dose, Respective Vaccine Dosage: " + vaccineList.get(i).getVaccineDosage());
                                            System.out.print(" ml)\n");
                                            cont1++;
                                        } else {
                                            System.out.print(vaccineList.get(i).getName() + " (Respective Dose: " + recVacCont.getRespectiveDose(snsUser.getSnsUserNumber(), i));
                                            System.out.print("º Dose, Respective Vaccine Dosage: " + vaccineList.get(i).getVaccineDosage());
                                            System.out.print(" ml)\n");
                                            cont2++;
                                        }
                                    }
                                }
                            }
                            cont3++;
                        }
                    }
                }
            }

            this.vacType = this.company.getVaccineScheduleStore().getVaccineScheduleList().get(Integer.parseInt(SNSUserOption) - 1).getVacType();


            System.out.println();
            check = 0;
            do {
                try {

                    System.out.println("\n\n************************************");
                    System.out.println("Available Vaccine Name/Brand ");
                    System.out.println("****************************************");
                    index = 0;
                    List<Vaccine> vacCheckList = new ArrayList<>();
                    for (int i = 0; i < vaccineList.size(); i++) {
                        if (recVacCont.checkAgeGroup(vaccineList.get(i).getAgeGroup(), recVacCont.turnBirthDateToAge(snsUser.getBirthDate()))) {
                            vacCheckList.add(vaccineList.get(i));
                            index++;
                            System.out.println(index + ". " + vaccineList.get(i).getName());
                        }
                    }
                    System.out.println("****************************************\n");
                    System.out.print("Select the administered vaccine (i.e., 1):");
                    String VacOption = sc.nextLine();
                    while (Integer.parseInt(VacOption) > index + 1 || Integer.parseInt(VacOption) < 0) {
                        VacOption = sc.nextLine();
                    }
                    this.vacName = vacCheckList.get(Integer.parseInt(VacOption) - 1).getName();
                    check = 1;
                } catch (IllegalArgumentException error) {
                    System.out.println("\n======================= Vaccine type cannot be blank =======================\n");
                }
            } while (check == 0);

            System.out.println("****************************************");

            check = 0;
            do {
                try {
                    System.out.print("Lot Number (i.e. 21C16-05):");
                    this.lotNumber = sc.nextLine();
                    recVac.setLotNumber(this.lotNumber);
                    check = 1;
                } catch (IllegalArgumentException | InputMismatchException error) {
                    System.out.println("\n======================= Insert a valid Lot Number =======================\n");
                }
            } while (check == 0);

            recVacCont.recordVaccine(vacType, vacName, lotNumber, snsUser.getSnsUserNumber());

            System.out.println(showData(vacType, vacName, lotNumber));
            System.out.println("\nConfirm data: (Y/N) ");
            String confirm = sc.nextLine().toUpperCase(Locale.ROOT);
            while (!confirm.equals("Y") & !confirm.equals("N")) {
                System.out.println("\n======================= Invalid character, type again =======================\n");
                System.out.println(showData(vacType, vacName, lotNumber));
                System.out.print("Confirm data: (Y/N) ");
                confirm = sc.nextLine().toUpperCase(Locale.ROOT);
            }

            if (confirm.equalsIgnoreCase("Y")) {
                recVacCont.saveRecordVaccine(vc);
                System.out.print("\n*** VACCINE RECORDED SUCCESSFULLY ***");
                recVacCont.removeSNSUserFromWaitingRoom(snsUser, vc);
                for (int i = 0; i < vaccineScheduleList.size(); i++) {
                    if (vaccineScheduleList.get(i).getSnsUserNumber().equals(snsUser.getSnsUserNumber())) {
                        vaccineScheduleController.removeVaccineSchedule(vaccineScheduleList.get(i));
                    }
                }

                try {
                    recVacCont.addSNSUserToRecoveryRoom(vacType, vacName, lotNumber, snsUser, vc);
                    Timer timer = new Timer();
                    TimerTask task = new Helper(vacType, vacName, lotNumber, snsUser, vc, company);
                    timer.schedule(task, Integer.parseInt(recVacCont.getRecoveryPeriod()));
                } catch (IOException | ClassNotFoundException | InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }

            } else {
                System.out.print("\n*** VACCINE NOT SCHEDULED ***");
            }
        }
    }
    public String showData (String vacType, String vacName, String lotNumber){

        return ("\n-------------------------")+
                ("\n| Vaccine Type: " + vacType)+
                ("\n| Vaccine Name/Brand: " + vacName)+
                ("\n| Lot Number: " + lotNumber)+
                ("\n-------------------------\n");

    }

    class Helper extends TimerTask {
        private Company company;
        private String vacType;
        private String vacName;
        private String lotNumber;
        private SNSUserDTO snsUserDTO;
        private int vc;
        private SendRecoveryPeriodMessageController sendRecoveryPeriodMessageController = new SendRecoveryPeriodMessageController();

        public Helper(String vacType, String vacName, String lotNumber, SNSUserDTO snsUserDTO, int vc, Company company) {
            this.vacType = vacType;
            this.vacName = vacName;
            this.lotNumber = lotNumber;
            this.company = company;
            this.snsUserDTO = snsUserDTO;
            this.vc = vc;
        }

        public String showData(String vacType, String vacName, String lotNumber) {
            return ("\n-------------------------") +
                    ("\n| Vaccine Type: " + vacType) +
                    ("\n| Vaccine Name/Brand: " + vacName) +
                    ("\n| Lot Number: " + lotNumber) +
                    ("\n-------------------------\n");

        }

        public void run() {
            PrintWriter printWriter;
            try {
                sendRecoveryPeriodMessageController.sendMessage(vacType, vacName, lotNumber, snsUserDTO);
                printWriter = new PrintWriter("SMS.txt");
                printWriter.println("Hey, " + snsUserDTO.getName() + "!\n");
                printWriter.println("Here are your Vaccine Administration Details:");
                printWriter.println(showData(vacType, vacName, lotNumber));
                printWriter.println("The recovery period is over you can leave the Vaccination Center.");
                printWriter.println("See you Next Time!");
                printWriter.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            this.company.getRecoveryRoomStore().removeSNSUserFromRecoveryRoom(snsUserDTO, vc);
        }
    }
}
