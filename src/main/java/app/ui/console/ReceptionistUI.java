package app.ui.console;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ReceptionistUI implements Runnable {
    private final Company company;
    public ReceptionistUI(){

        company = App.getInstance().getCompany();
    }

    public void run() {

        // Menu Vaccination Centers
        System.out.println("\n\n************************************");
        System.out.println("Available Vaccination Centers ");
        System.out.println("************************************");
        int index = 0;
        int i = 0;
        for (VaccinationCenter o : company.getVaccinationCenterStore().getVaccinationCenterList()) {
            index++;
            System.out.println(index + ". " + company.getVaccinationCenterStore().getVaccinationCenterList().get(i).getName());
            i++;
        }
        System.out.println("************************************");
        System.out.println("0 - Cancel");
        int vc = Utils.selectsIndex(company.getVaccinationCenterStore().getVaccinationCenterList());
        System.out.println("************************************");

        //Menu2
        if (vc > -1) {

            List<MenuItem> options = new ArrayList<MenuItem>();
            options.add(new MenuItem("Schedule a vaccine", new VaccineScheduleReceptionistUI(vc)));
            options.add(new MenuItem("Register a new arrival ", new RegistArrivalUI(vc)));
            int option = 0;
            do {
                option = Utils.showAndSelectIndex(options, "\n\nReceptionist Menu:");

                if ((option >= 0) && (option < options.size())) {
                    options.get(option).run();
                }
            }
            while (option != -1);

        }
    }
}
