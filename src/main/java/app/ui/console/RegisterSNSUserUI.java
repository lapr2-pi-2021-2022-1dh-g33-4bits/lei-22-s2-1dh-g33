package app.ui.console;

import app.controller.App;
import app.controller.RegisterSNSUserController;
import app.domain.model.Company;
import app.mappers.dto.SNSUserDTO;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

public class RegisterSNSUserUI implements Runnable{

    static Scanner sc = new Scanner(System.in);
    static RegisterSNSUserController SNSUserCont = new RegisterSNSUserController();

    private final Company company;
    private String name;
    private String sex;
    private String birthDate;
    private String address;
    private String phoneNumber;
    private String email;
    private String snsUserNumber;
    private String citizenCardNumber;
    int check;

    public RegisterSNSUserUI(){

        company = App.getInstance().getCompany();
    }

    public void run() {
        SNSUserDTO snsUser = new SNSUserDTO();

        System.out.println("\n===================================================");
        System.out.println("Type the following attributes to register a SNS User.");

        check = 0;
        do {
            try {
                System.out.print("Name (i.e. Francelina):");
                this.name = sc.nextLine();
                snsUser.setName(this.name);
                check = 1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid Name =======================\n");
            }
        } while (check == 0);

        check = 0;
        do {
            try {
                System.out.print("Sex (i.e. M):");
                this.sex = sc.nextLine();
                snsUser.setSex(this.sex);
                check = 1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Insert a valid Sex =======================\n");
            }
        } while (check == 0);

        check = 0;
        do {
            try {
                System.out.print("Birth Date (i.e. 19/05/2022):");
                this.birthDate = sc.nextLine();
                snsUser.setBirthDate(this.birthDate);
                check = 1;
            } catch (IllegalArgumentException | IllegalStateException error) {
                System.out.println("\n======================= Insert a valid Birth Date =======================\n");
            }
        } while (check == 0);

        check = 0;
        do {
            try {
                System.out.print("Address (i.e. Rua dos Campos):");
                this.address = sc.nextLine();
                snsUser.setAddress(this.address);
                check = 1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Insert a valid Address =======================\n");
            }
        } while (check == 0);

        check = 0;
        do {
            try {
                System.out.print("Phone Number (i.e. 912345670):");
                this.phoneNumber = sc.nextLine();
                snsUser.setPhoneNumber(this.phoneNumber);
                check = 1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Insert a valid Phone Number =======================\n");
            }
        } while (check == 0);

        check = 0;
        do {
            try {
                System.out.print("E-mail (i.e. francelina@gmail.com):");
                this.email = sc.nextLine();
                snsUser.setEmail(this.email);
                check = 1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Insert a valid E-mail =======================\n");
            }
        } while (check == 0);

        check = 0;
        do {
            try {
                System.out.print("SNS User Number (9 Digits):");
                this.snsUserNumber = sc.nextLine();
                snsUser.setSnsUserNumber(this.snsUserNumber);
                check = 1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Insert a valid SNS User Number =======================\n");
            }
        } while (check == 0);

        check = 0;
        do {
            try {
                System.out.print("Citizen Card Number (8 Digits):");
                this.citizenCardNumber = sc.nextLine();
                snsUser.setCitizenCardNumber(this.citizenCardNumber);
                check = 1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Insert a valid Citizen Card Number =======================\n");
            }
        } while (check == 0);

        try {

            SNSUserCont.createSNSUser(name, sex, birthDate, address, phoneNumber, email, snsUserNumber, citizenCardNumber);
            SNSUserCont.validateNewSNSUser(snsUser);

            showData(name, sex, birthDate, address, phoneNumber, email, snsUserNumber, citizenCardNumber);
            System.out.println("\nConfirm data: (Y/N) ");
            String confirm = sc.nextLine().toUpperCase(Locale.ROOT);
            while (!confirm.equals("Y") & !confirm.equals("N")) {
                System.out.println("\n======================= Invalid character, type again =======================\n");
                showData(name, sex, birthDate, address, phoneNumber, email, snsUserNumber, citizenCardNumber);
                System.out.print("Confirm data: (Y/N) ");
                confirm = sc.nextLine().toUpperCase(Locale.ROOT);
            }

            if (confirm.equalsIgnoreCase("Y")) {
                SNSUserCont.saveSNSUser();
                System.out.print("\n*** SAVED SUCCESSFULLY ***");
            } else {
                System.out.print("\n*** UNSAVED ***");
            }
        } catch (IllegalStateException | IOException error ) {
            System.out.println("\n======================= This SNS User already exists =======================\n");
        }
    }

    public void showData (String name, String sex, String birthDate, String address, String phoneNumber, String email, String snsUserNumber, String citizenCardNumber){
        System.out.println("\n-------------------------");
        System.out.println("| Name: " + name);
        System.out.println("| Sex: " + sex);
        System.out.println("| Birth Date: " + birthDate);
        System.out.println("| Address: " + address);
        System.out.println("| Phone Number: " + phoneNumber);
        System.out.println("| E-mail: " + email);
        System.out.println("| SNS User Number: " + snsUserNumber);
        System.out.println("| Citizen Card Number: " + citizenCardNumber);
        System.out.println("-------------------------\n");
    }


}
