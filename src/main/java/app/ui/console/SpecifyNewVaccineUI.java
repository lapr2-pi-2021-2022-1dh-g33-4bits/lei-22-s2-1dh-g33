package app.ui.console;

import app.controller.App;
import app.controller.SpecifyNewVaccineController;
import app.domain.model.Company;
import app.domain.model.Vaccine;
import app.domain.model.VaccineType;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;


public class SpecifyNewVaccineUI implements Runnable {

    static Scanner readString = new Scanner(System.in);
    static SpecifyNewVaccineController newVacController = new SpecifyNewVaccineController();

    private final Company company;
    private String ID;
    private String name;
    private String brand;
    private String vaccineType;
    private String ageGroup;
    private String doseNumber;
    private String vaccineDosage;
    private String timeBetweenDoses;
    int check;

    public SpecifyNewVaccineUI () {
        company = App.getInstance().getCompany();
    }

    @Override
    public void run() {
        Vaccine vaccine = new Vaccine();

        System.out.println("\n===================================================");
        System.out.println("Type the following attributes to specify a new vaccine.");

        check=0;
        do {
            try {
                System.out.print("ID (i.e., 91300):");
                this.ID = readString.nextLine();
                vaccine.setID(this.ID);
                check=1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid ID =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Name (i.e., Pfizer-BioNTech COVID-19 Vaccine):");
                this.name = readString.nextLine();
                vaccine.setName(this.name);
                check=1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Name cannot be blank =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Brand (i.e., Pfizer, Inc.):");
                this.brand = readString.nextLine();
                vaccine.setBrand(this.brand);
                check=1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Brand cannot be blank =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.println("\n\n************************************");
                System.out.println("Available Vaccine Types ");
                System.out.println("************************************");
                int index = 0;
                int i = 0;
                for (VaccineType o : company.getVaccineTypesStore().getVaccineTypesList()) {
                    index++;
                    System.out.println(index + ". " + company.getVaccineTypesStore().getVaccineTypesList().get(i).getVaccineType());
                    i++;
                }
                System.out.println("************************************\n");
                System.out.print("Select the intended vaccine type (i.e., 1):");
                String vacTypeName = readString.nextLine();
                while (Integer.parseInt(vacTypeName)>index+1 || Integer.parseInt(vacTypeName) < 0){
                    vacTypeName = readString.nextLine();
                }
                this.vaccineType = company.getVaccineTypesStore().getVaccineTypesList().get(Integer.parseInt(vacTypeName)-1).getVaccineType();
                vaccine.setVaccineType(this.vaccineType);
                check=1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Vaccine type cannot be blank =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Age Group (i.e., 18-30;50-80):");
                this.ageGroup = readString.nextLine();
                vaccine.setAgeGroup(this.ageGroup);
                check=1;
            } catch (IllegalArgumentException | IllegalStateException error) {
                System.out.println("\n======================= Insert an age group following the example: i.e., 18-30;50-80 =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Dose Number:");
                this.doseNumber = readString.nextLine();
                vaccine.setDoseNumber(this.doseNumber);
                check=1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid dose number =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Vaccine Dosage (the amount of the vaccine to be administered, in mL: i.e., 0.5):");
                this.vaccineDosage= readString.nextLine();
                vaccine.setVaccineDosage(this.vaccineDosage);
                check=1;
            } catch (IllegalArgumentException | IllegalStateException error) {
                System.out.println("\n======================= Insert a valid vaccine dosage =======================\n");

            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Time Between Doses (number of days):");
                this.timeBetweenDoses = readString.nextLine();
                vaccine.setTimeBetweenDoses(this.timeBetweenDoses);
                check=1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid time frame (number of days) =======================\n");
            }
        } while (check==0);
        System.out.println("===================================================\n");

        //createVaccine(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
        try {
            newVacController.SpecifyVaccine(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
            newVacController.validateVaccine(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);

            showCase(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
            System.out.println("Confirm data: (Y/N) ");
            String confirm = readString.nextLine().toUpperCase(Locale.ROOT);
            while (!confirm.equals("Y") & !confirm.equals("N")) {
                System.out.println("\n======================= Invalid character, type again =======================\n");
                showCase(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
                System.out.print("Confirm data: (Y/N) ");
                confirm = readString.nextLine().toUpperCase(Locale.ROOT);
            }

            if (confirm.equalsIgnoreCase("Y")) {
                newVacController.saveNewVaccine();
                System.out.print("\n*** SAVED SUCCESSFULLY ***");
            } else {
                System.out.print("\n*** UNSAVED ***");
            }
        } catch (IllegalStateException error ) {
            System.out.println("\n======================= This vaccine already exists =======================\n");
        }
    }

    public void showCase (String ID, String name, String brand, String vaccineType, String ageGroup, String doseNumber, String vaccineDosage, String timeBetweenDoses) {
        System.out.println("\n-------------------------");
        System.out.println("ID: " + ID);
        System.out.println("Name: " + name);
        System.out.println("Brand: " + brand);
        System.out.println("Vaccine type: " + vaccineType);
        System.out.println("Age group: " + ageGroup);
        System.out.println("Dose number: " + doseNumber);
        System.out.println("Vaccine dosage: " + vaccineDosage);
        System.out.println("Time between doses: " + timeBetweenDoses);
        System.out.println("-------------------------\n");
    }
/*
    public void createVaccine (int ID, String name, String brand, String vaccineType, String ageGroup, int doseNumber, String vaccineDosage, int timeBetweenDoses){
        newVacController.SpecifyVaccine(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
        newVacController.validateTestType(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
    }
 */
}