package app.ui.console;

import app.controller.ImportDataController;
import app.domain.model.VaccinationData;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class ImportDataUI implements Runnable {

    static Scanner readString = new Scanner(System.in);
    static ImportDataController controller = new ImportDataController();
    private List<VaccinationData> LoadingList = new ArrayList<>();
    private final List<String> errorString = new ArrayList<>();

    private String snsUserNumber;
    private String vacineType;
    private String dose;
    private String loteNumber;
    private String scheduleDate;
    private String scheduleTime;
    private String arrivalDate;
    private String arrivalTime;
    private String administrationTime;
    private String administrationDate;
    private String leavingTime;
    private String leavingDate;

    int failedImportCounter = 0;


    public ImportDataUI() {
    }

    public void run() {

        String answer;

        System.out.println("\n*********************************************");
        System.out.println("Type the file directory (i.e. on Mac: /Users/vitorpinho/Desktop/(file_name) and on Windows: C:/Users/vitorpinho/Desktop/(file_name) ):");
        String directory = readString.nextLine();
        System.out.println("Confirm the directory: \n 1 - Confirm. \n 2- I want to change to change it.");
        answer = readString.nextLine();

        while (!Objects.equals(answer, "1")) {
            System.out.println("Type the file directory (i.e. on Mac: /Users/vitorpinho/Desktop/(file_name) and on Windows: C:/Users/vitorpinho/Desktop/(file_name) ):");
            directory = readString.nextLine();
            System.out.println("Confirm the directory: \n 1- Confirm. \n 2 - I want to change to change it.");
            answer = readString.nextLine();
        }
        File setOfSNSUsersFile = new File(directory + ".csv");
        try {
            Scanner scan = new Scanner(setOfSNSUsersFile);


            int cont = 0;
            int conterror = 0;
            scan.nextLine();
            while (scan.hasNext()) {
                String[] inputDataArray = scan.nextLine().split(";");
                VaccinationData vcc = new VaccinationData();

                snsUserNumber = inputDataArray[0];
                vacineType = inputDataArray[1];
                dose = inputDataArray[2];
                loteNumber = inputDataArray[3];
                String schedule = inputDataArray[4];
                String[] inputDataArray1 = schedule.split(" ");
                scheduleDate = inputDataArray1[0];
                scheduleTime = inputDataArray1[1];
                schedule = inputDataArray[5];
                inputDataArray1 = schedule.split(" ");
                arrivalDate = inputDataArray1[0];
                arrivalTime = inputDataArray1[1];
                schedule = inputDataArray[6];
                inputDataArray1 = schedule.split(" ");
                administrationDate = inputDataArray1[0];
                administrationTime = inputDataArray1[1];
                schedule = inputDataArray[7];
                inputDataArray1 = schedule.split(" ");
                leavingDate = inputDataArray1[0];
                leavingTime = inputDataArray1[1];
                try {
                    vcc.setSnsUserNumber(this.snsUserNumber);
                    vcc.setVacineType(this.vacineType);
                    vcc.setDose(this.dose);
                    vcc.setLoteNumber(this.loteNumber);
                    vcc.setScheduleDate(this.scheduleDate);
                    vcc.setScheduleTime(this.scheduleTime);
                    vcc.setArrivalDate(this.arrivalDate);
                    vcc.setArrivalTime(this.arrivalTime);
                    vcc.setAdministrationDate(this.administrationDate);
                    vcc.setAdministrationTime(this.administrationTime);
                    vcc.setLeavingTime(this.leavingTime);
                    vcc.setLeavingDate(this.leavingDate);
                    if (controller.verifySnsUser(this.snsUserNumber)) {
                        controller.LoadFile(snsUserNumber, vacineType, dose, loteNumber, scheduleDate, scheduleTime, arrivalDate, arrivalTime, administrationDate, administrationTime, leavingDate, leavingTime);
                    } else {
                        errorString.add(snsUserNumber);
                        conterror++;
                    }
                } catch (IllegalArgumentException | IllegalStateException | ParseException error) {
                    failedImportCounter++;
                    break;

                }
                cont++;
            }
            if (conterror != 0) {
                System.out.println("This users are not registered in the system: ");
                for (int i = 0; i < errorString.size(); i++) {
                    System.out.println(errorString.get(i));
                }
            }
            System.out.println("**** The file was saved successfully (" + cont + " SNS users imported)****");
            String answer2 = "NULL";
            System.out.println("Do you want to see the data loaded? (Y/N)");
            while (!(answer2.equalsIgnoreCase("N") || answer2.equalsIgnoreCase("Y"))) {
                answer2 = readString.nextLine();
            }
            if (answer2.equals("Y")) {
                String answer3 = "NULL";
                System.out.println("Do you want to see the data sorted by arrival time or by the center leaving time?");
                System.out.println("1 - Arrival Time");
                System.out.println("2 - Leaving Time");
                while (!(answer3.equals("1") || answer3.equals("2"))) {
                    answer3 = readString.nextLine();
                }
                System.out.println("Do you want to see the data sorted in ascending or descending order?");
                System.out.println("1 - Ascending Order");
                System.out.println("2 - Descending Order");
                String answer4 = "NULL";
                while (!(answer4.equals("1") || answer4.equals("2"))) {
                    answer4 = readString.nextLine();
                }
                if (answer3.equals("1")) {
                    if (answer4.equals("1")) {
                        LoadingList = controller.sortArrivalDate();
                        for (int i = 0; i < LoadingList.size(); i++) {
                            System.out.println(controller.getUserName(LoadingList.get(i).getSnsUserNumber()) + " " + LoadingList.get(i).toString());
                        }
                    } else {
                        LoadingList = controller.sortArrivalDate();
                        for (int i = LoadingList.size() - 1; i >= 0; i--) {
                            System.out.println(controller.getUserName(LoadingList.get(i).getSnsUserNumber()) + " " + LoadingList.get(i).toString());
                        }
                    }
                } else {
                    if (answer4.equals("1")) {
                        LoadingList = controller.sortLeavingDate();
                        for (int i = 0; i < LoadingList.size(); i++) {
                            System.out.println(controller.getUserName(LoadingList.get(i).getSnsUserNumber()) + " " + LoadingList.get(i).toString());
                        }
                    } else {
                        LoadingList = controller.sortLeavingDate();
                        for (int i = LoadingList.size() - 1; i >= 0; i--) {
                            System.out.println(controller.getUserName(LoadingList.get(i).getSnsUserNumber()) + " " + LoadingList.get(i).toString());
                        }
                    }
                }
            }
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }
    }