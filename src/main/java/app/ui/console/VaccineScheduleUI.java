package app.ui.console;

import app.controller.App;
import app.controller.RegisterSNSUserController;
import app.controller.SendConfirmationMessageController;
import app.controller.VaccineScheduleController;
import app.domain.model.Company;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineSchedule;
import app.domain.model.VaccineType;


import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

public class VaccineScheduleUI implements Runnable{

    static Scanner sc = new Scanner(System.in);
    static VaccineScheduleController vacSchedcont = new VaccineScheduleController();
    static SendConfirmationMessageController sendMessageController = new SendConfirmationMessageController();
    static RegisterSNSUserController registerSNSUserController = new RegisterSNSUserController();

    private final Company company;
    private String snsUserNumber;
    private String vaccinationCenterName;
    private String date;
    private String time;
    private String vacType;
    int check;

    public VaccineScheduleUI(){

        company = App.getInstance().getCompany();
    }

    public void run() {
        VaccineSchedule vacSched = new VaccineSchedule();


        System.out.println("\n===================================================");
        System.out.println("Type the following attributes to schedule a vaccine.");

        check = 0;
        do {
            try {
                System.out.print("SNS User Number (9 Digits):");
                this.snsUserNumber = sc.nextLine();
                vacSched.setSnsUserNumber(this.snsUserNumber);
                check = 1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid SNS User Number =======================\n");
            }
        } while (check == 0);

       /* check = 0;
        do {
            try {
                System.out.print("Vaccination Center Name (i.e. Pedras Rubras):");
                this.vaccinationCenterName = sc.nextLine();
                vacSched.setVaccinationCenterName(this.vaccinationCenterName);
                check = 1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid Vaccination Center Name =======================\n");
            }
        } while (check == 0);*/

        System.out.println("\n\n************************************");
        System.out.println("Available Vaccination Centers ");
        System.out.println("************************************");
        int index = 0;
        int i = 0;
        for (VaccinationCenter o : company.getVaccinationCenterStore().getVaccinationCenterList()) {
            index++;
            System.out.println(index + ". " + company.getVaccinationCenterStore().getVaccinationCenterList().get(i).getName());
            i++;
        }
        System.out.println("************************************\n");
        System.out.print("Select the intended vaccination center (i.e., 1):");
        String VacNameOption = sc.nextLine();
        while (Integer.parseInt(VacNameOption)>index+1 || Integer.parseInt(VacNameOption) < 0){
          VacNameOption = sc.nextLine();
        }
        this.vaccinationCenterName = company.getVaccinationCenterStore().getVaccinationCenterList().get(Integer.parseInt(VacNameOption)-1).getName();

        System.out.println("************************************");

        check = 0;
        do {
            try {
                System.out.print("Date (i.e. 12/12/2022):");
                this.date = sc.nextLine();
                vacSchedcont.verifyDateAndTimeBetweenDoses(date, snsUserNumber);
                vacSched.setDate(this.date);
                check = 1;
            } catch (IllegalArgumentException | IllegalStateException | ParseException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid Date =======================\n");
            }
        } while (check == 0);

        check = 0;
        do {
            try {
                System.out.print("Time (i.e. 19:05):");
                this.time = sc.nextLine();
                vacSched.setTime(this.time);
                check = 1;
            } catch (IllegalArgumentException | IllegalStateException error) {
                System.out.println("\n======================= Insert a valid Time =======================\n");
            }
        } while (check == 0);

       /* check = 0;
        do {
            try {
                System.out.print("Vaccine Type (i.e. Covid-19):");
                this.vacType = sc.nextLine();
                vacSched.setVacType(this.vacType);
                check = 1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid Vaccination Type =======================\n");
            }
        } while (check == 0);

        System.out.println("\n===================================================\n");*/

        System.out.println("\n\n************************************");
        System.out.println("Available Vaccine Types ");
        System.out.println("************************************");
        index = 0;
        i = 0;
        for (VaccineType o : company.getVaccineTypesStore().getVaccineTypesList()) {
            index++;
            System.out.println(index + ". " + company.getVaccineTypesStore().getVaccineTypesList().get(i).getVaccineType());
            i++;
        }
        System.out.println("************************************\n");
        System.out.print("Select the intended vaccine type (i.e., 1):");
        String VacTypeOption = sc.nextLine();
        while (Integer.parseInt(VacTypeOption)>index+1 || Integer.parseInt(VacTypeOption) < 0){
            VacTypeOption = sc.nextLine();
        }
        this.vacType = company.getVaccineTypesStore().getVaccineTypesList().get(Integer.parseInt(VacTypeOption)-1).getVaccineType();

        System.out.println("************************************");

        try {
            vacSchedcont.vaccineSchedule(snsUserNumber, vaccinationCenterName, date, time, vacType);
            vacSchedcont.validateNewVaccineSchedule(snsUserNumber, vaccinationCenterName, date, time, vacType);

            System.out.println(showData(snsUserNumber, vaccinationCenterName, date, time, vacType));
            System.out.println("\nConfirm data: (Y/N) ");
            String confirm = sc.nextLine().toUpperCase(Locale.ROOT);
            while (!confirm.equals("Y") & !confirm.equals("N")) {
                System.out.println("\n======================= Invalid character, type again =======================\n");
                System.out.println(showData(snsUserNumber, vaccinationCenterName, date, time, vacType));
                System.out.print("Confirm data: (Y/N) ");
                confirm = sc.nextLine().toUpperCase(Locale.ROOT);
            }

            if (confirm.equalsIgnoreCase("Y")) {
                vacSchedcont.saveVaccineSchedule();
                vacSchedcont.saveVaccineHistory();
                System.out.print("\n*** VACCINE SCHEDULED SUCCESSFULLY ***");

                System.out.println("\nDO YOU WANT TO RECEIVE A SMS MESSAGE: (Y/N) ");
                String confirm1 = sc.nextLine().toUpperCase(Locale.ROOT);
                while (!confirm1.equals("Y") & !confirm1.equals("N")) {
                    System.out.println("\n======================= Invalid character, type again =======================\n");
                }
                if (confirm1.equalsIgnoreCase("Y")){
                    System.out.println();
                    sendMessageController.sendMessage(registerSNSUserController.getNameWithSNSUserNumber(snsUserNumber), snsUserNumber, vaccinationCenterName, date, time, vacType);
                    PrintWriter printWriter = new PrintWriter("SMS.txt");
                    printWriter.println(showData(snsUserNumber, vaccinationCenterName, date, time, vacType));
                    printWriter.close();
                    System.out.print("\n*** SMS MESSAGE HAS BEEN SENT ***");
                }else {
                    System.out.print("\n*** SMS MESSAGE HAS NOT BEEN SENT ***");
                }
            }else{
                System.out.print("\n*** VACCINE NOT SCHEDULED ***");
            }

        } catch(IllegalStateException | FileNotFoundException error ){
                System.out.println("\n======================= This vaccine already exists =======================\n");
        }
    }

        public String showData (String snsUserNumber, String vaccinationCenterName, String date, String time, String vacType){

        return ("\n-------------------------")+
            ("\n| SNS User Number: " + snsUserNumber)+
            ("\n| Vaccination Center Name: " + vaccinationCenterName)+
            ("\n| Date: " + date)+
            ("\n| Time: " + time)+
            ("\n| Vaccination Type: " + vacType)+
            ("\n-------------------------\n");

        }

}