package app.ui.console;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DevTeamUI implements Runnable{

    public DevTeamUI()
    {

    }
    public void run()
    {
        System.out.println("\n");
        System.out.printf("Development Team:\n");
        System.out.printf("\t Mariana Neves - 1211264@isep.ipp.pt \n");
        System.out.printf("\t Luís Pinto - 1211192@isep.ipp.pt \n");
        System.out.printf("\t Rui Couto - 1211497@isep.ipp.pt \n");
        System.out.printf("\t Vitor Pinho - 1211175@isep.ipp.pt \n");
    }
}
