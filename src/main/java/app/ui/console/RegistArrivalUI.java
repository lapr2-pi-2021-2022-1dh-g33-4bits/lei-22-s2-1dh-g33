package app.ui.console;

import app.controller.RegistArrivalController;
import app.domain.model.SNSUser;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineSchedule;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class RegistArrivalUI implements Runnable {

    static Scanner scanner = new Scanner(System.in);

    RegistArrivalController ra = new RegistArrivalController();

    private VaccinationCenter vaccinationCenter;
    private VaccineSchedule vaccineSchedule;
    private String snsUserNumber;
    private int vc;

    private String time;

    private String date;

    public RegistArrivalUI(int vc) {
        this.vc = vc;
    }

    @Override
    public void run() {
        System.out.println("\nRegister new arrival");
        System.out.println("\n************************************\n");

        int check = 0;
        do {
            try {
                System.out.print("SNS User Number (9 Digits):");
                this.snsUserNumber = scanner.nextLine();
                ra.setSnsUserNumber(this.snsUserNumber);
                ra.validateSNSUserNumber(snsUserNumber);
                check = 1;
            } catch (IllegalArgumentException | IllegalStateException error) {
                System.out.println("\n======================= Insert a valid SNS User Number =======================\n");
            }
        } while (check == 0);

        check = 0;
            do {
                try {
                    System.out.print("Enter the date (dd/MM/yyyy):");
                    this.date = scanner.nextLine();
                    ra.setDate(this.date);
                    ra.checkDate(date);
                    check = 1;
                } catch (IllegalArgumentException | IllegalStateException error) {
                    System.out.println("\n======================= Insert a valid Date =======================\n");
                }
            } while (check == 0);

            check = 0;
            do {
                try {
                    System.out.print("Enter the arrival time (HH:MM):");
                    this.time = scanner.nextLine();
                    ra.setTime(this.time);
                    ra.checkTime(time);
                    check = 1;
                } catch (IllegalArgumentException | IllegalStateException error) {
                    System.out.println("\n======================= Insert a valid Time =======================\n");
                }
            } while (check == 0);

            System.out.println(showData(snsUserNumber,date,time));
            System.out.println("\nConfirm: (Y/N)");
            String response = scanner.nextLine();

            while (!response.equalsIgnoreCase("Y") & !response.equalsIgnoreCase("N")) {
                System.out.println("\n*********************** Invalid char, type again *************************\n");
                System.out.println("Confirm : (Y/N)");
                response = scanner.nextLine();
            }

        if (response.equalsIgnoreCase("Y")) {

            //try {
            if (ra.saveRegister(snsUserNumber,vc)) {
                ra.searchForSnsUser(vc, date, time, snsUserNumber);
                System.out.println("\n SNS USER ARRIVAL REGISTERED SUCCESSFULLY ");
            } else if (ra.checkIfSNSUserExists(snsUserNumber) ){
                System.out.println("\n SNS USER NOT REGISTERED");
            } else {
                System.out.println("\n SNS USER ALREADY REGISTERED");
            }

            //} catch (IOException e) {
            //System.out.println(e.getMessage());
            //System.out.println("\n SNS USER NOT REGISTERED");
                    //}
                    //}

        }
    }
    public String showData (String snsUserNumber, String date, String time){

        return ("\n-------------------------")+
                ("\n| SNS User Number: " + snsUserNumber)+
                ("\n| Date: " + date)+
                ("\n| Time: " + time)+
                ("\n-------------------------\n");

    }
}
