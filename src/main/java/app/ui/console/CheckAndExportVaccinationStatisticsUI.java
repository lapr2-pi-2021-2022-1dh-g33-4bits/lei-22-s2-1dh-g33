package app.ui.console;

import app.controller.App;
import app.controller.CheckAndExportVaccinationStatisticsController;
import app.domain.model.Company;
import app.domain.model.VaccinationStatistic;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;


public class CheckAndExportVaccinationStatisticsUI implements Runnable {


    static Scanner scanner = new Scanner(System.in);
    private String initialDate;
    private String finalDate;
    private int vc;

    int check;

    static CheckAndExportVaccinationStatisticsController checkAndExportVaccinationStatisticsController = new CheckAndExportVaccinationStatisticsController();

    private final Company company;

    /**
     * Instantiates a new Check and export vaccination statistics ui.
     *
     * @param vc the vc
     */
    public CheckAndExportVaccinationStatisticsUI (int vc) {
        this.vc = vc;
        company = App.getInstance().getCompany();
    }

    @Override
    public void run() {
        VaccinationStatistic vaccinationStatistic = new VaccinationStatistic();
        String confirm = "N";
        while (confirm.toUpperCase(Locale.ROOT).equals("N")) {
            System.out.println("\n===================================================");
            System.out.println("Please choose the date interval to analyze:");
            check = 0;
            do {
                try {
                    System.out.print("Initial date (dd/MM/yyyy):");
                    this.initialDate = scanner.nextLine();
                    vaccinationStatistic.setDate(this.initialDate);
                    check = 1;
                } catch (IllegalArgumentException error) {
                    System.out.println("\n======================= Insert a valid date (dd/MM/yyyy) =======================\n");
                }
            } while (check == 0);

            check = 0;
            do {
                try {
                    System.out.print("Final date (dd/MM/yyyy):");
                    this.finalDate = scanner.nextLine();
                    vaccinationStatistic.setDate(this.finalDate);
                    check = 1;
                } catch (IllegalArgumentException | InputMismatchException error) {
                    System.out.println("\n======================= Insert a valid date (dd/MM/yyyy) =======================\n");
                }
            } while (check == 0);

            System.out.println("\nConfirm selected date interval (Y/N): ");
            confirm = scanner.nextLine().toUpperCase(Locale.ROOT);
            while (!confirm.equals("Y") & !confirm.equals("N")) {
                System.out.println("\n======================= Invalid character, type again =======================\n");
                System.out.println("\nConfirm selected date interval (Y/N): ");
                confirm = scanner.nextLine().toUpperCase(Locale.ROOT);
            }
        }
        String[] separatedInitialDate = initialDate.split("/");
        if (separatedInitialDate.length==3) {
            if (separatedInitialDate[0].length() == 1 && separatedInitialDate[1].length() == 2) {
                initialDate = "0" + separatedInitialDate[0] + "/" + separatedInitialDate[1] + "/" + separatedInitialDate[2];
            }
            if (separatedInitialDate[0].length() == 2 && separatedInitialDate[1].length() == 1) {
                initialDate = separatedInitialDate[0] + "/0" + separatedInitialDate[1] + "/" + separatedInitialDate[2];
            }
            if (separatedInitialDate[0].length() == 1 && separatedInitialDate[1].length() == 1) {
                initialDate = "0" + separatedInitialDate[0] + "/0" + separatedInitialDate[1] + "/" + separatedInitialDate[2];
            }
        }
        String[] separatedFinalDate = finalDate.split("/");
        if (separatedFinalDate.length==3) {
            if (separatedFinalDate[0].length() == 1 && separatedFinalDate[1].length() == 2) {
                finalDate = "0" + separatedFinalDate[0] + "/" + separatedFinalDate[1] + "/" + separatedFinalDate[2];
            }
            if (separatedFinalDate[0].length() == 2 && separatedFinalDate[1].length() == 1) {
                finalDate = separatedFinalDate[0] + "/0" + separatedFinalDate[1] + "/" + separatedFinalDate[2];
            }
            if (separatedFinalDate[0].length() == 1 && separatedFinalDate[1].length() == 1) {
                finalDate = "0" + separatedFinalDate[0] + "/0" + separatedFinalDate[1] + "/" + separatedFinalDate[2];
            }
        }
        long numberOfDays=0;

        try {
            numberOfDays = checkAndExportVaccinationStatisticsController.createVaccinationStatistics(initialDate, finalDate, vc);
            checkAndExportVaccinationStatisticsController.checkVaccinationStatistics(numberOfDays, initialDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String confirm2;
        System.out.println("\nDo you want to export this statistics to a csv file? (Y/N)");
        confirm2 = scanner.nextLine().toUpperCase(Locale.ROOT);
        while (!confirm2.equals("Y") & !confirm2.equals("N")) {
            System.out.println("\n======================= Invalid character, type again =======================\n");
            System.out.println("\nDo you want to export this statistics to a csv file? (Y/N)");
            confirm2 = scanner.nextLine().toUpperCase(Locale.ROOT);
        }

        if (confirm2.toUpperCase(Locale.ROOT).equals("Y")) {
            confirm = "N";
            String fileName = "statistic";
            while (confirm.toUpperCase(Locale.ROOT).equals("N")) {
                System.out.println("\n===================================================");
                System.out.println("Please choose the name for your vaccination statistics csv:");
                fileName = scanner.nextLine();

                System.out.println("\nConfirm the name (Y/N): ");
                confirm = scanner.nextLine().toUpperCase(Locale.ROOT);
                while (!confirm.equals("Y") & !confirm.equals("N")) {
                    System.out.println("\n======================= Invalid character, type again =======================\n");
                    System.out.println("\nConfirm the name (Y/N): ");
                    confirm = scanner.nextLine().toUpperCase(Locale.ROOT);
                }
            }

            try {
                checkAndExportVaccinationStatisticsController.exportVaccinationStatistics(fileName, numberOfDays, initialDate);
                System.out.println(fileName + ".csv was exported successfully!");
            } catch (FileNotFoundException | ParseException e) {
                e.printStackTrace();
            }
        }


    }
}
