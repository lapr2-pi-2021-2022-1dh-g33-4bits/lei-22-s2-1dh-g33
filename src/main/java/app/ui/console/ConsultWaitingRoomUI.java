package app.ui.console;

import app.controller.ConsultWaitingRoomController;
import app.mappers.dto.SNSUserDTO;

import java.util.List;

public class ConsultWaitingRoomUI implements Runnable {

    public int vc;
    ConsultWaitingRoomController ctrll = new ConsultWaitingRoomController();

    public ConsultWaitingRoomUI(int vc) {
        this.vc = vc;
    }

    @Override
    public void run() {

        System.out.println("************************************");
        List<SNSUserDTO> list = ctrll.getWaitingRoom(vc);
        if (list.isEmpty()){
            System.out.println("Waiting Room is empty");
            System.out.println("************************************");
        }
        else {
            System.out.println("Waiting Room: ");
            System.out.println("************************************");
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i).toString());
            }
            System.out.println("************************************");
        }

    }
}
