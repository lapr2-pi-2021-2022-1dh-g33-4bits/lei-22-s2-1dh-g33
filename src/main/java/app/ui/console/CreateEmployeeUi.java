package app.ui.console;

import app.controller.App;
import app.controller.CreateEmployeeController;
import app.domain.model.Employee;
import app.ui.console.utils.Utils;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.io.IOException;

public class CreateEmployeeUi implements Runnable {

    static Scanner scanner = new Scanner(System.in);

    private CreateEmployeeController cec = new CreateEmployeeController();

    private CreateEmployeeController ece = new CreateEmployeeController(App.getInstance().getCompany());

    private String name;
    private String address;
    private String emailAddress;
    private String phoneNumber;
    private String socCode;
    private String id;
    private String NIF;

    int check;
    public void run() {
        Employee es = new Employee();
        cec.addOrganizationRoles();
        System.out.println("\n\n************************************");
        System.out.println("Available roles: ");
        System.out.println("************************************");
        int index = 0;
        int i = 0;
        for(Object o : cec.getOrganizationRoles()){
            index++;
            System.out.println(index+ ". " + cec.getOrganizationRoles().get(i).getId());
            i++;
        }
        System.out.println("************************************");
        System.out.println("0 - cancel");


        System.out.println("************************************\n");
        int option = Utils.selectsIndex(cec.getOrganizationRoles());
        String role = cec.getOrganizationRoles().get(option).getId();

        check=0;
        do {
            try {
                System.out.println("\n************************************\n");
                System.out.println("Enter name: ");
                this.name = scanner.nextLine();
                es.setName(this.name);
                check=1;
            }catch (IllegalArgumentException error){
                System.out.println("Insert a valid name");
            }
        } while (check == 0);

        check=0;
        do{
            try{
                System.out.println("\n************************************\n");
                System.out.println("Enter address: ");
                this.address = scanner.nextLine();
                es.setAddress(this.address);
                check=1;
            } catch (IllegalArgumentException error){
                System.out.println("Insert a valid address: ");
            }
        }while (check==0);

        check=0;
        do{
            try{
                System.out.println("\n************************************\n");
                System.out.println("Enter email address. (ex:___@lei.sem2.pt)");
                this.emailAddress = scanner.nextLine();
                es.setEmailAddress(this.emailAddress);
                check=1;
            } catch (IllegalArgumentException error){
                System.out.println("Insert a valid email address: ");
            }
        }while (check==0);
    /*
        System.out.println("\n************************************\n");
        System.out.println("Enter email address. (ex:___@lei.sem2.pt)");
        String emailAddress = scanner.nextLine();
*/

        check=0;
        do{
            try{
                System.out.println("\n************************************\n");
                System.out.println("Enter phone number: (9 digits)");
                this.phoneNumber = scanner.nextLine();
                es.setPhoneNumber(this.phoneNumber);
                check=1;
            } catch (IllegalArgumentException error){
                System.out.println("Insert a valid phone number: ");
            }
        }while (check==0);
        /*
        System.out.println("\n************************************\n");
        System.out.println("Enter phone number: (12 digits)");
        String phoneNumber = scanner.nextLine();
*/
        check=0;
        do{
            try{
                System.out.println("\n************************************\n");
                System.out.println("Enter Standard occupational classification code: (ex: 12-3456)");
                this.socCode = scanner.nextLine();
                es.setSocCode(this.socCode);
                check=1;
            } catch (IllegalArgumentException error){
                System.out.println("Insert a valid Standard occupational classification code: ");
            }
        }while (check==0);
/*
        System.out.println("\n************************************\n");
        System.out.println("Enter Standard occupational classification code: (ex: 12-3456)");
        String socCode = scanner.nextLine();
*/
        check=0;
        do{
            try{
                System.out.println("\n************************************\n");
                System.out.println("Enter ID: (7 digits)");
                this.id = scanner.nextLine();
                es.setId(this.id);
                check=1;
            } catch (IllegalArgumentException error){
                System.out.println("Insert a valid ID: ");
            }
        }while (check==0);
        /*
        System.out.println("\n************************************\n");
        System.out.println("Enter ID: (7 digits)");
        String id = scanner.nextLine();
*/
        check=0;
        do{
            try{
                System.out.println("\n************************************\n");
                System.out.println("Enter NIF: (9 digits)");
                this.NIF = scanner.nextLine();
                es.setNIF(this.NIF);
                check=1;
            } catch (IllegalArgumentException error){
                System.out.println("Insert a valid ID: ");
            }
        }while (check==0);
        /*
        System.out.println("\n************************************\n");
        System.out.println("Enter NIF: (9 digits)");
        String NIF = scanner.nextLine();
*/

        System.out.println("\n************************************\n");
        cec.CreateEmployee(role,name,address,emailAddress,phoneNumber,socCode,id,NIF);


        System.out.println("Role: " + cec.getRole());
        System.out.println("\nName: " + cec.getEmployeeName());
        System.out.println("Address: " + cec.getEmployeeAddress());
        System.out.println("Email Address: " + cec.getEmployeeEmailAddress());
        System.out.println("Phone Number: " + cec.getPhoneNumber());
        System.out.println("Standard occupational classification code: " + cec.getSocCode());
        System.out.println("NIF: " + cec.getNIF());

        System.out.println("\n************************************\n");

        System.out.println("\nConfirm data: (Y/N)");
        String response = scanner.nextLine();

        while(!response.equalsIgnoreCase("Y") & !response.equalsIgnoreCase("N")){
            System.out.println("\n*********************** Invalid char, type again *************************\n");
            System.out.println("Confirm data: (Y/N)");
            response = scanner.nextLine();

        }

        if(response.equalsIgnoreCase("Y")){
            try{
                if(cec.saveEmployee()){
                    System.out.println(cec.EmployeeAuthToString());
                    System.out.println("\n*** EMPLOYEE REGISTER SUCCESSFULLY ***");
                } else {
                    System.out.println("\\n*** EMPLOYEE ALREADY EXISTS ***\"");
                }
            } catch (IOException er) {
                cec.notSaveEmployee();
                System.out.println("\\n*** EMPLOYEE NOT REGISTERED ***\"");
            }
        }

    }
}
