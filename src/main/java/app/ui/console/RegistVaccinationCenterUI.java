package app.ui.console;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;
import app.controller.RegistVaccinationCenterController;
import app.domain.model.VaccinationCenter;
import java.io.IOException;


public class RegistVaccinationCenterUI implements Runnable {

    static Scanner sc = new Scanner(System.in);
    static RegistVaccinationCenterController vacCont = new RegistVaccinationCenterController();

    private String name;
    private String address;
    private String phoneNumber;
    private String area;
    private String mailAddress;
    private String faxNumber;
    private String webAddress;
    private String openingHour;
    private String closingHour;
    private String slotDuration;
    private String maxVacSlot;
    int check;

    public RegistVaccinationCenterUI (){
    }

    public void run() {
        VaccinationCenter vac = new VaccinationCenter();

        System.out.println("\n===================================================");
        System.out.println("Type the following attributes to register a vaccination center.");

        check=0;
        do {
            try {
                System.out.print("Name (i.e. Pedras Rubras Center):");
                this.name = sc.nextLine();
                vac.setName(this.name);
                check=1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid name =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Address (i.e. Rua do Casinha):");
                this.address = sc.nextLine();
                vac.setAddress(this.address);
                check=1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Insert a valid address =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Phone Number (12 digits):");
                this.phoneNumber = sc.nextLine();
                vac.setPhoneNumber(this.phoneNumber);
                check=1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid Phone Number =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Area (i.e. Porto):");
                this.area = sc.nextLine();
                vac.setArea(this.area);
                check=1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid area =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Mail Address (i.e. pedrasrubrascenter@gmail.com):");
                this.mailAddress = sc.nextLine();
                vac.setMailAddress(this.mailAddress);
                check=1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Insert a valid Mail Address =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Fax Number (i.e. 123456):");
                this.faxNumber = sc.nextLine();
                vac.setFaxNumber(this.faxNumber);
                check=1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid Fax Number =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Web Address (i.e. www.pedrasrubrascenter.pt):");
                this.webAddress = sc.nextLine();
                vac.setWebAddress(this.webAddress);
                check=1;
            } catch (IllegalArgumentException error) {
                System.out.println("\n======================= Insert a valid Web Address =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Opening Hour (i.e. 14):");
                this.openingHour = sc.nextLine();
                vac.setOpeningHour(this.openingHour);
                check=1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid Opening Hour =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Closing Hour (i.e. 23):");
                this.closingHour = sc.nextLine();
                vac.setClosingHour(this.closingHour);
                check=1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid Closing Hour =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Slot Duration (i.e. 4):");
                this.slotDuration = sc.nextLine();
                vac.setSlotDuration(this.slotDuration);
                check=1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid Slot Duration =======================\n");
            }
        } while (check==0);

        check=0;
        do {
            try {
                System.out.print("Maximum Vaccines per Slot (i.e. 7):");
                this.maxVacSlot = sc.nextLine();
                vac.setMaxVacSlot(this.maxVacSlot);
                check=1;
            } catch (IllegalArgumentException | InputMismatchException error) {
                System.out.println("\n======================= Insert a valid amount for Maximum Vaccines per Slot =======================\n");
            }
        } while (check==0);

        System.out.println("\n===================================================\n");


        try{
            vacCont.registerVaccinationCenter(name, address, phoneNumber, area, mailAddress, faxNumber, webAddress, openingHour, closingHour, slotDuration, maxVacSlot);
            vacCont.validateVaccinationCenter(name,address,phoneNumber,area,mailAddress,faxNumber,webAddress,openingHour,closingHour,slotDuration,maxVacSlot);

            showData(name, address, phoneNumber, area, mailAddress, faxNumber, webAddress, openingHour, closingHour, slotDuration, maxVacSlot);
            System.out.println("\nConfirm data: (Y/N) ");
            String confirm = sc.nextLine().toUpperCase(Locale.ROOT);
            while (!confirm.equals("Y") & !confirm.equals("N")) {
                System.out.println("\n======================= Invalid character, type again =======================\n");
                showData(name, address, phoneNumber, area, mailAddress, faxNumber, webAddress, openingHour, closingHour, slotDuration, maxVacSlot);
                System.out.print("Confirm data: (Y/N) ");
                confirm = sc.nextLine().toUpperCase(Locale.ROOT);
            }
            if (confirm.equalsIgnoreCase("Y")) {
                vacCont.saveNewVaccinationCenter();
                System.out.print("\n*** SAVED SUCCESSFULLY ***");
            } else {
                System.out.print("\n*** UNSAVED ***");
            }
        } catch (IllegalStateException error ) {
            System.out.println("\n======================= This Vaccination Center already exists =======================\n");
        }
    }
    public void showData (String name, String address, String phoneNumber, String area, String mailAddress, String faxNumber, String webAddress, String openingHour, String closingHour, String slotDuration, String maxVacSlot) {
        System.out.println("\n-------------------------");
        System.out.println("| Name: " + name);
        System.out.println("| Address: " + address);
        System.out.println("| Phone Number: " + phoneNumber);
        System.out.println("| Area: " + area);
        System.out.println("| Mail Address: " + mailAddress);
        System.out.println("| Fax Number: " + faxNumber);
        System.out.println("| Web Address: " + webAddress);
        System.out.println("| Opening Hour: " + openingHour);
        System.out.println("| Closing Hour: " + closingHour);
        System.out.println("| Slot Duration: " + slotDuration);
        System.out.println("| Maximum Vaccines per Slot: " + maxVacSlot);
        System.out.println("-------------------------\n");
    }
}
