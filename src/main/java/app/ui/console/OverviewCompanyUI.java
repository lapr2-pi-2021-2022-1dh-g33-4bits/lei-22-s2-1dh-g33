package app.ui.console;

import app.controller.OverviewCompanyController;
import app.domain.model.VaccinationData;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;
import java.util.Scanner;

public class OverviewCompanyUI implements Runnable {
    static Scanner scanner = new Scanner(System.in);

    OverviewCompanyController overviewCompanyController = new OverviewCompanyController();

    public void run() {
        try{

            System.out.println("Insert a date (DD/MM/YYYY): ");
            String date = scanner.nextLine();
            System.out.println("Insert now the interval of analyse (x minutes): ");
            String minutes = scanner.nextLine();
            int minutes1 = Integer.parseInt(minutes);
            List <VaccinationData> vaccinationDataList = overviewCompanyController.getVaccinationDataList(date);
            int [] sequence = overviewCompanyController.setInterval(minutes,vaccinationDataList);
            int [] subsequence = overviewCompanyController.sum(sequence);
            System.out.println("*************  Maximum Sum Contiguous Sublist ************");
            System.out.print( " [ ");
            for ( int i = 0 ; i < subsequence.length; i++){
                System.out.print(subsequence[i]);
                if (i != subsequence.length-1) {
                    System.out.print(", ");
                }
            }
            System.out.print(" ]");
            System.out.println();
            int sum  = overviewCompanyController.sumSequence(subsequence);
            System.out.println("**************** Sum *****************");
            System.out.println(sum);
            System.out.println("**************** Interval ****************");
            System.out.println("* " + overviewCompanyController.getFirstHour(sequence,subsequence,minutes1));
            System.out.println("* " + overviewCompanyController.getLastHour(subsequence,minutes1,overviewCompanyController.getFirstHour(sequence,subsequence,minutes1)));

        } catch (ParseException | IOException | ClassNotFoundException | InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }
}
