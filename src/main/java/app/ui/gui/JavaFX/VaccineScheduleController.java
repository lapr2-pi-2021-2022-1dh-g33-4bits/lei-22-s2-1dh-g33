package app.ui.gui.JavaFX;

import app.controller.App;
import app.controller.RegisterSNSUserController;
import app.controller.SendConfirmationMessageController;
import app.domain.model.Company;
import app.ui.gui.WindowChanges.newStage;
import javafx.beans.InvalidationListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.*;

public class VaccineScheduleController implements Initializable {

    private Company company;
    private String vacCenter;
    private String snsUserNumber;
    private String time;
    private String date;
    private String vacType;
    static app.controller.VaccineScheduleController vaccineScheduleController = new app.controller.VaccineScheduleController();
    static SendConfirmationMessageController sendMessageController = new SendConfirmationMessageController();
    static RegisterSNSUserController registerSNSUserController = new RegisterSNSUserController();

    @FXML
    private TextField snsUserNumberField;

    @FXML
    private TextField timeField;

    @FXML
    private DatePicker datePicker;

    @FXML
    private CheckBox checkBox;


/*
    public VaccineScheduleController(Company company) {
        this.company = company;
    }

 */

    @FXML
    private ChoiceBox<String> choiceBoxVacCenter = new ChoiceBox<>();

    @FXML
    private ChoiceBox<String> choiceBoxVacType = new ChoiceBox<>();



    @FXML
    void Logout(ActionEvent event) {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/Login.fxml"));
            Parent root = fxmlLoader.load();
            stage.initStyle(StageStyle.UNDECORATED);
            fxmlLoader.setRoot(new AnchorPane());

            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e){
            e.printStackTrace();
            e.getCause();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        company = App.getInstance().getCompany();
        String[] vaccinationCenter = new String[company.getVaccinationCenterStore().getVaccinationCenterList().size()];
        String[] vaccineType = new String[company.getVaccineTypesStore().getVaccineTypesList().size()];
        for (int i = 0; i < company.getVaccinationCenterStore().getVaccinationCenterList().size();i++) {
            vaccinationCenter[i] = (this.company.getVaccinationCenterStore().getVaccinationCenterList().get(i).getName());
        }
        choiceBoxVacCenter.getItems().addAll(vaccinationCenter);
        choiceBoxVacCenter.setOnAction(this::getVacCenter);

        for (int i = 0; i < company.getVaccineTypesStore().getVaccineTypesList().size();i++) {
            vaccineType[i] = (this.company.getVaccineTypesStore().getVaccineTypesList().get(i).getVaccineType());
        }
        choiceBoxVacType.getItems().addAll(vaccineType);

        choiceBoxVacType.setOnAction(this::getVaccineType);

    }
    @FXML
    void schedule(ActionEvent event){
        String asd = choiceBoxVacType.getValue();
        try {
            getSnsUserNumber();
            getTime();
            getDate();
            vaccineScheduleController.vaccineSchedule(snsUserNumber, vacCenter, date, time, vacType);
            vaccineScheduleController.validateNewVaccineSchedule(snsUserNumber, vacCenter, date, time, vacType);
            vaccineScheduleController.saveVaccineSchedule();
            vaccineScheduleController.saveVaccineHistory();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    void SMS(ActionEvent event) throws FileNotFoundException {
        if (checkBox.isSelected()) {
            sendMessageController.sendMessage(registerSNSUserController.getNameWithSNSUserNumber(snsUserNumber), snsUserNumber, vacCenter, date, time, vacType);
            PrintWriter printWriter = new PrintWriter("SMS.txt");
            printWriter.println(vaccineScheduleController.showData(snsUserNumber, vacCenter, date, time, vacType));
            printWriter.close();
        }
    }


    public void getVacCenter (ActionEvent event){
        vacCenter = choiceBoxVacCenter.getValue();
    }

    public void getVaccineType (ActionEvent event){
        vacType = choiceBoxVacCenter.getValue();
    }

    @FXML
    void getSnsUserNumber(){
        this.snsUserNumber = snsUserNumberField.toString();
    }

    @FXML
    void getTime(){
        this.time = timeField.toString();
    }

    @FXML
    void getDate(){
        date = datePicker.toString();
    }
}

