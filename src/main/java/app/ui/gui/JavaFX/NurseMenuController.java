package app.ui.gui.JavaFX;

import app.ui.gui.WindowChanges.newStage;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class NurseMenuController {

    @FXML
    public void Logout(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/Login.fxml")));
            new newStage(event, root);

        } catch (IOException e) {
            e.printStackTrace();
            e.getCause();
        }
    }


    @FXML
    public void consultUserWaitingRoom(ActionEvent event) {
       hideStage(event);
       //new ConsultWaitingRoomUI().run();
       showStage(event);
    }

    @FXML
    public void recordAdverseReactions(ActionEvent event) {
        hideStage(event);
        //new RecordAdverseReactionsUI();
        showStage(event);
    }

    @FXML
    void recordAdministrationVaccine(ActionEvent event) {
        hideStage(event);
        //new RecordVaccinationUI();
        showStage(event);
    }

    public void hideStage(ActionEvent event) {
        Stage Stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Stage.hide();
    }

    public void showStage(ActionEvent event) {
        Stage Stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Stage.show();
    }

    @FXML
    public void exit(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }
}
