package app.ui.gui.JavaFX;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class LoadingScreenController extends javax.swing.JFrame implements Initializable {

    @FXML
    private ProgressBar LoadingBar;

    @FXML
    private ProgressIndicator LoadingValue;

    @FXML
    private ImageView image;

    @FXML
    private StackPane rootPane;

    @FXML
    private Label title;

    private double xOffset = 0;
    private double yOffset = 0;


    public void initialize(URL url, ResourceBundle rb){
        //setTextAnimation(title);
        LoadingBar.setProgress(0.0);
        LoadingValue.setProgress(0.0);
        new bg_Thread().start();

    }

    /*
    public void setTextAnimation(Label title){
        String addWord = "VIBRANT ANALYSIS";
        title.setFont(Font.font("BlackChancery", FontPosture.REGULAR,87));
        title.setTextFill(Color.BLACK);
        title.setText("");

        final IntegerProperty i = new SimpleIntegerProperty(0);
        Timeline timeline = new Timeline();

        KeyFrame keyFrame = new KeyFrame(
                Duration.seconds(0.3),
                event -> {
                    if(i.get() > addWord.length()) {
                        timeline.stop();
                    } else {
                        title.setText((addWord.substring(0, i.get())));
                        i.set(i.get() + 1);
                    }
                });

        timeline.getKeyFrames().add(keyFrame);
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

     */



    class bg_Thread extends Thread {

        @Override
        public void run() {
            for(int i = 0; i < 101; i++) {
                try {
                    Thread.sleep(65);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                LoadingValue.setProgress(i/100.0);
                LoadingBar.setProgress(i/100.0);
            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    Parent root = null;

                    FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/Login.fxml"));
                    try {
                        root = fxmlLoader.load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Stage stage = new Stage();
                    stage.initStyle(StageStyle.UNDECORATED);
                    fxmlLoader.setRoot(new AnchorPane());

                    stage.setTitle("Vibrant Analysis");
                    stage.setScene(new Scene(root));
                    stage.show();

                    Scene scene = new Scene(root);

                    root.setOnMousePressed(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent mouseEvent) {
                            xOffset = mouseEvent.getSceneX();
                            yOffset = mouseEvent.getScreenY();
                        }
                    });

                    root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent mouseEvent) {
                            stage.setX(mouseEvent.getSceneX() - xOffset);
                            stage.setY(mouseEvent.getSceneX() - yOffset);
                        }
                    });

                    stage.setScene(scene);
                    stage.show();

                    rootPane.getScene().getWindow().hide();
                }
            });
        }
    }
}
