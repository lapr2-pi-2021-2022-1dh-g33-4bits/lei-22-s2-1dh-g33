package app.ui.gui.JavaFX;

public class InfoTableEmployees {

    String name ;
    String address;
    String emailadress;
    String phonenumber;
    String soccode;
    String id;
    String nif;

    public InfoTableEmployees(String name, String adress, String emailadress, String phonenumber, String soccode, String id, String nif){
        this.address = adress;
        this.emailadress = emailadress;
        this.phonenumber = phonenumber;
        this.soccode = soccode;
        this.name = name;
        this.id = id;
        this.nif = nif;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailadress() {
        return emailadress;
    }

    public void setEmailadress(String emailadress) {
        this.emailadress = emailadress;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getSoccode() {
        return soccode;
    }

    public void setSoccode(String soccode) {
        this.soccode = soccode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }
}
