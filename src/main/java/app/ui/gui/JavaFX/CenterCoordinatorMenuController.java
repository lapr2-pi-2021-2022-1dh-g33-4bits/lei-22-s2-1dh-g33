package app.ui.gui.JavaFX;

import app.ui.console.AnalyzePerformanceUI;
import app.ui.console.CheckAndExportVaccinationStatisticsUI;
import app.ui.console.ImportDataUI;
import app.ui.gui.WindowChanges.newStage;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class CenterCoordinatorMenuController {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private ImageView image;

    @FXML
    private Label loginMessageLabel;

    @FXML
    private Label loginMessageLabel1;

    @FXML
    private Label loginMessageLabel11;

    @FXML
    void Logout(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/Login.fxml")));
            new newStage(event, root);

        } catch (IOException e) {
            e.printStackTrace();
            e.getCause();
        }
    }

    @FXML
    void analyzeCenterPerformance(ActionEvent event) {
        hideStage(event);
        new AnalyzePerformanceUI().run();
        hideStage(event);
    }

    @FXML
    void checkAndExport(ActionEvent event) {
        hideStage(event);
        //new CheckAndExportVaccinationStatisticsUI().run();
        hideStage(event);

    }

    @FXML
    void importDataLegacySystem(ActionEvent event) {
        hideStage(event);
        new ImportDataUI();
        hideStage(event);

    }

    public void hideStage(ActionEvent event) {
        Stage Stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Stage.hide();
    }

    public void showStage(ActionEvent event) {
        Stage Stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Stage.show();
    }

    @FXML
    public void exit(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

}
