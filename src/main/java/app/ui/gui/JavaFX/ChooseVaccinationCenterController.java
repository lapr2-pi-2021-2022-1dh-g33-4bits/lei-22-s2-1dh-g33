package app.ui.gui.JavaFX;

import app.domain.model.Company;
import app.ui.gui.WindowChanges.newStage;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class ChooseVaccinationCenterController implements Initializable {

    private String vacCenter;
    private Company company;


    @FXML
    private Button Logout;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ChoiceBox<String> choiceBoxVaccinationCenter;
    @FXML
    private Button confirmVaccinationCenter;
    @FXML
    private ImageView image;

    public ChooseVaccinationCenterController (Company company) {
        this.company = company;
    }

    @FXML
    void Logout(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/ScheduleVaccine.fxml")));
            new newStage(event, root);
        } catch (IOException e){
            e.printStackTrace();
            e.getCause();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        for (int i = 0; i < company.getVaccinationCenterStore().getVaccinationCenterList().size();i++) {
            choiceBoxVaccinationCenter.getItems().add(this.company.getVaccinationCenterStore().getVaccinationCenterList().get(i).getName());
        }
        choiceBoxVaccinationCenter.setOnAction(this::getVacCenter);

    }

    @FXML
    void chooseVaccinationCenter(MouseEvent event) {

    }

    @FXML
    void confirmVaccinationCenter(ActionEvent event) {
        getVacCenter(event);
    }

    public void getVacCenter (ActionEvent event){
        vacCenter = choiceBoxVaccinationCenter.getValue();
    }


}
