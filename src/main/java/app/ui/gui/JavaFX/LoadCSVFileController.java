package app.ui.gui.JavaFX;

import app.domain.model.SNSUser;
import app.domain.shared.Constants;
import app.mappers.dto.SNSUserDTO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class LoadCSVFileController {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private Button cancel;

    @FXML
    private Label example;

    @FXML
    private TextField directoryTextField;

    @FXML
    private ImageView image;

    @FXML
    private Label montra;

    @FXML
    private Button load;

    static app.controller.LoadCSVFileController loadCSVFileController = new app.controller.LoadCSVFileController();
    private List<SNSUserDTO> snsUserLoadingList = new ArrayList<>();
    private SNSUserDTO snsUser;
    private String name;
    private String sex;
    private String birthDate;
    private String address;
    private String phoneNumber;
    private String email;
    private String snsUserNumber;
    private String citizenCardNumber;
    int successfullImportCounter = 0;
    int failedImportCounter=0;
    int fileID = 0;


    @FXML
    void goBack(ActionEvent event) {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/ADMINISTRATORMenu.fxml"));
            Parent root = fxmlLoader.load();
            stage.initStyle(StageStyle.UNDECORATED);
            fxmlLoader.setRoot(new AnchorPane());

            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            e.getCause();
        }
    }

    @FXML
    void load (ActionEvent event) throws IOException {
        String directory = directoryTextField.getText();
        File setOfSNSUsersFile = new File(directory + ".csv");
        int confirmation = 0;

        Scanner scan = null;

        try {
            scan = new Scanner(setOfSNSUsersFile);
        } catch (FileNotFoundException e) {
            example.setText("The file you entered isn't accepted");
            directoryTextField.setText("");
            confirmation = 1;
            e.printStackTrace();
        }

        String[] inputDataArray ;

        if (confirmation == 0) {

            int cont = 0;
            inputDataArray = scan.nextLine().split(";");
            if (inputDataArray.length == 8) {
                fileID=1;
            } else if (inputDataArray.length==1) {
                fileID=2;
            } else {
                example.setText("The file you entered isn't accepted");
                confirmation = 1;
            }

            if (confirmation == 0) {

                while (scan.hasNext()) {

                    if (fileID == 1) {
                        inputDataArray = scan.nextLine().split(";");
                    } else if (fileID == 2) {
                        if (cont == 0) {
                            inputDataArray = inputDataArray[0].split(",");
                        } else {
                            inputDataArray = scan.nextLine().split(",");
                        }
                    } else {
                        break;
                    }
                    snsUser = new SNSUserDTO();
                    loadCSVFileController.addOrganizationRoles();

                    name = inputDataArray[0];
                    sex = inputDataArray[1];
                    birthDate = inputDataArray[2];
                    address = inputDataArray[3];
                    phoneNumber = inputDataArray[4];
                    email = inputDataArray[5];
                    snsUserNumber = inputDataArray[6];
                    citizenCardNumber = inputDataArray[7];

                    try {
                        snsUser.setName(this.name);
                        snsUser.setSex(this.sex);
                        snsUser.setBirthDate(this.birthDate);
                        snsUser.setAddress(this.address);
                        snsUser.setPhoneNumber(this.phoneNumber);
                        snsUser.setEmail(this.email);
                        snsUser.setSnsUserNumber(this.snsUserNumber);
                        snsUser.setCitizenCardNumber(this.citizenCardNumber);

                        loadCSVFileController.LoadCSVFile(name, sex, birthDate, address, phoneNumber, email, snsUserNumber, citizenCardNumber);
                        loadCSVFileController.validateNewSNSUser(snsUser);
                        snsUserLoadingList.add(snsUser);
                        successfullImportCounter++;
                    } catch (IllegalArgumentException | IllegalStateException error) {
                        failedImportCounter++;
                        break;
                    } catch (IllegalCallerException userAlreadyExists) {
                        break;
                    }
                    cont++;
                }
                if (failedImportCounter == 0) {
                    loadCSVFileController.saveSNSUserLoadingList(snsUserLoadingList, "Y");
                    for (SNSUserDTO a: snsUserLoadingList) {
                        montra.setText(montra.getText() + "\n" + a.toString());
                    }
                } else {
                    montra.setText("\n======================= The file you entered wasn't loaded because some errors were found. =======================");
                }
            }


        }




    }

    public void example(MouseEvent mouseEvent) {
    }
}



