package app.ui.gui.JavaFX;

import app.ui.console.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import app.ui.gui.WindowChanges.newStage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Objects;

public class AdminMenuController {

    @FXML
    void Logout(ActionEvent event) {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/Login.fxml"));
            Parent root = fxmlLoader.load();
            stage.initStyle(StageStyle.UNDECORATED);
            fxmlLoader.setRoot(new AnchorPane());

            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e){
            e.printStackTrace();
            e.getCause();
        }
    }

    @FXML
    void newEmployeeAccount(ActionEvent event) {
        hideStage(event);
        new CreateEmployeeUi().run();
        showStage(event);
    }

    @FXML
    void newSnsUser(ActionEvent event) {
        hideStage(event);
        new RegisterSNSUserUI().run();
        showStage(event);
    }

    @FXML
    void newLoadUsers(ActionEvent event)  {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/LoadCsvFile.fxml"));
            Parent root = fxmlLoader.load();
            stage.initStyle(StageStyle.UNDECORATED);
            fxmlLoader.setRoot(new AnchorPane());
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            e.getCause();
        }
    }

    @FXML
    void newListOfEmployees(ActionEvent event) {
        hideStage(event);
        new RequestAListOfEmployeesUI().run();
        showStage(event);

    }

    @FXML
    void newVaccinationCenter(ActionEvent event) {
        hideStage(event);
        new RegistVaccinationCenterUI().run();
        showStage(event);
    }

    @FXML
    void newVaccineAndAdministration(ActionEvent event) {
        hideStage(event);
        new SpecifyNewVaccineUI().run();
        showStage(event);
    }

    @FXML
    void newVaccineType(ActionEvent event) {
        hideStage(event);
        new VaccineTypeUI().run();
        showStage(event);
    }


    public void hideStage(ActionEvent event) {
        Stage Stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Stage.hide();
    }

    public void showStage(ActionEvent event) {
        Stage Stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Stage.show();
    }

    @FXML
    public void exit(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

    public void handleMinButtonAction(ActionEvent event) {
    }

    public void handleMaxButtonAction(ActionEvent event) {
    }
}
