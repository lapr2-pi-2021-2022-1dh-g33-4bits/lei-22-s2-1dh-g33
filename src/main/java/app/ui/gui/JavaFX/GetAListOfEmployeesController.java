package app.ui.gui.JavaFX;

import app.domain.model.Employee;
import app.ui.gui.WindowChanges.newStage;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class GetAListOfEmployeesController {

    @FXML
    private TextArea taInformation;
    @FXML
    private ChoiceBox<String> choiceBox;


    @FXML
    private void initialize() {
        choiceBox.getItems().addAll("Nurse", "Receptionist", "Center Coordinator");

    }


    public void handleButtonAction(ActionEvent event){


        taInformation.appendText(choiceBox.getSelectionModel().getSelectedItem());

    }


    public void cancel(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/Login.fxml")));
            new newStage(event, root);

        } catch (IOException e) {
            e.printStackTrace();
            e.getCause();
        }
    }

    public void hideStage(ActionEvent event) {
        Stage Stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Stage.hide();
    }

    public void showStage(ActionEvent event) {
        Stage Stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Stage.show();
    }

    @FXML
    public void exit(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

}
