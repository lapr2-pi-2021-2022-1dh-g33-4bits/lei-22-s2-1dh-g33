package app.ui.gui.JavaFX;

import app.controller.App;
import app.domain.model.Company;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;


public class RequestAListOfEmployeesController {

    private Company company = App.getInstance().getCompany();
    public  ChoiceBox<String> choiceBoxVacCenter = new ChoiceBox<>();
    private int id;


    public class RequestAListOfEmployeesUI {

        public void exit(ActionEvent event) {
            Platform.exit();
            System.exit(0);
        }


        public void initialize(URL url, ResourceBundle resourceBundle) {


            for (int i = 0; i < company.getOrganizationRolesStore().getOrganizationRolesList().size(); i++) {
                choiceBoxVacCenter.getItems().add(company.getOrganizationRolesStore().getOrganizationRolesList().get(i).getId());
            }
            choiceBoxVacCenter.setOnAction(this::id);
        }

        public void id (ActionEvent event){
            id = Integer.parseInt(choiceBoxVacCenter.getValue());
        }


        @FXML
        private TableColumn<InfoTableEmployees, String> name;

        @FXML
        private TableColumn<InfoTableEmployees, String> adress;

        @FXML
        private TableColumn<InfoTableEmployees, String> emailadress;

        @FXML
        private TableColumn<InfoTableEmployees, String> phonenumber;

        @FXML
        private TableColumn<InfoTableEmployees, String> soccode;


        @FXML
        private TableColumn<InfoTableEmployees, String> nif;

        @FXML
        private TableView<InfoTableEmployees> table;

        @FXML
        public void showStage(ActionEvent event) {
            Stage Stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            Stage.show();
        }
    }

}
