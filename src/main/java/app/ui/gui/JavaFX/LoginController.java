package app.ui.gui.JavaFX;

import app.controller.App;
import app.controller.AuthController;
import app.domain.model.Company;
import app.ui.console.AuthUI;
import auth.AuthFacade;
import app.domain.store.OrganizationRolesStore;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LoginController {

    @FXML
    private TextField usernameTextField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label LoginMessageLabel;

    private Company company;
    static AuthController authController = new AuthController();

    private AuthFacade authFacade;

    private AuthUI authUI = new AuthUI();

    private List<String> list = new ArrayList<>();

    @FXML
    void Readme(ActionEvent event) {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/ReadMe.fxml"));
            Parent root = fxmlLoader.load();
            stage.initStyle(StageStyle.UNDECORATED);
            fxmlLoader.setRoot(new AnchorPane());
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            e.getCause();
        }
    }

    public LoginController() {
        company = App.getInstance().getCompany();
        authFacade = company.getAuthFacade();
        authFacade.doLogout();

        list.add("/fxml/ADMINISTRATORMenu.fxml");
        list.add("/fxml/ReceptionistMenu.fxml");
        list.add("/fxml/NURSEMenu.fxml");
        list.add("/fxml/CenterCoordinatorMenu.fxml");
        list.add("/fxml/SNS USERMenu.fxml");
    }

    @FXML
    public void loginButtonOnAction(ActionEvent event) {
        String username = usernameTextField.getText();
        String password = passwordField.getText();


        boolean success = false;
        success = authController.doLogin(username, password);
        if (!success) {
            LoginMessageLabel.setText("Invalid Username and/or Password.");
            usernameTextField.setText("");
            passwordField.setText("");
        }
        String role = authFacade.getCurrentUserSession().getUserRoles().get(0).getId();
        getUIForRoles(event,role);

        /*
        try {
            authController.doLogin(username, password);
            authFacade.doLogin(username,password).isLoggedIn();
            String role = authFacade.getCurrentUserSession().getUserRoles().get(0).getId();

            getUIForRoles(event,role);
        } catch (Exception e) {
            LoginMessageLabel.setText("Invalid Username and/or Password.");
            usernameTextField.setText("");
            passwordField.setText("");
        }
         */


    }

    private void getUIForRoles(ActionEvent event, String role) {
        OrganizationRolesStore organizationRolesStore = new OrganizationRolesStore();
        for(int i = 0; i < organizationRolesStore.getOrganizationRolesList().size(); i++) {
            if(organizationRolesStore.getOrganizationRolesList().get(i).getId().toLowerCase(Locale.ROOT).equals(role.toLowerCase(Locale.ROOT))){
                try {
                    Stage stage = new Stage();
                    FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/" + organizationRolesStore.getOrganizationRolesList().get(i).getId().toUpperCase(Locale.ROOT) + "Menu.fxml"));
                    Parent root = fxmlLoader.load();
                    stage.initStyle(StageStyle.UNDECORATED);
                    fxmlLoader.setRoot(new AnchorPane());
                    stage.setScene(new Scene(root));
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                    e.getCause();
                }
            }
        }
    }

    @FXML
    public void exit(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

    public void handleMinButtonAction(ActionEvent event) {
    }

    public void handleMaxButtonAction(ActionEvent event) {
    }
}
