package app.ui.gui.JavaFX;

import app.ui.gui.WindowChanges.newStage;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Objects;

public class ReadMeController {

    @FXML
    public void cancelButtonOnAction(ActionEvent event) {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/Login.fxml"));
            Parent root = fxmlLoader.load();
            stage.initStyle(StageStyle.UNDECORATED);
            fxmlLoader.setRoot(new AnchorPane());

            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            e.getCause();
        }
    }

    @FXML
    public void exit(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

    public void handleMaxButtonAction(ActionEvent event) {
    }

    public void handleMinButtonAction(ActionEvent event) {
    }
}
