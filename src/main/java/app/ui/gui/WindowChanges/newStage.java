package app.ui.gui.WindowChanges;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class newStage {

    private double xOffset = 0;
    private double yOffset = 0;

    public newStage() {
    }

    public newStage(ActionEvent event, Parent root) {
        Stage Stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Stage.setScene(new Scene(root));

        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                xOffset = mouseEvent.getSceneX();
                yOffset = mouseEvent.getSceneY();
            }
        });

        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                Stage.setX(mouseEvent.getScreenX() - xOffset);
                Stage.setY(mouseEvent.getScreenY() - yOffset);

            }
        });
    }

    public void handleMinButtonAction(Button maxButton, ActionEvent event) {

        Stage Stage = (Stage) maxButton.getScene().getWindow();
        Stage.setIconified(true);
    }

    public void handleMaxButtonAction(Button minButton, AnchorPane anchorPane, ImageView image, ActionEvent event) {

        Stage Stage = (Stage) minButton.getScene().getWindow();
        Stage.setFullScreen(true);
        anchorPane.setLayoutX(480);
        anchorPane.setLayoutY(208);
        image.setFitHeight(1080);
        image.setFitWidth(1920);
    }
}