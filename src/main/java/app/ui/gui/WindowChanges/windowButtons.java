package app.ui.gui.WindowChanges;

import app.Persistence;
import app.controller.App;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;


public class windowButtons {

    @FXML
    private Button minButton;

    @FXML
    private Button maxButton;

    @FXML
    private ImageView image;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    public void handleMaxButtonAction(ActionEvent event) {
        new newStage().handleMaxButtonAction(minButton, anchorPane, image, event);
    }

    @FXML
    public void handleMinButtonAction(ActionEvent event) {
        new newStage().handleMinButtonAction(maxButton, event);
    }

    public void exit(ActionEvent event) throws IOException {
        Persistence.saveObject(App.getInstance().getCompany().getAdverseReactionsStore().getAdverseReactionsList(), "adverseReactions.bin");
        Persistence.saveObject(App.getInstance().getCompany().getEmployeeStore().getEmployeeStore(), "employee.bin");
        Persistence.saveObject(App.getInstance().getCompany().getOrganizationRolesStore().getOrganizationRolesList(), "organizationsRoles.bin");
        Persistence.saveObject(App.getInstance().getCompany().getRecordVaccineStore().getRecordVaccineListToSerialization(), "recordVaccine.bin");
        Persistence.saveObject(App.getInstance().getCompany().getRecoveryRoomStore().getRecoveryRoomListToSerialization(), "recoveryRoom.bin");
        Persistence.saveObject(App.getInstance().getCompany().getSNSUserDataStore().getSNSUserList(), "snsUser.bin");
        Persistence.saveObject(App.getInstance().getCompany().getVaccinationCenterStore().getVaccinationCenterList(), "vaccinationCenter.bin");
        Persistence.saveObject(App.getInstance().getCompany().getVaccinationDataStore().getDataList(), "vaccinationData.bin");
        Persistence.saveObject(App.getInstance().getCompany().getVaccinationHistoryStore().getVaccineHistoryList(), "vaccinationHistory.bin");
        Persistence.saveObject(App.getInstance().getCompany().getVaccinationStatisticsStore().getVaccinationStatisticsList(), "vaccinationStatistics.bin");
        Persistence.saveObject(App.getInstance().getCompany().getVaccineScheduleStore().getVaccineScheduleList(), "vaccineSchedule.bin");
        Persistence.saveObject(App.getInstance().getCompany().getVaccineStore().getVaccineList(), "vaccine.bin");
        Persistence.saveObject(App.getInstance().getCompany().getVaccineTypesStore().getVaccineTypesList(), "vaccineTypes.bin");
        Persistence.saveObject(App.getInstance().getCompany().getWaitingRoomStore().getWaitingRoomListToSerialization(), "waitingRoom.bin");
        Persistence.saveObject(App.getInstance().getCompany().getAuthFacade().getUserRoles(), "userRole.bin");

        System.exit(0);
    }


}