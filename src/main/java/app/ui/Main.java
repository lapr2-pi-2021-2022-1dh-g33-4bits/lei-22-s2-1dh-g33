package app.ui;

import app.controller.DGSRecordDataController;
import app.ui.console.MainMenuUI;
import app.ui.gui.JavaFX.LoadingScreenController;
import app.ui.gui.JavaFX.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

//Teste
public class Main /*extends Thread */ extends Application {

    @Override
    public void start (Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/fxml/LoadingScreen.fxml"));
        Parent root = fxmlLoader.load();
        stage.initStyle(StageStyle.UNDECORATED);
        fxmlLoader.setRoot(new AnchorPane());

        stage.setTitle("Vibrant Analysis");
        stage.setScene(new Scene(root));
        stage.show();

    }




    public static void main(String[] args) throws IOException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        launch(args);
        Main thread = new Main();
        //thread.start();

        try {
            MainMenuUI menu = new MainMenuUI();

            menu.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

        public void run() {

        DGSRecordDataController dgsCtrl = new DGSRecordDataController();
        try {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        while (!((LocalTime.now().format(formatter)).equals(dgsCtrl.getHours()))) {

        }
        } catch (ClassNotFoundException | IOException | NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
        }

        try {
            File file = new File("Daily_Data_Record.csv");
        PrintWriter printWriter = new PrintWriter(file);
        StringBuilder stringBuilder = new StringBuilder();
            DateTimeFormatter dayFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");


            printWriter.println("DAY:" + LocalDateTime.now().format(dayFormat));
            printWriter.println();


        for(int i=0; i<dgsCtrl.vaccinationCenterNumber(); i++) {

            printWriter.print(dgsCtrl.getName(i) + ":\t");
            printWriter.println(dgsCtrl.getTotalNumberRecoveryRoom(i));

        }
        printWriter.flush();
        printWriter.close();

    } catch (IOException e) {
        System.out.println(e.getMessage());
    }
}}
