package app.mappers.dto;
import app.domain.model.SNSUser;

import java.io.Serializable;

public class SNSUserDTO implements Serializable {

    private static final long serialVersionUID = -5729542750061632789L;
    private String name;
    private String sex;
    private String birthDate;
    private String address;
    private String phoneNumber;
    private String email;
    private String snsUserNumber;
    private String citizenCardNumber;


    public SNSUserDTO(String name, String sex, String birthDate, String address, String phoneNumber, String email, String snsUserNumber, String citizenCardNumber) {
        this.name = name;
        this.sex = sex;
        this.birthDate = birthDate;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.snsUserNumber = snsUserNumber;
        this.citizenCardNumber = citizenCardNumber;
    }

    public SNSUserDTO(SNSUser snsUser) {
        this.name = snsUser.getName();
        this.sex = snsUser.getSex();
        this.birthDate = snsUser.getBirthDate();
        this.phoneNumber = snsUser.getPhoneNumber();
        this.snsUserNumber = snsUser.getSnsUserNumber();

    }

    public SNSUserDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }

    public String getCitizenCardNumber() {
        return citizenCardNumber;
    }

    public void setCitizenCardNumber(String citizenCardNumber) {
        this.citizenCardNumber = citizenCardNumber;
    }

    @Override
    public String toString() {
        return "SNS User - " + "Name: " + name+ ", SNS user number: " + snsUserNumber + ", Birth date: " + birthDate + ", Sex: " + sex + ", Phone number: " + phoneNumber
                ;
    }
}
