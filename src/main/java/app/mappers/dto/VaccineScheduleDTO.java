package app.mappers.dto;

import app.domain.model.VaccineSchedule;

import java.io.Serializable;

public class VaccineScheduleDTO implements Serializable {

    private static final long serialVersionUID = -6445075380083190186L;
    private String snsUserNumber;
    private String vaccinationCenterName;
    private String date;
    private String vacType;

    public VaccineScheduleDTO(String snsUserNumber, String vaccinationCenterName, String date, String vacType){
        this.snsUserNumber = snsUserNumber;
        this.vaccinationCenterName = vaccinationCenterName;
        this.date = date;
        this.vacType = vacType;
    }

    public VaccineScheduleDTO(VaccineSchedule vacSched){
        this.snsUserNumber = vacSched.getSnsUserNumber();
        this.vaccinationCenterName = vacSched.getVaccinationCenterName();
        this.date = vacSched.getDate();
        this.vacType = vacSched.getVacType();
    }

    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }

    public String getVaccinationCenterName() {
        return vaccinationCenterName;
    }

    public void setVaccinationCenterName(String vaccinationCenterName) {
        this.vaccinationCenterName = vaccinationCenterName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVacType() {
        return vacType;
    }

    public void setVacType(String vacType) {
        this.vacType = vacType;
    }

    @Override
    public String toString() {
        return "VaccineScheduleDTO{" +
                "snsUserNumber='" + snsUserNumber + '\'' +
                ", vaccinationCenterName='" + vaccinationCenterName + '\'' +
                ", date='" + date + '\'' +
                ", vacType='" + vacType + '\'' +
                '}';
    }
}
