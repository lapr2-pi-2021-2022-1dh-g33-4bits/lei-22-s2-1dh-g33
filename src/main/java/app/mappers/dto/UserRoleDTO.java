package app.mappers.dto;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import java.io.Serializable;

public class UserRoleDTO implements Serializable {

    private static final long serialVersionUID = 8259943039523807934L;
    private String id;
    private String description;

    public UserRoleDTO(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public String getId() {
        return this.id;
    }

    public String getDescription() {
        return this.description;
    }
}

