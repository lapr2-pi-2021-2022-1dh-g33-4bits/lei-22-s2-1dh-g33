package app.mappers;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.VaccineSchedule;
import app.domain.store.VaccineScheduleStore;
import app.mappers.dto.VaccineScheduleDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VaccineScheduleMapper implements Serializable {

    private static final long serialVersionUID = -6857981124853048294L;
    private final Company company;
    private final VaccineScheduleStore store;
    private List<VaccineScheduleDTO> VaccineScheduleDTO;
    private VaccineSchedule vacSched;

    public VaccineScheduleMapper() {
        this.company = App.getInstance().getCompany();
        this.store = company.getVaccineScheduleStore();
    }

    public List<VaccineScheduleDTO> toDTO(List<VaccineSchedule> vaccineScheduleList) {
        List<VaccineScheduleDTO> vaccineScheduleDTO = new ArrayList<>();
        for (VaccineSchedule vc : vaccineScheduleList) {
            vaccineScheduleDTO.add(toDTO(vc));
        }
        return vaccineScheduleDTO;
    }

    public VaccineScheduleDTO toDTO(VaccineSchedule vacSched) {
        this.vacSched = vacSched;
        return new VaccineScheduleDTO(vacSched);
    }

    public void toVaccineSchedule(VaccineScheduleDTO vaccineScheduleDTO) {

        try {
            if (!vaccineScheduleDTO.getSnsUserNumber().equals(vacSched.getSnsUserNumber())) {
                vacSched.setSnsUserNumber(vaccineScheduleDTO.getSnsUserNumber());
            } else if (!vaccineScheduleDTO.getVaccinationCenterName().equals(vacSched.getVaccinationCenterName())) {
                vacSched.setVaccinationCenterName(vaccineScheduleDTO.getVaccinationCenterName());
            } else if (!vaccineScheduleDTO.getDate().equals(vacSched.getDate())) {
                vacSched.setDate(vaccineScheduleDTO.getDate());
            } else if (!vaccineScheduleDTO.getVacType().equals(vacSched.getVacType())) {
                vacSched.setVacType(vaccineScheduleDTO.getVacType());
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("ERROR: " + e.getMessage());
        }
    }
}