package app.mappers;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import app.mappers.dto.UserRoleDTO;
import auth.domain.model.UserRole;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class UserRoleMapper implements Serializable {

    private static final long serialVersionUID = 1042088813036125345L;

    public UserRoleMapper() {
    }

    public UserRoleDTO toDTO(UserRole role) {
        return new UserRoleDTO(role.getId(), role.getDescription());
    }

    public List<UserRoleDTO> toDTO(List<UserRole> roles) {
        List<UserRoleDTO> rolesDTO = new ArrayList();
        Iterator var3 = roles.iterator();

        while(var3.hasNext()) {
            UserRole role = (UserRole)var3.next();
            rolesDTO.add(this.toDTO(role));
        }

        return rolesDTO;
    }

    public List<UserRoleDTO> toDTO(Set<UserRole> roles) {
        List<UserRoleDTO> rolesDTO = new ArrayList();
        Iterator var3 = roles.iterator();

        while(var3.hasNext()) {
            UserRole role = (UserRole)var3.next();
            rolesDTO.add(this.toDTO(role));
        }

        return rolesDTO;
    }
}
