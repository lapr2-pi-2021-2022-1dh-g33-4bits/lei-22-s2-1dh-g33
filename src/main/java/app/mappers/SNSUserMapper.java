package app.mappers;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.SNSUser;
import app.domain.store.SNSUserDataStore;
import app.mappers.dto.SNSUserDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SNSUserMapper implements Serializable {

    private static final long serialVersionUID = 5120621358063022802L;
    private final Company company;
    private final SNSUserDataStore SNSUserStore;
    private List<SNSUserDTO> SnsUserDTO;
    private SNSUser SnsUser;

    public SNSUserMapper() {
        this.company = App.getInstance().getCompany();
        this.SNSUserStore = company.getSNSUserDataStore();
    }

    public List<SNSUserDTO> toDTO(List<SNSUser> snsUserList){
        List<SNSUserDTO> SnsUserDTO = new ArrayList<>();
        for(SNSUser s : snsUserList){
            SnsUserDTO.add(toDTOSecundary(s));
        }
        return SnsUserDTO;
    }

    public SNSUserDTO toDTOSecundary(SNSUser SnsUser){
        this.SnsUser = SnsUser;
        return new SNSUserDTO(SnsUser);
    }

    public void toSnsUser(SNSUserDTO SnsUserDTO){

        try{
             if (!SnsUserDTO.getSnsUserNumber().equals(SnsUser.getSnsUserNumber())){
                SnsUser.setSnsUserNumber(SnsUserDTO.getSnsUserNumber());

            } else if (!SnsUserDTO.getBirthDate().equals(SnsUser.getBirthDate())){
                SnsUser.setBirthDate(SnsUserDTO.getBirthDate());

            } else if(!SnsUserDTO.getSex().equals(SnsUser.getSex())){
                SnsUser.setSex(SnsUserDTO.getSex());

            } else if(!SnsUserDTO.getPhoneNumber().equals(SnsUser.getPhoneNumber())){
                SnsUser.setPhoneNumber(SnsUserDTO.getPhoneNumber());


            } else if(!SnsUserDTO.getName().equals(SnsUser.getName())){
                SnsUser.setName(SnsUserDTO.getName());

            }
        } catch (Exception e) {
            throw new IllegalArgumentException("ERROR: " + e.getMessage());
        }
    }

}
