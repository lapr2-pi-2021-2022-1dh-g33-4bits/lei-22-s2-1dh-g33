package app;

import java.io.*;
import java.util.ArrayList;

public class Persistence {
    /**
     * Reads the object from file
     *
     * @param ficheiro file path / name
     * @return object loaded
     */
    public static Object readObjectFromFile(String ficheiro) throws IOException, ClassNotFoundException {
        ficheiro = "./storage/" + ficheiro;
        FileInputStream fileIn = new FileInputStream(ficheiro);
        ObjectInputStream in = new ObjectInputStream(fileIn);
        Object a = in.readObject();
        in.close();
        fileIn.close();
        return a;

    }

    public static void saveObject(Object objectToSave, String fileName) throws IOException {

        fileName = "./storage/" + fileName;
        new File(fileName).delete();
        FileOutputStream fileOut = new FileOutputStream(fileName);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(objectToSave);
        out.close();
        fileOut.close();
    }
}