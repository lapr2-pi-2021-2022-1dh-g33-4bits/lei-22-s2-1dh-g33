package app.subSequenceAlgorithm;

import com.isep.mdis.*;

public class Benchmark implements SubSequenceAlgorithm {
    /**
     * Adapter to enter in API
     * @param sequence of tests performance
     * @return biggest subsequence
     */
    @Override
    public int[] sum(int[] sequence) {
        return Sum.Max(sequence);
    }
}
