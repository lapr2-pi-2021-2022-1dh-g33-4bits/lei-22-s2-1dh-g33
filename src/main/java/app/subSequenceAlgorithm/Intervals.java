package app.subSequenceAlgorithm;

//import app.domain.model.Test;

import app.domain.model.VaccinationData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class Intervals {

    private final int WorkingHourInMinutes = 720;
    private final String firstHour = "08:00";
    private final String lastHour = "20:00";
    private int minutesInterval = 10;
    private String minutesIntervalTIME;
    private List <VaccinationData> vaccinationDataList;



    public Intervals(String minutesIntervalTIME, List <VaccinationData> vaccinationDataList) throws ParseException {
        this.minutesInterval = Integer.parseInt((minutesIntervalTIME));
        this.minutesIntervalTIME = minutesIntervalTIME;
        this.vaccinationDataList = vaccinationDataList;
    }

    public Intervals() {

    }

    /**
     * Counts the number total of periods between the hours introduced
     *
     * @return number of periods
     */
    private int numberTotalOfPeriods() {

        int numberOfPeriods = WorkingHourInMinutes / minutesInterval;
        return numberOfPeriods;
    }

    /**
     * Adds x minutes to the current minutes
     *
     * @param currentMinute current minutes
     * @return minutes after adding x minutes
     */
    public String addXMinutes(String currentMinute, int i) throws ParseException {
        GregorianCalendar gc = new GregorianCalendar();
        SimpleDateFormat format= new SimpleDateFormat("HH:mm");
        gc.setTime(format.parse(currentMinute));
        gc.add(Calendar.MINUTE,i);
        return format.format(gc.getTime());
    }

    /**
     * Creates a sequence with the number of periods size and sends it to fill
     *
     * @param dataList list of vaccination data
     * @return sequence filled
     */
    public int[] createSequence(List<VaccinationData> dataList) throws ParseException {
        int[] numberSequence = new int[numberTotalOfPeriods()];
        fillSequence(numberSequence, dataList);
        return numberSequence;
    }

    /**
     * fills the sequence with the vaccination data in each interval of x minutes
     * @param numberSequence sequence to be filled
     * @param dataList list of vaccination data
     */
    private void fillSequence (int [] numberSequence, List <VaccinationData> dataList) throws ParseException {
        String currentMinute;
        String nextXMinute;
        int numberOfUsersArriving = 0;
        int numberOfUsersLeaving = 0;
        currentMinute = firstHour;
        nextXMinute = addXMinutes(currentMinute, minutesInterval);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        for (int i = 0; i < numberSequence.length; i++) {
            Date dateActual = sdf.parse(currentMinute);
            Date limitDate = sdf.parse(nextXMinute);
            numberOfUsersArriving = 0;
            numberOfUsersLeaving = 0;
            for (VaccinationData data : dataList) {
                Date timeArrival = sdf.parse(data.getArrivalTime());
                if (timeArrival.equals(dateActual)){
                    numberOfUsersArriving++;
                }
                if (timeArrival.after(dateActual) && timeArrival.before(limitDate)) {
                    numberOfUsersArriving++;
                }
                Date leavingTime = sdf.parse(data.getLeavingTime());
                if (leavingTime.equals(dateActual)){
                    numberOfUsersLeaving++;
                }
                if (leavingTime.after(dateActual) && leavingTime.before(limitDate)){
                    numberOfUsersLeaving++;
                }

            }
            numberSequence[i] = ( numberOfUsersArriving - numberOfUsersLeaving);
            currentMinute = nextXMinute;
            nextXMinute = addXMinutes(currentMinute, minutesInterval);
        }
    }
}





