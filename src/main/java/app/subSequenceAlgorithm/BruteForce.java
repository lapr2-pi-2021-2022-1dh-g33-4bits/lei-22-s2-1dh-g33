package app.subSequenceAlgorithm;

import java.util.Arrays;

public class BruteForce implements SubSequenceAlgorithm {

    /**
     * Sends the array to  the brute force algorithm
     * @param sequence of tests performance
     * @return biggest subsequence
     */
    @Override
    public int[] sum(int[] sequence) {
        return Max(sequence);
    }

    /**
     * Determines the biggest subsequence
     * @param sequence of tests performance
     * @return biggest subsequence
     */
    public int[] Max(int[] sequence) {
        int sum;
        int max = sequence[0];
        int position = 0;
        int lastPosition = 0;
        int sumNext;

        for (int pairs = 0; pairs < sequence.length; pairs++) {
            for (int i = 0; i < sequence.length - pairs; i++) {
                sumNext = 0;
                for (int j = 0; j < pairs; j++) {
                    sumNext = sumNext + sequence[i + j + 1];
                }
                sum = sequence[i] + sumNext;
                if (sum > max) {
                    max = sum;
                    position = i;
                    lastPosition = i + pairs + 1;
                }
            }
        }
        return Arrays.copyOfRange(sequence, position, lastPosition);
    }
}