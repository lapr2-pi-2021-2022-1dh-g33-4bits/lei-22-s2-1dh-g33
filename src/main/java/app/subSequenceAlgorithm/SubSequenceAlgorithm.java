package app.subSequenceAlgorithm;


public interface SubSequenceAlgorithm {
    /**
     * Interface to access the API for finding the biggest subsequence
     * @param array of tests performance
     * @return biggest subsequence
     */
    public int[] sum(int[] array);
}