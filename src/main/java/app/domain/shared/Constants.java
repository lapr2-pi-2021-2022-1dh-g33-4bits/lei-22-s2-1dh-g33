package app.domain.shared;

import java.io.Serializable;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Constants implements Serializable {
    public static final String ROLE_ADMIN = "ADMINISTRATOR";
    public static final String ROLE_RECEPTIONIST = "RECEPTIONIST";
    public static final String ROLE_NURSE = "NURSE";
    public static final String ROLE_CENTERCOORDINATOR = "CENTER COORDINATOR";
    public static final String ROLE_SNSUSER = "SNS USER";


    public static final String PARAMS_FILENAME = "config.properties";
    public static final String PARAMS_COMPANY_DESIGNATION = "Company.Designation";

    private static final long serialVersionUID = 7663257620009702662L;
}
