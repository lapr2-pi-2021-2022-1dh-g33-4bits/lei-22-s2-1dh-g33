package app.domain.store;

import app.Persistence;
import app.domain.model.VaccineSchedule;

import java.util.ArrayList;
import java.util.List;

public class VaccineScheduleStore  {

    private List<VaccineSchedule> vaccineScheduleList = new ArrayList<>();
    private VaccineSchedule vacSched;

    private static int vaccineScheduleNumber = 0;

    public VaccineScheduleStore() {
        vaccineScheduleList = new ArrayList<>();
        try {
            vaccineScheduleList = (List<VaccineSchedule>) Persistence.readObjectFromFile("vaccineSchedule.bin");
        } catch (Exception e){
        }
    }

    public List<VaccineSchedule> getVaccineScheduleList() {
        return vaccineScheduleList;
    }

    public VaccineSchedule scheduleVaccine(String snsUserNumber,String vaccinationCenterName,String date,String time,String vacType){
       this.vacSched = new VaccineSchedule(snsUserNumber,vaccinationCenterName,date,time,vacType);
       return this.vacSched;
    }

    public void addVaccineSchedule (VaccineSchedule vacSched){
        this.vaccineScheduleList.add(vacSched);
    }

    public void removeVaccineSchedule (VaccineSchedule vacSched){
        this.vaccineScheduleList.remove(vacSched);
    }

    public void validateScheduleVaccine(String snsUserNumber,String vaccinationCenterName,String date,String time,String vacType) {
        VaccineSchedule vacSched = new VaccineSchedule(snsUserNumber, vaccinationCenterName, date,time,vacType);
        for (VaccineSchedule vc : this.vaccineScheduleList) {
            if (vc.getSnsUserNumber().equals(vacSched.getSnsUserNumber())){
                if (!vc.getVacType().equals(vacSched.getVacType()))
                throw new IllegalStateException("Cannot Schedule a Vaccine with a diferent Vaccine Type");
            }
        }
    }

    public int findIndex (String vacType, String snsUserNumber) {
        for (int i = 0; i<this.getVaccineScheduleList().size(); i++){
            if (this.getVaccineScheduleList().get(i).getSnsUserNumber().equals(snsUserNumber) && this.getVaccineScheduleList().get(i).getVacType().equals(vacType )){
                return i;
            }
        }
        return 0;
    }


    public boolean validateSNSUserNumber(String snsUserNumber) {
        for (int i = 0; i<this.getVaccineScheduleList().size(); i++){
            if (this.getVaccineScheduleList().get(i).getSnsUserNumber().equals(snsUserNumber)){
                return true;
            }
        }
        return false;
    }

}
