package app.domain.store;


import app.Persistence;
import app.mappers.dto.SNSUserDTO;

import java.util.ArrayList;
import java.util.List;

public class WaitingRoomStore {

    private List<List<SNSUserDTO>> waitingRoomList = new ArrayList<>();
    private SNSUserDTO snsUser;

    public WaitingRoomStore() {
        waitingRoomList = new ArrayList<>();
        try {
            waitingRoomList = (List<List<SNSUserDTO>>) Persistence.readObjectFromFile("waitingRoom.bin");
        } catch (Exception e){
        }
    }

    /**
     * Adds recovery room.
     */
    public void addWaitingRoom () {
        waitingRoomList.add(new ArrayList<>());
    }

    /**
     * Adds sns user to recovery room.
     *
     * @param snsUser           the sns user
     * @param vaccinationCenter the vaccination center
     */
    public void addSNSUserToWaitingRoom (SNSUserDTO snsUser, int vaccinationCenter) {
        waitingRoomList.get(vaccinationCenter).add(snsUser);
    }

    /**
     * Remove sns user from waiting room.
     *
     * @param snsUser           the sns user
     * @param vaccinationCenter the vaccination center
     */
    public void removeSNSUserFromWaitingRoom (SNSUserDTO snsUser, int vaccinationCenter) {
        waitingRoomList.get(vaccinationCenter).remove(snsUser);
    }

    /**
     * Gets recovery room list.
     *
     * @param vaccinationCenter the vaccination center
     * @return the recovery room list
     */
    public List<SNSUserDTO> getWaitingRoomList(int vaccinationCenter) {
        return waitingRoomList.get(vaccinationCenter);
    }

    /**
     * Gets recovery room size.
     *
     * @param vaccinationCenter the vaccination center
     * @return the recovery room size
     */
    public String getWaitingRoomSize (int vaccinationCenter) {
        return Integer.toString(waitingRoomList.get(vaccinationCenter).size());
    }

    /**
     * Checks if it exists a sns user on the recovery room list.
     *
     * @param snsUserNumber     the sns user number
     * @param vaccinationCenter the vaccination center
     * @return the sns user
     */
    public SNSUserDTO checkIfSNSUserExists (String snsUserNumber, int vaccinationCenter) {
        int counter = waitingRoomList.get(vaccinationCenter).size();
        for (int i=0; i < counter ; i++) {
            if (waitingRoomList.get(vaccinationCenter).get(i).getSnsUserNumber().equals(snsUserNumber)) {
                snsUser = waitingRoomList.get(vaccinationCenter).get(i);
                return snsUser;
            }
        }
        return null;
    }


    public boolean sameRegister (String snsUserNumber, int vaccinationCenter) {
        int counter = waitingRoomList.get(vaccinationCenter).size();
        for(int i = 0; i<counter; i++){
            if (waitingRoomList.get(vaccinationCenter).get(i).getSnsUserNumber().equals(snsUserNumber)){
                return true;
            }
        }
        return false;
    }

    public List<List<SNSUserDTO>> getWaitingRoomListToSerialization() {
        return waitingRoomList;
    }

}



