package app.domain.store;

import app.Persistence;
import app.domain.model.VaccinationCenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VaccinationCenterStore {

    private List<VaccinationCenter> vaccinationCenterList = new ArrayList<>();
    private VaccinationCenter vac;

    public VaccinationCenterStore() {
        vaccinationCenterList = new ArrayList<>();
        try {
            vaccinationCenterList = (List<VaccinationCenter>) Persistence.readObjectFromFile("vaccinationCenter.bin");
        } catch (Exception e){
        }
    }

    public VaccinationCenter registerNewVaccinationCenter (String name, String address, String phoneNumber, String area, String mailAddress, String faxNumber, String webAddress, String openingHour, String closingHour, String slotDuration, String maxVacSlot){
        this.vac = new VaccinationCenter(name,address,phoneNumber,area,mailAddress,faxNumber,webAddress,openingHour,closingHour,slotDuration,maxVacSlot);
        return this.vac;
    }

    public void addVaccinationCenter (VaccinationCenter vac) {
        this.vaccinationCenterList.add(vac);
    }

   /* public boolean validateVaccinationCenter (VaccinationCenter vac){

        if (vac==null){
            return false;
        }
        return !this.vaccinationCenterList.contains(vac);
    }

    public boolean sameVaccinationCenter(VaccinationCenter vac) {
        for (VaccinationCenter v : vaccinationCenterList) {
            if (v.getName().trim().equalsIgnoreCase(vac.getName().trim())) {
                return false;
            }
            if (v.getPhoneNumber().trim().equalsIgnoreCase(vac.getPhoneNumber().trim())) {
                return false;
            }
            if (v.getAddress().trim().equalsIgnoreCase(vac.getAddress().trim())) {
                return false;
            }
            if (v.getArea().trim().equalsIgnoreCase(vac.getArea().trim())) {
                return false;
            }
        }
        return true;
    }*/

    public List<VaccinationCenter> getVaccinationCenterList() {
        return vaccinationCenterList;
    }



    /*public boolean saveVaccinationCenter (VaccinationCenter vac) {

        if (!validateVaccinationCenter(vac)){
            return false;
        }

        return this.vaccinationCenterList.add(vac);
    }*/

    public void validateNewVaccinationCenter(String name, String address, String phoneNumber, String area, String mailAddress, String faxNumber, String webAddress, String openingHour, String closingHour, String slotDuration, String maxVacSlot) {
        VaccinationCenter vac = new VaccinationCenter(name,address,phoneNumber,area,mailAddress,faxNumber,webAddress,openingHour,closingHour,slotDuration,maxVacSlot);
        for (VaccinationCenter v : this.vaccinationCenterList) {
            if (Objects.equals(v.getName(), vac.getName())) {
                throw new IllegalStateException("Name already exists.");
            }
        }
    }
}