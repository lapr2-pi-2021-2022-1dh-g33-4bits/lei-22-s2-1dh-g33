package app.domain.store;

import app.Persistence;
import app.domain.model.VaccinationData;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Vaccination data store.
 */
public class VaccinationDataStore  {

    private List<VaccinationData> data = new ArrayList<>();

    /**
     * Instantiates a new Vaccination data store.
     */
    public VaccinationDataStore() {
        data = new ArrayList<>();
        try {
            data = (List<VaccinationData>) Persistence.readObjectFromFile("vaccinationData.bin");
        } catch (Exception e){
        }
    }

    /**
     * Gets data list.
     *
     * @return the data list
     */
    public List<VaccinationData> getDataList() {
        return data;
    }

    /**
     * Add data schedule.
     *
     * @param i            the
     * @param scheduleDate the schedule date
     * @param scheduleTime the schedule time
     * @param vaccineType  the vaccine type
     */
    public void addDataSchedule(int i, String scheduleDate, String scheduleTime, String vaccineType) {
        getDataList().get(i).setScheduleDate(scheduleDate);
        getDataList().get(i).setScheduleTime(scheduleTime);
        getDataList().get(i).setVacineType(vaccineType);
    }

    /**
     * Add data arrival.
     *
     * @param i           the
     * @param arrivalHour the arrival hour
     */
    public void addDataArrival(int i, String arrivalHour) {
        getDataList().get(i).setArrivalTime(arrivalHour);
    }

    /**
     * Add data vaccination time.
     *
     * @param i               the
     * @param vaccinationTime the vaccination time
     * @param vaccinationDate the vaccination date
     * @param vaccinationDose the vaccination dose
     * @param loteNumber      the lote number
     */
    public void addDataVaccinationTime(int i, String vaccinationTime, String vaccinationDate, String vaccinationDose, String loteNumber) {
        getDataList().get(i).setAdministrationTime(vaccinationTime);
        getDataList().get(i).setAdministrationDate(vaccinationDate);
        getDataList().get(i).setDose(vaccinationDose);
        getDataList().get(i).setLoteNumber(loteNumber);
    }

    /**
     * Add leaving time.
     *
     * @param i           the
     * @param leavingTime the leaving time
     */
    public void addLeavingTime(int i, String leavingTime) {
        getDataList().get(i).setLeavingTime(leavingTime);
    }

    /**
     * Add import data.
     *
     * @param i               the
     * @param vaccinationTime the vaccination time
     * @param vaccinationDate the vaccination date
     * @param vaccinationDose the vaccination dose
     * @param loteNumber      the lote number
     * @param scheduleDate    the schedule date
     * @param scheduleTime    the schedule time
     * @param vaccineType     the vaccine type
     * @param arrivalHour     the arrival hour
     * @param leavingTime     the leaving time
     */
    public void addImportData(int i, String vaccinationTime, String vaccinationDate, String vaccinationDose, String loteNumber, String scheduleDate, String scheduleTime, String vaccineType, String arrivalHour, String leavingTime) {
        addLeavingTime(i, leavingTime);
        addDataVaccinationTime(i, vaccinationTime, vaccinationDate, vaccinationDose, loteNumber);
        addDataArrival(i, arrivalHour);
        addDataSchedule(i, scheduleDate, scheduleTime, vaccineType);
    }

    /**
     * Gets index.
     *
     * @param data          the data
     * @param snsUserNumber the sns user number
     * @return the index
     */
    public int getIndex(List<VaccinationData> data, String snsUserNumber) {
        int i;
        for (i = 0; i < data.size(); i++) {
            if (data.get(i).getSnsUserNumber().equals(snsUserNumber)) {
                return i;
            }
        }

        return i;
    }

    /**
     * Add.
     *
     * @param userNumber         the user number
     * @param vacineType         the vacine type
     * @param dose               the dose
     * @param loteNumber         the lote number
     * @param scheduleDate       the schedule date
     * @param scheduleTime       the schedule time
     * @param arrivalDate        the arrival date
     * @param arrivalTime        the arrival time
     * @param administrationDate the administration date
     * @param administrationTime the administration time
     * @param leavingDate        the leaving date
     * @param leavingTime        the leaving time
     * @throws ParseException the parse exception
     */
    public void add(String userNumber, String vacineType, String dose, String loteNumber, String scheduleDate, String scheduleTime, String arrivalDate, String arrivalTime, String administrationDate, String administrationTime, String leavingDate, String leavingTime) throws ParseException {
        getDataList().add(new VaccinationData(userNumber, vacineType, dose, loteNumber, scheduleDate, scheduleTime, arrivalDate, arrivalTime, administrationDate, administrationTime, leavingDate, leavingTime));
    }



}




