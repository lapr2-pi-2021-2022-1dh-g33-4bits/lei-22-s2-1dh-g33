package app.domain.store;

import app.Persistence;
import app.mappers.dto.SNSUserDTO;

import java.util.ArrayList;
import java.util.List;

public class SNSUserDataStore {

    private List<SNSUserDTO> snsUserList = new ArrayList<>();
    private SNSUserDTO snsUserDTO;


    public SNSUserDataStore(){
        snsUserList = new ArrayList<>();
        try{
            snsUserList = (List<SNSUserDTO>) Persistence.readObjectFromFile("snsUser.bin");
        } catch (Exception e){
        }
    }

    /**
     * @param citizenCardNumber - Data entered by the receptionist
     * @param snsUserNumber     - Data entered by the receptionist
     * @param birthDate         - Data entered by the receptionist
     * @param sex               - Data entered by the receptionist
     * @param phoneNumber       - Data entered by the receptionist
     * @param address           - Data entered by the receptionist
     * @param email             - Data entered by the receptionist
     * @param name              - Data entered by the receptionist
     * @return - Creating the client object
     */
    public SNSUserDTO createSNSUser(String name, String sex, String birthDate, String address, String phoneNumber, String email, String snsUserNumber, String citizenCardNumber){
        return new SNSUserDTO(name,sex,birthDate,address,phoneNumber,email,snsUserNumber,citizenCardNumber);
    }

    /**
     * If there is already a SNS User with the same citizen card number and sns user number in the list
     *
     * @param snsUser - snsUser attributes
     */
    public void validateSNSUser(SNSUserDTO snsUser) {
        for (SNSUserDTO snsUserData : snsUserList) {
            if (snsUserData.getCitizenCardNumber().equals(snsUser.getCitizenCardNumber())) {
                throw new IllegalCallerException("There is a SNS User with the same Citizen Card Number already created.");
            }
            if (snsUserData.getSnsUserNumber().equals(snsUser.getSnsUserNumber())) {
                throw new IllegalCallerException("There is a SNS User with the same SNS user number already created.");
            }
        }
    }

    /**
     * Checks if it exists a sns user on the recovery room list.
     *
     * @param snsUserNumber     the sns user number
     * @return the sns user
     */
    public SNSUserDTO checkIfSNSUserExists (String snsUserNumber) {
        for (SNSUserDTO snsUserData : snsUserList) {
            if (snsUserData.getSnsUserNumber().equals(snsUserNumber)) {
                return snsUserData;
            }
        }
        return null;
    }

    /**
     * Saves the SNS User in the list of all SNS Users created
     *
     * @param snsUser - snsUser Data
     */
    public void saveSNSUser(SNSUserDTO snsUser){
        validateSNSUser(snsUser);
        addSNSUser(snsUser);
    }

    public void addSNSUser(SNSUserDTO snsUser) {
        if(snsUser != null) {
            if(!snsUserList.contains(snsUser)) {
                snsUserList.add(snsUser);
            }
        }
    }

    public boolean sameRegister(SNSUserDTO snsUser){
        for(SNSUserDTO c : snsUserList){
            if(c.getSnsUserNumber().trim().equalsIgnoreCase(snsUser.getSnsUserNumber().trim())){
                return false;
            }
        }
        return true;
    }

    public List<SNSUserDTO> getSNSUserList () { return snsUserList; }

    /**
     * Gets the SNS User with name and email
     * @param name list with specific names
     * @return list with the clients
     */
    public List<SNSUserDTO> getSNSUserByName(List<String> name){
        List<SNSUserDTO> snsUsersSortList = new ArrayList<>();
        for(String s : name){
            for(SNSUserDTO u : snsUserList){
                if(u.getName().equals(s)){
                    snsUsersSortList.add(u);
                }
            }
        }
        return snsUsersSortList;
    }

    public SNSUserDTO getBySNSUserCardNumber(String SnsUserCardNumber){
        for(int i = 0 ; i < snsUserList.size(); i++){
            if(snsUserList.get(i).getSnsUserNumber().equals(SnsUserCardNumber)){
                return snsUserList.get(i);
            }
        }
        return null;
    }

    public boolean getBySNSUserCardNumberExists(String SnsUserCardNumber) {
        for (int i = 0; i < snsUserList.size(); i++) {
            if (snsUserList.get(i).getSnsUserNumber().equals(SnsUserCardNumber)) {
                return true;
            }

        }
        return false;
    }

    public String getNameBySnsUserCard (String SnsUserCardNumber) {
        for (int i = 0; i < snsUserList.size(); i++) {
            if (snsUserList.get(i).getSnsUserNumber().equals(SnsUserCardNumber)) {
              return snsUserList.get(i).getName();
            }
        }
        return "Null";
    }
    /**
     * Gets the number of SNS Users
     * @return SNS User list size (number of SNS Users)
     */
    public int getNumberOfSNSUsers(){ return snsUserList.size();}
}
