package app.domain.store;


import app.Persistence;
import app.controller.App;
import auth.AuthFacade;
import auth.domain.model.UserRole;

import java.util.ArrayList;
import java.util.List;

public class OrganizationRolesStore {

    private static List<UserRole> organizationRolesList = new ArrayList<UserRole>();

    /**
     * Adds the known roles of the company to the organizationRolesList
     */
    public OrganizationRolesStore() {
        organizationRolesList = new ArrayList<>();
        try {
            organizationRolesList = (List<UserRole>) Persistence.readObjectFromFile("organizationsRoles.bin");
        } catch (Exception e){
        }
        organizationRolesList.add(new UserRole("Administrator", "ADMINISTRATOR"));
        organizationRolesList.add(new UserRole("Receptionist", "RECEPTIONIST"));
        organizationRolesList.add(new UserRole("Nurse", "NURSE"));
        organizationRolesList.add(new UserRole("Center Coordinator", "CENTER COORDINATOR"));
        organizationRolesList.add(new UserRole("SNS User", "SNS USER"));
    }

    public static List<UserRole> GetList () {
        return organizationRolesList;
    }


    /**
     * Introduces the roles from organizationRolesList so they can be used to log in
     */
    public void addRoles() {
        AuthFacade AF = App.getInstance().getCompany().getAuthFacade();
        for (UserRole ur : organizationRolesList) {
            AF.addUserRole(ur.getId(), ur.getDescription());
        }
    }



    /**
     * Gets the organizationRolesList where the roles are saved so the administrator can choose which role a new employee will have
     *
     * @return - organizationRolesList in for of a list
     */

    public List<UserRole> getOrganizationRolesList() {return organizationRolesList;}
}
