package app.domain.store;

import app.Persistence;
import app.domain.model.Employee;
import auth.AuthFacade;

import java.util.ArrayList;
import java.util.List;

public class EmployeeStore {

    private static List<Employee> employeeList = new ArrayList<>();
    private AuthFacade authFacade;

    private static int employeeNumber = 0;

    public EmployeeStore(){
        employeeList = new ArrayList<>();
        try {
            employeeList = (List<Employee>) Persistence.readObjectFromFile("employee.bin");
        } catch (Exception e){
        }
    }


    /**
     * Object employee is created and the attributes are sent to verification
     *
     * @param name         - Data entered by Administrator.
     * @param address      - Data entered by Administrator.
     * @param emailAddress - Data entered by Administrator.
     * @param phoneNumber  - Data entered by Administrator.
     * @param socCode      - Data entered by Administrator.
     * @param id           - Data entered by Administrator.
     * @param NIF          - Data entered by Administrator.
     * @param roleId       - Data entered by Administrator.
     * @return
     */

    public Employee createEmployee(String roleId, String name, String address, String phoneNumber, String emailAddress, String id, String socCode, String NIF) {
        employeeNumber++;
        return new Employee(roleId, name, address, phoneNumber, emailAddress, id, socCode, NIF);
    }


    /**
     * Validates a employee so it can be approved to be saved or not
     *
     * @param employee - Employee created with the info entered by the administrator
     * @return - True (if the employee wasn't already created) or false (if the employee is null or has already been added to the list of employees)
     */
    public boolean validateEmployee(Employee employee) {

        if (employee == null)
            return false;

        return !this.employeeList.contains(employee);
    }

    /**
     * Checks attributes that can't be the same for two employees
     *
     * @param employee - Employee created with the info entered by the administrator
     * @return - True (if the address, email and phone number are new to the system) or false (if there is already a employee with the same address, email, phone number, ID, NIF)
     */
    public boolean sameEmployee(Employee employee) {
        for (Employee c : employeeList) {
            if (c.getAddress().trim().equalsIgnoreCase(employee.getAddress().trim())) {
                return false;
            }
            if (c.getEmailAddress().trim().equalsIgnoreCase(employee.getEmailAddress().trim())) {
                return false;
            }
            if (c.getPhoneNumber().trim().equalsIgnoreCase(employee.getPhoneNumber().trim())) {
                return false;
            }
            if (c.getId().trim().equalsIgnoreCase(employee.getId().trim())) {
                return false;
            }
            if (c.getNIF().trim().equalsIgnoreCase(employee.getNIF().trim())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Adds employee to the employee list
     *
     * @param employee - Employee created with the info entered by the administrator
     * @return - True (if the employee was added successfully to the list) or false (if the employee failed the validation or if there was an error adding the employee)
     */
    public boolean saveEmployee(Employee employee) {

        if (!validateEmployee(employee))
            return false;

        return this.employeeList.add(employee);
    }

    public void notSaveEmployee() {
        if (employeeNumber > 0) {
            employeeNumber--;
        }
    }

    public List<Employee> getEmployeeStore() {
        return employeeList;
    }

}
