package app.domain.store;

import app.Persistence;
import app.domain.model.RecordVaccine;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class RecordVaccineStore {

    private List<List<RecordVaccine>> recordVaccineList = new ArrayList<>();
    private RecordVaccine recVac;
    private Date biggestDate;
    private Date smallestDate;
    private static int vaccineRecordedNumber = 0;

    public RecordVaccineStore() {
        recordVaccineList = new ArrayList<>();
        try {
            recordVaccineList = (List<List<RecordVaccine>>) Persistence.readObjectFromFile("recordVaccine.bin");
        } catch (Exception e){
        }
    }

    public List<RecordVaccine> getRecordVaccineList(int vc) {
        return recordVaccineList.get(vc);
    }

    public List<List<RecordVaccine>> getRecordVaccineListToSerialization() {return recordVaccineList;}

    public RecordVaccine recordVaccine(String date, String vacType,String vacName,String lotNumber,String snsUserNUmber){
        vaccineRecordedNumber++;
        return this.recVac = new RecordVaccine(date, vacType,vacName,lotNumber,snsUserNUmber);
    }

    public static int getVaccineRecordedNumber() {
        return vaccineRecordedNumber;
    }

    public void addRecordVaccine(RecordVaccine recVac, int vc) {
        this.recordVaccineList.get(vc).add(recVac);
    }

    public void addVaccinationCenter () {
        recordVaccineList.add(new ArrayList<>());
    }

    public int turnBirthDateToAge(String birthdate) {
        String[] separated = birthdate.split("/");
        if (separated.length==3) {
            if (separated[0].length()==1 && separated[1].length()==2) {
                birthdate = "0" + separated[0] + "/" + separated[1]+"/"+separated[2];
            }
            if (separated[0].length()==2 && separated[1].length()==1) {
                birthdate = separated[0] + "/0" + separated[1]+"/"+separated[2];
            }
            if (separated[0].length()==1 && separated[1].length()==1) {
                birthdate = "0" + separated[0] + "/0" + separated[1]+"/"+separated[2];
            }
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        String dateStr = dtf.format(now);
        LocalDate localDatecurr = LocalDate.parse(dateStr, dtf);
        LocalDate localDatebirth = LocalDate.parse(birthdate, dtf);
        Period age = Period.between(localDatebirth,localDatecurr);
        return age.getYears();
    }

    public boolean checkIfDateExistsOnRecord (String date, int vc) {
        for (int i = 0; i < recordVaccineList.get(vc).size(); i++) {
            String teste = recordVaccineList.get(vc).get(i).getDate();
            if (Objects.equals(recordVaccineList.get(vc).get(i).getDate(), date)) {
                return true;
            }
        }
        return false;
    }

    public Date getSmallestDateOnRecord () {
        return smallestDate;
    }

    public Date getBiggestDateOnRecord () {
        return biggestDate;
    }


}
