package app.domain.store;

import app.Persistence;
import app.domain.model.VaccineType;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VaccineTypesStore {

    private List<VaccineType> vaccineTypesList = new ArrayList<VaccineType>();


    public VaccineTypesStore() {
        vaccineTypesList = new ArrayList<>();
        try {
            vaccineTypesList = (List<VaccineType>) Persistence.readObjectFromFile("vaccineTypes.bin");
        } catch (Exception e){
        }
    }
    public List<VaccineType> getVaccineTypesList() {
        return vaccineTypesList;
    }

    private VaccineType vacType;

    public VaccineType registVaccineType(String vaccineType){
        this.vacType = new VaccineType(vaccineType);
        return this.vacType;
    }

    public void addVaccineType(VaccineType vacType){
        this.vaccineTypesList.add(vacType);
    }

    public void validateVaccineType(String vaccineType) {
        VaccineType vaccineType1 = new VaccineType(vaccineType);
        for (VaccineType vc : this.vaccineTypesList) {
            if ((Objects.equals(vc.getVaccineType(),vaccineType1.getVaccineType()))){
                throw new IllegalStateException("Vaccine Type already exists");
            }
        }
    }


    /*
     * Adds the known vaccine types to the vaccineTypesList.
     */

   /* public VaccineTypesStore () {
        vaccineTypesList.add(new VaccineType("Tetanus"));
        vaccineTypesList.add(new VaccineType("Hepatitis A"));
        vaccineTypesList.add(new VaccineType("Hepatitis B"));
        vaccineTypesList.add(new VaccineType("Polio"));
        vaccineTypesList.add(new VaccineType("Chickenpox"));
        vaccineTypesList.add(new VaccineType("Covid-19"));
    }

    /*
     * Introduces the vaccine types from vaccineTypeList so they can be used to specify a new vaccine
     */


}
