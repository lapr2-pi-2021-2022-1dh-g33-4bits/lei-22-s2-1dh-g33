package app.domain.store;

import app.Persistence;
import app.domain.model.VaccineSchedule;

import java.util.ArrayList;
import java.util.List;

public class VaccinationHistoryStore {

    private List<VaccineSchedule> vaccineHistoryList = new ArrayList<>();

    public VaccinationHistoryStore() {
        vaccineHistoryList = new ArrayList<>();
        try {
            vaccineHistoryList = (List<VaccineSchedule>) Persistence.readObjectFromFile("vaccinationHistory.bin");
        } catch (Exception e){
        }
    }

    public List<VaccineSchedule> getVaccineHistoryList() {
        return vaccineHistoryList;
    }

    public void addVaccineHistory (VaccineSchedule vacSched){
        this.vaccineHistoryList.add(vacSched);
    }

}
