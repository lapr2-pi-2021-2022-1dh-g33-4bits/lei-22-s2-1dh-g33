package app.domain.store;

import app.Persistence;
import app.domain.model.Vaccine;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VaccineStore  {

    private List<Vaccine> vaccineStore = new ArrayList<>();
    private Vaccine vaccine;

    public VaccineStore() {
        vaccineStore = new ArrayList<>();
        try {
            vaccineStore = (List<Vaccine>) Persistence.readObjectFromFile("vaccine.bin");
        } catch (Exception e){
        }
    }

    /**
     * Object vaccine is created and the attributes are sent to verification
     *
     * @param ID               - Data entered by Administrator.
     * @param name             - Data entered by Administrator.
     * @param brand            - Data entered by Administrator.
     * @param vaccineType      - Data entered by Administrator.
     * @param ageGroup         - Data entered by Administrator.
     * @param doseNumber       - Data entered by Administrator.
     * @param vaccineDosage    - Data entered by Administrator.
     * @param timeBetweenDoses - Data entered by Administrator.
     * @return
     */
    public Vaccine SpecifyNewVaccine (String ID, String name, String brand, String vaccineType, String ageGroup, String doseNumber, String vaccineDosage, String timeBetweenDoses) {
        this.vaccine = new Vaccine(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
        return this.vaccine;
    }

    /**
     * Adds a new vaccine
     * @param vaccine - vaccine data
     */
    public void addVaccine(Vaccine vaccine) {
        this.vaccineStore.add(vaccine);
    }

    /**
     * @param ID               - Data entered by Administrator.
     * @param name             - Data entered by Administrator.
     * @param brand            - Data entered by Administrator.
     * @param vaccineType      - Data entered by Administrator.
     * @param ageGroup         - Data entered by Administrator.
     * @param doseNumber       - Data entered by Administrator.
     * @param vaccineDosage    - Data entered by Administrator.
     * @param timeBetweenDoses - Data entered by Administrator.
     * @return - If there is already a parameter category with the same ID on the list
     */
    public void validateNewVaccine(String ID, String name, String brand, String vaccineType, String ageGroup, String doseNumber, String vaccineDosage, String timeBetweenDoses) {
        Vaccine vac = new Vaccine(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
        for (Vaccine v : this.vaccineStore) {
            if (Objects.equals(v.getID(), vac.getID())) {
                throw new IllegalStateException("ID already exists.");
            }
        }
    }



    /**
     * Checks if the attributes are the same
     *
     * @param  - Vaccine was created with data given by de administrator
     * @return - True (if the ID and name are new) or false (if there's already a vaccine with that name or ID)
     */
    /*
    public boolean sameVaccine (Vaccine vaccine) {
        for (Vaccine v : vaccineStore) {
            if (v.getID()==(vaccine.getID())) {
                return false;
            }
            if (v.getName().trim().equalsIgnoreCase(vaccine.getName().trim())) {
                return false;
            }
        }
        return true;
    }
     */

    public List<Vaccine> getVaccineList() {
        return vaccineStore;
    }
}
