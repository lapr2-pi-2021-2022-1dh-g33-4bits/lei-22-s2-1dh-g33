package app.domain.store;

import app.Persistence;
import app.domain.model.VaccinationStatistic;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Vaccination statistics store.
 */
public class VaccinationStatisticsStore {

    private List<VaccinationStatistic> vaccinationStatisticsList = new ArrayList<>();

    /**
     * Instantiates a new Vaccination statistics store.
     */
    public VaccinationStatisticsStore() {
        vaccinationStatisticsList = new ArrayList<>();
        try {
            vaccinationStatisticsList = (List<VaccinationStatistic>) Persistence.readObjectFromFile("vaccinationStatistics.bin");
        } catch (Exception e){
        }
    }

    /**
     * Gets vaccination statistics list.
     *
     * @return the vaccination statistics list
     */
    public List<VaccinationStatistic> getVaccinationStatisticsList() {
        return vaccinationStatisticsList;
    }

    /**
     * New vaccination statistic.
     *
     * @param date              the date
     * @param fullVaccinatedDay the full vaccinated day
     * @return the vaccination statistic
     */
    public VaccinationStatistic newVaccinationStatistic (String date, String fullVaccinatedDay) {
        return new VaccinationStatistic(date, fullVaccinatedDay);
    }

    /**
     * Add vaccination statistics day.
     *
     * @param vaccinationStatistic the vaccination statistic
     */
    public void addVaccinationStatisticsDay (VaccinationStatistic vaccinationStatistic) {
        vaccinationStatisticsList.add(vaccinationStatistic);
    }

    /**
     * Check vaccination statistics to string.
     *
     * @param date the date
     */
    public void checkVaccinationStatisticsToString (String date) {
        for (int i = 0; i < vaccinationStatisticsList.size(); i++) {
            if (vaccinationStatisticsList.get(i).getDate().equals(date)) {
                System.out.println("Date: " + vaccinationStatisticsList.get(i).getDate() + "; Number of fully vaccinated users:" + vaccinationStatisticsList.get(i).getNumberFullVaccinated());
            }
        }
    }

    /**
     * Export vaccination statistics to string.
     *
     * @param date        the date
     * @param printWriter the print writer
     */
    public void exportVaccinationStatisticsToString (String date, PrintWriter printWriter) {
        for (int i = 0; i < vaccinationStatisticsList.size(); i++) {
            if (vaccinationStatisticsList.get(i).getDate().equals(date)) {
                printWriter.println(vaccinationStatisticsList.get(i).toString());
            }
        }

    }

    /**
     * Clear vaccination statistics.
     */
    public void clearVaccinationStatistics () {
        vaccinationStatisticsList.clear();
    }

}
