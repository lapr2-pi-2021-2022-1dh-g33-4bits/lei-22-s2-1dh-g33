package app.domain.store;

import app.Persistence;
import app.mappers.dto.SNSUserDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Recovery room store.
 */
public class RecoveryRoomStore  {

    private List<List<SNSUserDTO>> recoveryRoomList = new ArrayList<>();
    private SNSUserDTO snsUser;

    public RecoveryRoomStore() {
        recoveryRoomList = new ArrayList<>();
        try {
            recoveryRoomList = (List<List<SNSUserDTO>>) Persistence.readObjectFromFile("recoveryRoom.bin");
        } catch (Exception e){
        }
    }

    /**
     * Adds recovery room.
     */
    public void addRecoveryRoom () {
        recoveryRoomList.add(new ArrayList<>());
    }

    /**
     * Adds sns user to recovery room.
     *
     * @param snsUser           the sns user
     * @param vaccinationCenter the vaccination center
     */
    public void addSNSUserToRecoveryRoom (SNSUserDTO snsUser, int vaccinationCenter) {
        recoveryRoomList.get(vaccinationCenter).add(snsUser);
    }


    /**
     * Remove sns user from recovery room.
     *
     * @param snsUser           the sns user
     * @param vaccinationCenter the vaccination center
     */
    public void removeSNSUserFromRecoveryRoom (SNSUserDTO snsUser, int vaccinationCenter) {
        recoveryRoomList.get(vaccinationCenter).remove(snsUser);
    }

    /**
     * Gets recovery room list.
     *
     * @param vaccinationCenter the vaccination center
     * @return the recovery room list
     */
    public List<SNSUserDTO> getRecoveryRoomList(int vaccinationCenter) {
        return recoveryRoomList.get(vaccinationCenter);
    }

    /**
     * Gets recovery room size.
     *
     * @param vaccinationCenter the vaccination center
     * @return the recovery room size
     */
    public String getRecoveryRoomSize (int vaccinationCenter) {
        return Integer.toString(recoveryRoomList.get(vaccinationCenter).size());
    }

    /**
     * Checks if it exists a sns user on the recovery room list.
     *
     * @param snsUserNumber     the sns user number
     * @param vaccinationCenter the vaccination center
     * @return the sns user
     */
    public SNSUserDTO checkIfSNSUserExists (String snsUserNumber, int vaccinationCenter) {
        int counter = recoveryRoomList.get(vaccinationCenter).size();
        for (int i=0; i < counter ; i++) {
            if (recoveryRoomList.get(vaccinationCenter).get(i).getSnsUserNumber().equals(snsUserNumber)) {
                snsUser = recoveryRoomList.get(vaccinationCenter).get(i);
                return snsUser;
            }
        }
        return null;
    }

    public List<List<SNSUserDTO>> getRecoveryRoomListToSerialization() { return recoveryRoomList;}
}




