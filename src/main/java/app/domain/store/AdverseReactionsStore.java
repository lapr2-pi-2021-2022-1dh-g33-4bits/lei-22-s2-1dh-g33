package app.domain.store;

import app.Persistence;
import app.domain.model.AdverseReaction;

import java.util.ArrayList;
import java.util.List;



/**
 * The type Adverse reactions store.
 */
public class AdverseReactionsStore  {

    private List<AdverseReaction> adverseReactionsList = new ArrayList<>();
    private AdverseReaction adverseReaction;

    public AdverseReactionsStore(){
        adverseReactionsList = new ArrayList<>();
        try {
            adverseReactionsList = (List<AdverseReaction>)Persistence.readObjectFromFile("adverseReactions.bin");
        } catch (Exception e){
        }
    }

    /**
     * New adverse reaction.
     *
     * @param snsUserNumber       the sns user number
     * @param adverseReactionText the adverse reaction text
     * @return the adverse reaction
     */
    public AdverseReaction newAdverseReaction (String snsUserNumber, String adverseReactionText) {
        this.adverseReaction = new AdverseReaction(snsUserNumber, adverseReactionText);
        return this.adverseReaction;
    }

    /**
     * Add/Save adverse reaction.
     *
     * @param adverseReaction the adverse reaction
     */
    public void addAdverseReaction (AdverseReaction adverseReaction) {
        this.adverseReactionsList.add(adverseReaction);
    }

    public List<AdverseReaction> getAdverseReactionsList() {
        return adverseReactionsList;
    }
}
