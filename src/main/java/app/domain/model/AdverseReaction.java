package app.domain.model;

import org.apache.commons.lang3.StringUtils;

/**
 * The type Adverse reaction.
 */
public class AdverseReaction {
    private String snsUserNumber;
    private String adverseReaction;

    /**
     * Instantiates a new Adverse reaction.
     *
     * @param snsUserNumber   the sns user number
     * @param adverseReaction the adverse reaction
     */
    public AdverseReaction (String snsUserNumber, String adverseReaction) {
        this.snsUserNumber = snsUserNumber;
        this.adverseReaction = adverseReaction;
        checkRequirements(snsUserNumber, adverseReaction);
    }

    /**
     * Instantiates a new Adverse reaction.
     */
    public AdverseReaction(){
    }

    /**
     * Gets sns user number.
     *
     * @return the sns user number
     */
    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    /**
     * Sets sns user number.
     *
     * @param snsUserNumber the sns user number
     */
    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
        checkSNSUserNumber(snsUserNumber);
    }

    /**
     * Gets adverse reaction.
     *
     * @return the adverse reaction
     */
    public String getAdverseReaction() {
        return adverseReaction;
    }

    /**
     * Sets adverse reaction.
     *
     * @param adverseReaction the adverse reaction
     */
    public void setAdverseReaction(String adverseReaction) {
        this.adverseReaction = adverseReaction;
        checkAdverseReaction(adverseReaction);
    }

    /**
     * Check requirements.
     *
     * @param snsUserNumber   the sns user number
     * @param adverseReaction the adverse reaction
     */
    public void checkRequirements (String snsUserNumber, String adverseReaction) {
        checkSNSUserNumber(snsUserNumber);
        checkAdverseReaction(adverseReaction);
    }

    @Override
    public String toString () {
        return "SNS user number ='" + snsUserNumber + '\'' +
                "Adverse reaction ='" + adverseReaction + '\'' +
                '}';
    }

    /**
     * Check sns user number.
     *
     * @param name the name
     */
    public void checkSNSUserNumber (String name) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("\n======================= Insert a valid SNS User Number =======================\n");
        }
        if (snsUserNumber.length() != 9 || !snsUserNumber.matches("[0-9]+")) {
            throw new IllegalArgumentException("\n======================= Insert a valid SNS User Number =======================\n");
        }
    }

    /**
     * Check adverse reaction.
     *
     * @param adverseReaction the adverse reaction
     */
    public void checkAdverseReaction (String adverseReaction) {
        if (StringUtils.isBlank(adverseReaction)) {
            throw new IllegalArgumentException("\n======================= Adverse reactions cannot be blank =======================\n");
        }
        if (adverseReaction.length() > 300) {
            throw new IllegalArgumentException("\n======================= Adverse reactions cannot have more than 300 characters =======================\n");
        }
    }

}
