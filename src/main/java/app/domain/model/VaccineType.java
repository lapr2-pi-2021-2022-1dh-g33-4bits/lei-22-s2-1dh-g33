package app.domain.model;

import org.apache.commons.lang3.StringUtils;

public class VaccineType {
    private String vaccineType;

    public VaccineType(String vaccineType){

        checkVacType(vaccineType);
        this.vaccineType = vaccineType;

    }

    public VaccineType(){

    }

    public String getVaccineType() {
        return vaccineType;
    }

    public void setVaccineType(String vaccineType) {
        this.vaccineType = vaccineType;
        checkVacType(vaccineType);
    }

    public void checkVacType(String vaccineType) {
        if (StringUtils.isBlank(vaccineType)) {
            throw new IllegalArgumentException("Vaccine Type cannot be blank.");
        }
    }
}
