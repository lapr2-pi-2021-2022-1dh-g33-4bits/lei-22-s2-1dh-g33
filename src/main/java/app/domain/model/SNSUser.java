package app.domain.model;

import app.domain.store.OrganizationRolesStore;
import auth.domain.model.UserRole;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * The type Sns user.
 */
public class SNSUser {
    private String name;
    private String sex;
    private String birthDate;
    private String address;
    private String phoneNumber;
    private String email;
    private String snsUserNumber;
    private String citizenCardNumber;

    /**
     * Instantiates a new Sns user.
     *
     * @param name              the name
     * @param sex               the sex
     * @param birthDate         the birth date
     * @param address           the address
     * @param phoneNumber       the phone number
     * @param email             the email
     * @param snsUserNumber     the sns user number
     * @param citizenCardNumber the citizen card number
     */
    public SNSUser (String name, String sex, String birthDate, String address, String phoneNumber, String email, String snsUserNumber, String citizenCardNumber) {
        this.name = name;
        this.sex = sex;
        this.birthDate = birthDate;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.snsUserNumber = snsUserNumber;
        this.citizenCardNumber = citizenCardNumber;
        checkRequirements(name, sex, birthDate, address, phoneNumber, email, snsUserNumber, citizenCardNumber);
    }

    /**
     * Instantiates a new Sns user.
     */
    public SNSUser () {
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
        checkName(name);
    }

    /**
     * Gets sex.
     *
     * @return the sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * Sets sex.
     *
     * @param sex the sex
     */
    public void setSex(String sex) {
        this.sex = sex;
        checkSex(sex);
    }

    /**
     * Gets birth date.
     *
     * @return the birth date
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * Sets birth date.
     *
     * @param birthDate the birth date
     */
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
        checkBirthDate(birthDate);
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
        checkAddress(address);
    }

    /**
     * Gets phone number.
     *
     * @return the phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets phone number.
     *
     * @param phoneNumber the phone number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        checkPhoneNumber(phoneNumber);
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
        checkEmail(email);
    }

    /**
     * Gets sns user number.
     *
     * @return the sns user number
     */
    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    /**
     * Sets sns user number.
     *
     * @param snsUserNumber the sns user number
     */
    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
        checkSNSUserNumber(snsUserNumber);
    }

    /**
     * Gets citizen card number.
     *
     * @return the citizen card number
     */
    public String getCitizenCardNumber() {
        return citizenCardNumber;
    }

    /**
     * Sets citizen card number.
     *
     * @param citizenCardNumber the citizen card number
     */
    public void setCitizenCardNumber(String citizenCardNumber) {
        this.citizenCardNumber = citizenCardNumber;
        checkCitizenCardNumber(citizenCardNumber);
    }

    /**
     * Check requirements.
     *
     * @param name              the name
     * @param sex               the sex
     * @param birthDate         the birth date
     * @param address           the address
     * @param phoneNumber       the phone number
     * @param email             the email
     * @param snsUserNumber     the sns user number
     * @param citizenCardNumber the citizen card number
     */
    public void checkRequirements (String name, String sex, String birthDate, String address, String phoneNumber, String email, String snsUserNumber, String citizenCardNumber) {
        checkName(name);
        checkSex(sex);
        checkBirthDate(birthDate);
        checkAddress(address);
        checkPhoneNumber(phoneNumber);
        checkEmail(email);
        checkSNSUserNumber(snsUserNumber);
        checkCitizenCardNumber(citizenCardNumber);
    }

    @Override
    public String toString () {
        return "SNS User:" + '\'' +
                "Name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", birth date='" + birthDate + '\'' +
                ", address =" + address + '\'' +
                ", phone number =" + phoneNumber + '\'' +
                ", email ='" + email + '\'' +
                ", sns user number=" + snsUserNumber + '\'' +
                ", citizen card number ='" + citizenCardNumber + '\'' +
                '}';
    }

    /**
     * Validate sex.
     *
     * @param sex the sex
     * @return the boolean
     */
    public boolean validateSex (String sex) {
        if (sex.toUpperCase(Locale.ROOT).equals("MALE")){
            return true;
        }
        if (sex.toUpperCase(Locale.ROOT).equals("FEMALE")) {
            return true;
        }
        if (sex.toUpperCase(Locale.ROOT).equals("M")) {
            return true;
        }
        if (sex.toUpperCase(Locale.ROOT).equals("NA")) {
            return true;
        }
        if (sex.toUpperCase(Locale.ROOT).equals("MASCULINO")) {
            return true;
        }
        if (sex.toUpperCase(Locale.ROOT).equals("FEMININO")) {
            return true;
        }
        if (sex.toUpperCase(Locale.ROOT).replaceAll(" ", "").equals("")) {
            this.sex = "NA";
            return true;
        }
        return sex.toUpperCase(Locale.ROOT).equals("F");
    }

    /**
     * Validate birth date boolean.
     *
     * @param birthDate the birth date
     * @return the boolean
     */
    public boolean validateBirthDate (String birthDate) {
        LocalDateTime ldt;
        String[] separated = birthDate.split("/");
        if (separated.length==3) {
            if (separated[0].length()==1 && separated[1].length()==2) {
                birthDate = "0" + separated[0] + "/" + separated[1]+"/"+separated[2];
                this.birthDate = birthDate;
            }
            if (separated[0].length()==2 && separated[1].length()==1) {
                birthDate = separated[0] + "/0" + separated[1]+"/"+separated[2];
                this.birthDate = birthDate;
            }
            if (separated[0].length()==1 && separated[1].length()==1) {
                birthDate = "0" + separated[0] + "/0" + separated[1]+"/"+separated[2];
                this.birthDate = birthDate;
            }
        }
        DateTimeFormatter fomatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        try {
            ldt = LocalDateTime.parse(birthDate, fomatter);
            String result = ldt.format(fomatter);
            return result.equals(birthDate);
        } catch (DateTimeParseException e) {
            try {
                LocalDate ld = LocalDate.parse(birthDate, fomatter);
                String result = ld.format(fomatter);
                return result.equals(birthDate);
            } catch (DateTimeParseException exp) {
                try {
                    LocalTime lt = LocalTime.parse(birthDate, fomatter);
                    String result = lt.format(fomatter);
                    return result.equals(birthDate);
                } catch (DateTimeParseException e2) {
                    //Debugging purposes
                    //e2.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * Check name.
     *
     * @param name the name
     */
    public void checkName (String name) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Name cannot be blank.");
        }
    }

    /**
     * Check sex.
     *
     * @param sex the sex
     */
    public void checkSex (String sex) {
        if (!validateSex(sex)) {
            throw new IllegalArgumentException("Invalid sex format.");
        }
    }

    /**
     * Check birth date.
     *
     * @param birthDate the birth date
     */
    public void checkBirthDate (String birthDate) {
        if (StringUtils.isBlank(birthDate)) {
            throw new IllegalArgumentException("Birth date cannot be blank.");
        }
        if (!validateBirthDate(birthDate)) {
            throw new IllegalArgumentException("Invalid birth date format.");
        }
    }

    /**
     * Check address.
     *
     * @param address the address
     */
    public void checkAddress (String address) {
        if (StringUtils.isBlank(address)) {
            throw new IllegalArgumentException("Address cannot be blank.");
        }
    }

    /**
     * Check phone number.
     *
     * @param phoneNumber the phone number
     */
    public void checkPhoneNumber (String phoneNumber) {
        if (StringUtils.isBlank(phoneNumber)) {
            throw new IllegalArgumentException("Phone number cannot be blank.");
        }
        if (phoneNumber.length() != 9 || !phoneNumber.matches("[0-9]+")) {
            throw new IllegalArgumentException("Phone number isn't valid.");
        }
    }

    /**
     * Check email.
     *
     * @param email the email
     */
    public void checkEmail (String email) {
        if (StringUtils.isBlank(email)) {
            throw new IllegalArgumentException("Email cannot be blank.");
        }
/*
        It allows numeric values from 0 to 9.
        Both uppercase and lowercase letters from a to z are allowed.
        Underscore “_”, hyphen “-“, and dot “.” are allowed.
        Dot isn't allowed at the start and end of the local part.
        Consecutive dots aren't allowed.
        For the local part, a maximum of 64 characters are allowed.
 */
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@" +  //part before @
                "(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern pat = Pattern.compile(emailRegex);
        if (!pat.matcher(email.replaceAll("[^\\p{ASCII}]", "")).matches()) {
            throw new IllegalArgumentException("Email format is invalid.");
        }
    }

    /**
     * Check sns user number.
     *
     * @param snsUserNumber the sns user number
     */
    public void checkSNSUserNumber (String snsUserNumber) {
        if (StringUtils.isBlank(snsUserNumber)) {
            throw new IllegalArgumentException("SNS user number cannot be blank.");
        }
        if (snsUserNumber.length() != 9 || !snsUserNumber.matches("[0-9]+")) {
            throw new IllegalArgumentException("SNS user number isn't valid.");
        }
    }

    /**
     * Check citizen card number.
     *
     * @param citizenCardNumber the citizen card number
     */
    public void checkCitizenCardNumber (String citizenCardNumber) {
        if (StringUtils.isBlank(citizenCardNumber)) {
            throw new IllegalArgumentException("Citizen card number cannot be blank.");
        }
        if (citizenCardNumber.length() != 8 || !citizenCardNumber.matches("[0-9]+")) {
            throw new IllegalArgumentException("Citizen card number isn't valid.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SNSUser)) return false;
        SNSUser snsUser = (SNSUser) o;
        return Objects.equals(name, snsUser.name) && Objects.equals(sex, snsUser.sex) && Objects.equals(birthDate, snsUser.birthDate) && Objects.equals(address, snsUser.address) && Objects.equals(phoneNumber, snsUser.phoneNumber) && Objects.equals(email, snsUser.email) && Objects.equals(snsUserNumber, snsUser.snsUserNumber) && Objects.equals(citizenCardNumber, snsUser.citizenCardNumber);
    }

    /**
     * Role gets checked with the parameter established by the client
     *
     * @param role - Data entered by the Administrator send to verification
     */
    public void checkRole(String role){
        if(StringUtils.isBlank(role)){
            throw new IllegalArgumentException("Typed role cannot be blank.");
        }
        List<UserRole> OrganizationRolesList = new OrganizationRolesStore().getOrganizationRolesList();
        boolean roleExists = false;
        for(UserRole re : OrganizationRolesList){
            if(role.equalsIgnoreCase(re.getId())){
                roleExists = true;
            }
        }
        if(!roleExists){
            throw new IllegalArgumentException("Typed role must be from the available roles list.");
        }

    }

}
