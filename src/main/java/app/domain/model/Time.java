package app.domain.model;

import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.Calendar;
import java.util.Date;

public class Time {
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;

    private final int HOUR_NULL = 0;
    private final int MINUTE_NULL = 0;

    /**
     * Constructor which transforms date format in a Time object
     * @param occasion date object
     */
    public Time(Date occasion){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String[] dateDivided = dateFormat.format(occasion).split("/");
        year = Integer.parseInt(dateDivided[2]);
        month = Integer.parseInt(dateDivided[1]);
        day = Integer.parseInt(dateDivided[0]);
        hour = HOUR_NULL;
        minute = MINUTE_NULL;
    }

    /**
     * Constructor which transforms string format date in a Time object
     * @param occasion date in string format
     */
    public Time(String occasion) {
        String[] date = occasion.split("/");
        year = Integer.parseInt(date[2]);
        month = Integer.parseInt(date[1]);
        day = Integer.parseInt(date[0]);
        hour = HOUR_NULL;
        minute = MINUTE_NULL;
    }

    /**
     * Constructor which transforms string format date and string format time in a Time object
     * @param occasion string format date
     * @param hourAndMinutes string format time
     */
    public Time(String occasion, String hourAndMinutes) {
        String[] date = occasion.split("/");
        year = Integer.parseInt(date[2]);
        month = Integer.parseInt(date[1]);
        day = Integer.parseInt(date[0]);

        String[] hour = hourAndMinutes.split(":");
        this.hour = Integer.parseInt(hour[0]);
        this.minute = Integer.parseInt(hour[1]);

    }

    /**
     * Constructor which transforms a integer year, integer month and integer day in a Time object
     * @param year integer year
     * @param month integer month
     * @param day integer day
     */
    public Time(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
        hour =HOUR_NULL;
        minute = MINUTE_NULL;
    }

    /**
     * Constructor which transforms a Time object into another Time object
     * @param otherTime Time object
     */
    public Time(Time otherTime) {
        year = otherTime.getYear();
        month = otherTime.getMonth();
        day = otherTime.getDay();
        hour = otherTime.getHour();
        minute = otherTime.getMinute();
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    /**
     * Adds a day
     */
    public void addDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day);
        calendar.add(Calendar.DATE, 1);

        year = calendar.get(Calendar.YEAR);
        month = (calendar.get(Calendar.MONTH) + 1);
        day = calendar.get(Calendar.DATE);
    }

    /**
     * Subtracts a day
     */
    public void subtractDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day);
        calendar.add(Calendar.DATE, -1);

        year = calendar.get(Calendar.YEAR);
        month = (calendar.get(Calendar.MONTH) + 1);
        day = calendar.get(Calendar.DATE);
    }

    /**
     * Subtracts a week
     */
    public void subtractWeek() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day);
        calendar.add(Calendar.WEEK_OF_MONTH, -1);

        year = calendar.get(Calendar.YEAR);
        month = (calendar.get(Calendar.MONTH) + 1);
        day = calendar.get(Calendar.DATE);
    }

    /**
     * Subtracts a month
     */
    public void subtractMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day);
        calendar.add(Calendar.MONTH, -1);

        year = calendar.get(Calendar.YEAR);
        month = (calendar.get(Calendar.MONTH) + 1);
        day = calendar.get(Calendar.DATE);
    }

    /**
     * Subtracts a year
     */
    public void subtractYear() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day);
        calendar.add(Calendar.YEAR, -1);

        year = calendar.get(Calendar.YEAR);
        month = (calendar.get(Calendar.MONTH) + 1);
        day = calendar.get(Calendar.DATE);
    }

    /**
     * Counts the day
     * @return number of day
     */
    public int dayCounter() {
        int totalDays = 0;

        for (int i = 1; i < year; i++) {
            totalDays += isYearBissextile(i) ? 366 : 365;
        }
        for (int i = 1; i < month; i++) {
            YearMonth date = YearMonth.of(year, i);
            totalDays += date.lengthOfMonth();
        }
        totalDays += (isYearBissextile(year) && month > 2) ? 1 : 0;
        totalDays += day;

        return totalDays;
    }

    /**
     * Checks if the year is bissextile
     * @param year year to be checked
     * @return boolean true if year is bissextile or false if year isn't bissextile
     */
    public static boolean isYearBissextile(int year) {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }

    /**
     * Checks if day is a sunday
     * @return boolean true if the day is a sunday or false if the day isn't a sunday
     */
    public boolean isSunday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day); //january is 0
        return (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY);
    }

    /**
     * Turns hours and minutes into minutes
     * @return total minutes
     */
    public int getTotalMinutes() {
        return minute + hour * 60;
    }

    /**
     * Adds 30 minutes
     */
    public void add30Minutes() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day, hour, minute);
        calendar.add(Calendar.MINUTE, 30);

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DATE);
        if (((hour == 11 && minute >= 30) || hour > 11) && hour < 24) {
            hour = 12 + calendar.get(Calendar.HOUR);
        } else {
            hour = calendar.get(Calendar.HOUR);
        }
        minute = calendar.get(Calendar.MINUTE);
    }

    /**
     * Gets end of the week day
     * @return last day of the week
     */
    public int getEndOfTheWeekDay(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month-1, day);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * Gets last day of the month
     * @return last day of the month
     */
    public int getLastDayOfMonth(){
        YearMonth date = YearMonth.of(year, month);
        return date.lengthOfMonth();
    }

    /**
     * Gets last month of the year
     * @return last month of the year
     */
    public int getLastMonthOfYear(){
        YearMonth date = YearMonth.of(year, month);
        return date.lengthOfYear();
    }

}
