package app.domain.model;

import org.apache.commons.lang3.StringUtils;

import java.util.InputMismatchException;
import java.util.Objects;

public class VaccinationCenter {
    private String name;
    private String address;
    private String phoneNumber;
    private String area;
    private String mailAddress;
    private String faxNumber;
    private String webAddress;
    private String openingHour;
    private String closingHour;
    private String slotDuration;
    private String maxVacSlot;

    /**
     * Object employee is created and the attributes are sent to verification
     *
     * @param name          - Data entered by Administrator.
     * @param address       - Data entered by Administrator.
     * @param phoneNumber   - Data entered by Administrator.
     * @param area          - Data entered by Administrator.
     * @param mailAddress   - Data entered by Administrator.
     * @param faxNumber     - Data entered by Administrator.
     * @param webAddress    - Data entered by Administrator.
     * @param openingHour   - Data entered by Administrator.
     * @param closingHour   - Data entered by Administrator.
     * @param slotDuration  - Data entered by Administrator.
     * @param maxVacSlot    - Data entered by Administrator.
     */

    public VaccinationCenter(String name, String address, String phoneNumber, String area, String mailAddress, String faxNumber, String webAddress, String openingHour, String closingHour, String slotDuration, String maxVacSlot) {

        checkName(name);
        checkAddress(address);
        checkPhoneNumber(phoneNumber);
        checkArea(area);
        checkMailAddress(mailAddress);
        checkFaxNumber(faxNumber);
        checkWebAdress(webAddress);
        checkOpeningHour(openingHour);
        checkClosingHour(closingHour);
        checkSlotDuration(slotDuration);
        checkMaxVacSlot(maxVacSlot);

        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.area = area;
        this.mailAddress = mailAddress;
        this.faxNumber = faxNumber;
        this.webAddress = webAddress;
        this.openingHour = openingHour;
        this.closingHour = closingHour;
        this.slotDuration = slotDuration;
        this.maxVacSlot = maxVacSlot;
    }

    public VaccinationCenter (){

    }

    @Override
    public String toString() {
        return "VaccinationCenter{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", area=" + area +
                ", mailAddress='" + mailAddress + '\'' +
                ", faxNumber=" + faxNumber +
                ", webAddress='" + webAddress + '\'' +
                ", openingHour=" + openingHour +
                ", closingHour=" + closingHour +
                ", slotDuration=" + slotDuration +
                ", maxVacSlot=" + maxVacSlot +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        checkName(name);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        checkAddress(address);
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        checkPhoneNumber(phoneNumber);
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
        checkArea(area);
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
        checkMailAddress(mailAddress);
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
        checkFaxNumber(faxNumber);
    }

    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
        checkWebAdress(webAddress);
    }

    public String getOpeningHour() {
        return openingHour;
    }

    public void setOpeningHour(String openingHour) {
        this.openingHour = openingHour;
        checkOpeningHour(openingHour);
    }

    public String getClosingHour() {
        return closingHour;
    }

    public void setClosingHour(String closingHour) {
        this.closingHour = closingHour;
        checkClosingHour(closingHour);
    }

    public String getSlotDuration() {
        return slotDuration;
    }

    public void setSlotDuration(String slotDuration) {
        this.slotDuration = slotDuration;
        checkSlotDuration(slotDuration);
    }

    public String getMaxVacSlot() {
        return maxVacSlot;
    }

    public void setMaxVacSlot(String maxVacSlot) {
        this.maxVacSlot = maxVacSlot;
        checkMaxVacSlot(maxVacSlot);
    }

    private void checkName(String name) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Name cannot be blank.");
        }

        if(!name.replaceAll(" ","").matches("[a-zA-Z]+")) {
            throw new InputMismatchException("Insert a valid name");
        }
    }

    private void checkAddress(String address) {
        if (StringUtils.isBlank(address)) {
            throw new IllegalArgumentException("Address cannot be blank.");
        }
    }

    private void checkPhoneNumber (String phoneNumber){
        if (StringUtils.isBlank(phoneNumber)) {
            throw new IllegalArgumentException("Phone Number cannot be blank.");
        }

        if (phoneNumber.length() != 12 || !phoneNumber.matches("[0-9]+")) {
            throw new InputMismatchException("Phone Number must have 12 numbers.");
        }

        if(!phoneNumber.replaceAll(" ", "").matches("[0-9]+")) {
            throw new InputMismatchException("Phone Number cannot contain non chars.");
        }

    }

    private void checkArea(String area) {
        if (StringUtils.isBlank(area)) {
            throw new IllegalArgumentException("Area cannot be blank.");
        }

        if(!area.replaceAll(" ","").matches("[a-zA-Z]+")) {
            throw new InputMismatchException("Insert a valid area");
        }
    }
    //!phoneNumber.replaceAll(" ", "").matches("[0-9]+")
    private void checkMailAddress(String mailAddress) {
        if (StringUtils.isBlank(mailAddress)) {
            throw new IllegalArgumentException("Mail Address cannot be blank.");
        }
    }

    private void checkFaxNumber(String faxNumber) {
        if (StringUtils.isBlank(faxNumber)) {
            throw new IllegalArgumentException("Fax Number cannot be blank.");
        }

        if(!faxNumber.replaceAll(" ", "").matches("[0-9]+")) {
            throw new InputMismatchException("Insert a valid Fax Number");
        }
    }

    private void checkWebAdress(String webAddress) {
        if (StringUtils.isBlank(webAddress)) {
            throw new IllegalArgumentException("Web Address cannot be blank.");
        }
    }

    private void checkOpeningHour(String openingHour) {
        if (StringUtils.isBlank(openingHour)) {
            throw new IllegalArgumentException("Opening Hour cannot be blank.");
        }

        if (Integer.parseInt(openingHour)>23 || Integer.parseInt(openingHour)<0){
            throw new InputMismatchException("Opening hour is invalid");
        }

        if(!openingHour.replaceAll(" ", "").matches("[0-9]+")) {
            throw new InputMismatchException("Insert a valid Opening Hour");
        }
    }

    private void checkClosingHour(String closingHour) {
        if (StringUtils.isBlank(closingHour)) {
            throw new IllegalArgumentException("Closing Hour cannot be blank.");
        }

        if (Integer.parseInt(closingHour)>23 || Integer.parseInt(closingHour)<0){
            throw new InputMismatchException("Closing hour is invalid");
        }

        if(!closingHour.replaceAll(" ", "").matches("[0-9]+")) {
            throw new InputMismatchException("Insert a valid Closing Hour");
        }
    }

    private void checkSlotDuration(String slotDuration) {
        if (StringUtils.isBlank(slotDuration)) {
            throw new IllegalArgumentException("Slot Duration cannot be blank.");
        }

        if(!slotDuration.replaceAll(" ", "").matches("[0-9]+")) {
            throw new InputMismatchException("Slot Duration is invalid");
        }
    }

    private void checkMaxVacSlot(String maxVacSlot) {
        if (StringUtils.isBlank(maxVacSlot)) {
            throw new IllegalArgumentException("Maximum vaccines per slot cannot be blank.");
        }

        if(!maxVacSlot.replaceAll(" ", "").matches("[0-9]+")) {
            throw new InputMismatchException("Insert a valid amount for maximum vaccines per slot");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VaccinationCenter that = (VaccinationCenter) o;
        return Objects.equals(name, that.name) && Objects.equals(address, that.address) && Objects.equals(phoneNumber, that.phoneNumber) && Objects.equals(area, that.area) && Objects.equals(mailAddress, that.mailAddress) && Objects.equals(faxNumber, that.faxNumber) && Objects.equals(webAddress, that.webAddress) && Objects.equals(openingHour, that.openingHour) && Objects.equals(closingHour, that.closingHour) && Objects.equals(slotDuration, that.slotDuration) && Objects.equals(maxVacSlot, that.maxVacSlot);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, phoneNumber, area, mailAddress, faxNumber, webAddress, openingHour, closingHour, slotDuration, maxVacSlot);
    }
}