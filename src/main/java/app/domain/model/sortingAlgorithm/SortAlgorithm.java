package app.domain.model.sortingAlgorithm;

import app.domain.model.VaccinationData;

import java.util.List;

    public interface SortAlgorithm {

        public List<String> sortInitialDate(List<VaccinationData> list);

        public List<String> sortLeftDate(List<VaccinationData> list);
    }
