package app.domain.model.sortingAlgorithm;

import app.domain.model.VaccinationData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuickSort implements SortAlgorithm {

    public List<String> sortInitialDate(List<VaccinationData> vaccinationData) {
        List<String> dates = new ArrayList<>();
        int start = 0;
        int end = vaccinationData.size()-1;

        for (VaccinationData vaccinationData1 : vaccinationData) {
            dates.add(vaccinationData1.getArrivalDateAndTime());
        }
        quickSort(dates, start, end);
        return dates;
    }

    public List<String> sortLeftDate (List<VaccinationData> vaccinationData) {
        List<String> dates = new ArrayList<>();
        int start = 0;
        int end = vaccinationData.size()-1;

        for (VaccinationData vaccinationData1 : vaccinationData) {
            dates.add(vaccinationData1.getLeavindDateandTime());
        }
        quickSort(dates, start, end);
        return dates;
    }

    public void quickSort(List<String> list, int start, int end) {
        int i = start;
        int k = end;
        String aux;
        if (end - start >= 1) {
            String pivot = list.get(start);
            while (k > i) {
                while (list.get(i).compareTo(pivot) <= 0 && i <= end && k > i)
                    i++;
                while (list.get(k).compareTo(pivot) > 0 && k >= start && k >= i)
                    k--;
                if (k > i)
                    Collections.swap(list, i, k);
            }
            Collections.swap(list, start, k);
            quickSort(list, start, k - 1);
            quickSort(list, k + 1, end);
        }
    }
}
