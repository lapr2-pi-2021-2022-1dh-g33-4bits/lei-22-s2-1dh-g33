package app.domain.model.sortingAlgorithm;

import app.domain.model.VaccinationData;

import java.util.ArrayList;
import java.util.List;

public class MergeSort implements SortAlgorithm {

    public List<String> sortInitialDate (List<VaccinationData> vaccinationData) {
        List<String> dates = new ArrayList<>();
        int start = 0;
        int end = vaccinationData.size() - 1;

        for (VaccinationData vaccinationData1 : vaccinationData) {
            dates.add(vaccinationData1.getArrivalDateAndTime());
        }
        mergeSort(dates, start, end);
        return dates;
    }

    public List<String> sortLeftDate (List<VaccinationData> vaccinationData) {
        List<String> dates = new ArrayList<>();
        int start = 0;
        int end = vaccinationData.size() - 1;

        for (VaccinationData vaccinationData1 : vaccinationData) {
            dates.add(vaccinationData1.getLeavindDateandTime());
        }
        mergeSort(dates, start, end);
        return dates;
    }

    public static void mergeSort(List<String> list, int start, int end) {
        if ((end - start) >= 1) {//if arraylist size is greater than 1
            int halfPart = (start + end) / 2; //dividing the arraylist
            int start1 = start;
            int end1 = halfPart;
            int start2 = halfPart + 1;
            int end2 = end;
            mergeSort(list, start1, end1); //mergesort on first half
            mergeSort(list, start2, end2); //mergesort on second half

            //merge


            int start3 = start1;
            int end3 = end2;
            List<String> finalList = new ArrayList<String>();
            while (start1 <= end1 && start2 <= end2) { //alphabetically sorting both half arrays
                if (list.get(start1).compareTo(list.get(start2)) < 0) {//placing smallest element first
                    finalList.add(list.get(start1));
                    start1++;
                } else {
                    finalList.add(list.get(start2));
                    start2++;
                }
            }
            //add leftovers in half-arraylists
            while (start1 <= end1) {
                finalList.add(list.get(start1));
                start1++;
            }
            while (start2 <= end2) {
                finalList.add(list.get(start2));
                start2++;
            }

            for (int i = 0; i < finalList.size() && start3 <= end3; i++, start3++) {
                list.set(start3, finalList.get(i));
            }

        }
    }
}