package app.domain.model.sortingAlgorithm;

import app.domain.model.VaccinationData;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class SortingAlgorithmDemonstration implements Runnable {
    private final String sortClass = "sort";
    private final String filePath = "config.properties";

    @Override
    public void run() {
        try {

            int min = 1000000000;
            int max = 1001000000;

            List<VaccinationData> vaccinationDataList = new ArrayList<>();

            for (int i = 1000000000; i < max; i++) {
                vaccinationDataList.add(new VaccinationData());
            }
            System.out.println("quick: ");
            long start_time = System.nanoTime();
            Properties properties = new Properties();
            InputStream in = new FileInputStream(filePath);
            properties.load(in);
            Class<?> apiClass = Class.forName(properties.getProperty(sortClass));
            SortAlgorithm adapter = (SortAlgorithm) apiClass.getDeclaredConstructor().newInstance();
            in.close();
            adapter.sortInitialDate(vaccinationDataList);
            long end_time = System.nanoTime();
            double difference = (end_time - start_time) / 1000000;
            System.out.print(difference);
            System.out.println("\nmerge: ");
            long start_time1 = System.nanoTime();
            Properties properties1 = new Properties();
            InputStream in1 = new FileInputStream("src/main/resources/properties.properties");
            properties1.load(in1);
            Class<?> apiClass1 = Class.forName(properties.getProperty(sortClass));
            SortAlgorithm adapter1 = (SortAlgorithm) apiClass1.getDeclaredConstructor().newInstance();
            ;
            in.close();
            adapter1.sortLeftDate(vaccinationDataList);
            end_time = System.nanoTime();
            difference = (end_time - start_time1) / 1000000;
            System.out.println(difference);
        } catch (IOException | ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}