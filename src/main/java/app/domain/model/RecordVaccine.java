package app.domain.model;

import org.apache.commons.lang3.StringUtils;

import java.util.InputMismatchException;

/**
 * The type Record vaccine.
 */
public class RecordVaccine {
    private String date;
    private String vacType;
    private String vacName;
    private String lotNumber;
    private String snsUserNumber;

    /**
     * Instantiates a new Record vaccine.
     */
    public RecordVaccine(){

    }

    /**
     * Instantiates a new Record vaccine.
     *
     * @param date          the date
     * @param vacType       the vac type
     * @param vacName       the vac name
     * @param lotNumber     the lot number
     * @param snsUserNumber the sns user number
     */
    public RecordVaccine(String date, String vacType,String vacName,String lotNumber,String snsUserNumber){
        this.date = date;
        this.vacType = vacType;
        this.vacName = vacName;
        this.lotNumber = lotNumber;
        this.snsUserNumber = snsUserNumber;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Gets vac type.
     *
     * @return the vac type
     */
    public String getVacType() {
        return vacType;
    }

    /**
     * Sets vac type.
     *
     * @param vacType the vac type
     */
    public void setVacType(String vacType) {
        this.vacType = vacType;
    }

    /**
     * Gets vac name.
     *
     * @return the vac name
     */
    public String getVacName() {
        return vacName;
    }

    /**
     * Sets vac name.
     *
     * @param vacName the vac name
     */
    public void setVacName(String vacName) {
        this.vacName = vacName;
    }

    /**
     * Gets lot number.
     *
     * @return the lot number
     */
    public String getLotNumber() {
        return lotNumber;
    }

    /**
     * Sets lot number.
     *
     * @param lotNumber the lot number
     */
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
        checkLotNumber(lotNumber);
    }

    /**
     * Check lot number.
     *
     * @param lotNumber the lot number
     */
    public void checkLotNumber(String lotNumber){
        if (StringUtils.isBlank(lotNumber)) {
            throw new IllegalArgumentException("Sns User Number cannot be blank.");
        }

        if (!validateLotNumber(lotNumber)) {
            throw new InputMismatchException("Sns User Number cannot contain non chars.");
        }
    }

    /**
     * Validate lot number format.
     *
     * @param lotNumber the lot number
     * @return the boolean
     */
    public boolean validateLotNumber(String lotNumber){
            String[] lot = lotNumber.split("-");
            if (lot[0].length()==5){
                if (lot[1].length()==2 && lot[1].replaceAll(" ", "").matches("[0-9]+")) {
                    return true;
                }
                return false;
            }
        return false;
    }

    /**
     * Gets sns user number.
     *
     * @return the sns user number
     */
    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    /**
     * Sets sns user number.
     *
     * @param snsUserNumber the sns user number
     */
    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }
}
