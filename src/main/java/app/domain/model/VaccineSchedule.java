package app.domain.model;

import app.controller.App;
import app.domain.store.SNSUserDataStore;
import app.domain.store.VaccineScheduleStore;
import app.domain.store.VaccineStore;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.Locale;

public class VaccineSchedule {

    private SNSUserDataStore store = new SNSUserDataStore();
    private VaccineScheduleStore vstore = new VaccineScheduleStore();
    private VaccineStore vacs = new VaccineStore();
    private String snsUserNumber;
    private String vaccinationCenterName;
    private String date;
    private String time;
    private String vacType;
    private Company company;


    /**
     * Object employee is created and the attributes are sent to verification
     *
     * @param snsUserNumber         - Data entered by SNS User.
     * @param vaccinationCenterName - Data entered by SNS User.
     * @param date                  - Data entered by SNS User.
     * @param time                  - Data entered by SNS User.
     * @param vacType               - Data entered by SNS User.
     */


    public VaccineSchedule(String snsUserNumber, String vaccinationCenterName, String date,String time, String vacType) {
        this.snsUserNumber = snsUserNumber;
        this.vaccinationCenterName = vaccinationCenterName;
        this.date = date;
        this.time = time;
        this.vacType = vacType;
    }

    public VaccineSchedule(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }

    public VaccineSchedule(){
        this(App.getInstance().getCompany());
    }

    public VaccineSchedule(Company company){
        this.company = company;
        this.store = this.company.getSNSUserDataStore();
    }


    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
        checksnsUserNumber(snsUserNumber);
    }

    public String getVaccinationCenterName() {
        return vaccinationCenterName;
    }

    public void setVaccinationCenterName(String vaccinationCenterName) {
        this.vaccinationCenterName = vaccinationCenterName;
        checkVaccinationCenterName(vaccinationCenterName);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) throws ParseException {
        this.date = date;
        checkDate(date);
    }

    public String getVacType() {
        return vacType;
    }

    public void setVacType(String vacType) {
        this.vacType = vacType;
        checkVacType(vacType);
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
        checkTime(time);
    }

    public void checksnsUserNumber(String snsUserNumber) {
        if (StringUtils.isBlank(snsUserNumber)) {
            throw new IllegalArgumentException("Sns User Number cannot be blank.");
        }

        if (!snsUserNumber.replaceAll(" ", "").matches("[0-9]+")) {
            throw new InputMismatchException("Sns User Number cannot contain non chars.");
        }

        if (snsUserNumber.length() != 9 || !snsUserNumber.matches("[0-9]+")) {
            throw new InputMismatchException("Sns User Number must have 9 numbers.");
        }

        if (!searchForSnsUser(snsUserNumber)){
            throw new InputMismatchException("Sns User isn't registered.");
        }

    }


    public boolean searchForSnsUser(String snsUserNumber) {

        for (int i = 0; i < this.store.getSNSUserList().size(); i++) {
            if (this.store.getSNSUserList().get(i).getSnsUserNumber().equals(snsUserNumber)) {
                return true;
            }
        }
        return false;

    }

    public void checkVaccinationCenterName(String vaccinationCenterName) {
        if (StringUtils.isBlank(vaccinationCenterName)) {
            throw new IllegalArgumentException("Vaccination Center Name cannot be blank.");
        }

        if (!vaccinationCenterName.replaceAll(" ", "").toLowerCase(Locale.ROOT).replaceAll("[^\\p{ASCII}]", "").matches("[a-zA-Z]+")) {
            throw new InputMismatchException("Vaccination Center Name cannot contain numbers.");
        }
    }

    public boolean validateDate(String date) {

        LocalDateTime ldt;
        String[] separated = date.split("/");
        if (separated.length==3) {
            if (separated[0].length()==1 && separated[1].length()==2) {
                date = "0" + separated[0] + "/" + separated[1]+"/"+separated[2];
                this.date = date;
            }
            if (separated[0].length()==2 && separated[1].length()==1) {
                date = separated[0] + "/0" + separated[1]+"/"+separated[2];
                this.date = date;
            }
            if (separated[0].length()==1 && separated[1].length()==1) {
                date = "0" + separated[0] + "/0" + separated[1]+"/"+separated[2];
                this.date = date;
            }
        }
        DateTimeFormatter fomatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        try {
            ldt = LocalDateTime.parse(date, fomatter);
            String result = ldt.format(fomatter);
            return result.equals(date);
        } catch (DateTimeParseException e) {
            try {
                LocalDate ld = LocalDate.parse(date, fomatter);
                String result = ld.format(fomatter);
                return result.equals(date);
            } catch (DateTimeParseException exp) {
                try {
                    LocalTime lt = LocalTime.parse(date, fomatter);
                    String result = lt.format(fomatter);
                    return result.equals(date);
                } catch (DateTimeParseException e2) {
                    //Debugging purposes
                    //e2.printStackTrace();
                }
            }
        }
        return false;
    }

    public static boolean validateTime(String time) {
        try {
            String[] timeArray = time.split(":");
            return  Integer.parseInt(timeArray[0]) < 24 && Integer.parseInt(timeArray[1]) < 60;
        } catch (Exception e) {
            return false;
        }
    }

    public void checkTime(String time) {
        if (StringUtils.isBlank(time)) {
            throw new IllegalArgumentException("Time cannot be blank.");
        }
        if (!validateTime(time)) {
            throw new IllegalStateException("Insert a time following the example: i.e., 18:03");
        }
    }

    public void checkDate(String date) {
        if (StringUtils.isBlank(date)) {
            throw new IllegalArgumentException("Date cannot be blank.");
        }
        if (!validateDate(date)) {
            throw new IllegalStateException("Insert a date following the example: i.e., 18/12/2022");
        }
    }

    public void checkVacType(String vacType) {
        if (StringUtils.isBlank(vacType)) {
            throw new IllegalArgumentException("Vaccine Type cannot be blank.");
        }
    }
}