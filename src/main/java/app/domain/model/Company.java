package app.domain.model;

import app.domain.store.*;
import auth.AuthFacade;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Company {
    private String designation;
    private AuthFacade authFacade;
    private VaccinationCenterStore vaccinationCenterStore;
    private OrganizationRolesStore ors;
    private EmployeeStore es;
    private VaccineStore vaccineStore;
    private VaccineTypesStore vaccineTypesStore;
    private VaccineScheduleStore vaccineScheduleStore;
    private SNSUserDataStore SNSUserDataStore;
    private WaitingRoomStore wr;
    private RecoveryRoomStore rr;
    private RecordVaccineStore recordVaccineStore;
    private AdverseReactionsStore adverseReactionsStore;
    private VaccinationDataStore vaccinationDataStore;
    private VaccinationHistoryStore vaccinationHistoryStore;
    private VaccinationStatisticsStore vaccinationStatisticsStore;


    public Company(String designation)
    {
        if (StringUtils.isBlank(designation))
            throw new IllegalArgumentException("Designation cannot be blank.");

        this.designation = designation;
        this.authFacade = new AuthFacade();
        this.vaccinationCenterStore = new VaccinationCenterStore();
        this.vaccineStore = new VaccineStore();
        this.vaccineTypesStore = new VaccineTypesStore();
        this.vaccineScheduleStore = new VaccineScheduleStore();
        this.ors = new OrganizationRolesStore();
        this.es = new EmployeeStore();
        this.SNSUserDataStore = new SNSUserDataStore();
        this.wr = new WaitingRoomStore();
        this.rr = new RecoveryRoomStore();
        this.recordVaccineStore = new RecordVaccineStore();
        this.adverseReactionsStore = new AdverseReactionsStore();
        this.vaccinationDataStore = new VaccinationDataStore();
        this.vaccinationHistoryStore = new VaccinationHistoryStore();
        this.vaccinationStatisticsStore = new VaccinationStatisticsStore();
    }

    public VaccinationStatisticsStore getVaccinationStatisticsStore() {
        return vaccinationStatisticsStore;
    }

    public String getDesignation() {
        return designation;
    }

    public VaccinationCenterStore getVaccinationCenterStore() {
        return vaccinationCenterStore;
    }

    public AuthFacade getAuthFacade() {
        return authFacade;
    }

    public VaccineStore getVaccineStore() {
        return vaccineStore;
    }

    public VaccineTypesStore getVaccineTypesStore() {
        return vaccineTypesStore;
    }

    public VaccineScheduleStore getVaccineScheduleStore() {
        return vaccineScheduleStore;
    }

    public OrganizationRolesStore getOrganizationRolesStore(){
        return ors;
    }

    public EmployeeStore getEmployeeStore() { return es; }

    public SNSUserDataStore getSNSUserDataStore() {
        return SNSUserDataStore;
    }

    public WaitingRoomStore getWaitingRoomStore() { return wr; }

    public RecoveryRoomStore getRecoveryRoomStore() { return rr; }

    public RecordVaccineStore getRecordVaccineStore() {
        return recordVaccineStore;
    }

    public AdverseReactionsStore getAdverseReactionsStore() { return adverseReactionsStore; }

    public VaccinationDataStore getVaccinationDataStore() {return vaccinationDataStore;}

    public VaccinationHistoryStore getVaccinationHistoryStore() {
        return vaccinationHistoryStore;
    }

}
