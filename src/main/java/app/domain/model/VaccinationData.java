package app.domain.model;

import app.controller.App;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * The type Vaccination data.
 */


public class VaccinationData {
    public  String ArrivalDate;
    private String snsUserNumber; //check feito
    private String vacineType;//check feito
    private String dose;//ckeck feito
    private String loteNumber;
    private String scheduleDate;//check feito
    private String scheduleTime;//check feito
    private String arrivalDate;
    private String arrivalTime;//check feito
    private String arrivalDateAndTime;
    private String administrationDate;//check feito
    private String administrationTime;//check feito
    private String leavingTime;//check feito
    private String leavingDate;
    private String leavindDateandTime;

    private Company company;

    /**
     * Instantiates a new Vaccination data.
     *
     * @param snsUserNumber the sns user number
     * @param vacineType    the vacine type
     * @param dose          the dose
     * @param loteNumber    the lote number
     * @param scheduleDate  the schedule date
     * @param scheduleTime  the schedule time
     * @param arrivalDate   the arrival date
     * @param arrivalTime   the arrival time
     * @param leavingDate   the leaving date
     * @param leavingTime   the leaving time
     */
    public VaccinationData(String snsUserNumber, String vacineType, String dose, String loteNumber, String scheduleDate, String scheduleTime, String arrivalDate, String arrivalTime, String administrationDate, String administrationTime, String leavingDate, String leavingTime) throws ParseException {
        this.snsUserNumber = snsUserNumber;
        this.vacineType = vacineType;
        this.dose = dose;
        this.loteNumber = loteNumber;
        this.scheduleDate = checkDateFormat(scheduleDate);
        this.scheduleTime = checkHourFormat(scheduleTime);
        this.arrivalDate = checkDateFormat(arrivalDate);
        this.arrivalTime = checkHourFormat(arrivalTime);
        this.administrationDate = checkDateFormat(administrationDate);
        this.administrationTime = checkHourFormat(administrationTime);
        this.leavingDate = checkDateFormat(leavingDate);
        this.leavingTime = checkHourFormat(leavingTime);
        checkData(snsUserNumber,vacineType,dose,loteNumber,scheduleDate,scheduleTime,arrivalDate,arrivalTime,administrationDate,administrationTime,leavingDate, leavingTime);
        this.arrivalDateAndTime = checkDateFormat(arrivalDate) + " " + checkHourFormat(arrivalTime);
        this.leavindDateandTime = checkDateFormat(leavingDate) + " " + checkHourFormat(leavingTime);
    }

    public void checkData (String snsUserNumber, String vacineType, String dose, String loteNumber, String scheduleDate, String scheduleTime, String arrivalDate, String arrivalTime, String administrationDate, String administrationTime, String leavingDate, String leavingTime) throws ParseException {
        checkDateFormat(scheduleDate);
        checkDateFormat(arrivalDate);
        checkDateFormat(administrationDate);
        checkDateFormat(leavingDate);
        checkHourFormat(scheduleTime);
        checkHourFormat(arrivalTime);
        checkHourFormat(administrationTime);
        checkHourFormat(leavingTime);
        validateDose(dose);
    }


    public VaccinationData () { this(App.getInstance().getCompany()); }

    public VaccinationData (Company company) {
        this.company = company;
    }

    public String getArrivalDateAndTime() {
        return arrivalDateAndTime;
    }

    public String getLeavindDateandTime() {
        return leavindDateandTime;
    }

    public String getLeavingDate() {
        return leavingDate;
    }

    public void setLeavingDate(String leavingDate) {
        this.leavingDate = leavingDate;
    }

    public String getArrivalDate() {return arrivalDate;}

    public void setArrivalDate(String arrivalDate) {
        ArrivalDate = arrivalDate;
    }

    /**
     * Gets sns user number.
     *
     * @return the sns user number
     */
    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    /**
     * Sets sns user number.
     *
     * @param snsUserNumber the sns user number
     */
    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }

    /**
     * Gets vacine type.
     *
     * @return the vacine type
     */
    public String getVacineType() {
        return vacineType;
    }

    /**
     * Sets vacine type.
     *
     * @param vacineType the vacine type
     */
    public void setVacineType(String vacineType) {
        this.vacineType = vacineType;
    }

    /**
     * Gets dose.
     *
     * @return the dose
     */
    public String getDose() {
        return dose;
    }

    /**
     * Sets dose.
     *
     * @param dose the dose
     */
    public void setDose(String dose) {
        this.dose = dose;
    }

    /**
     * Gets lote number.
     *
     * @return the lote number
     */
    public String getLoteNumber() {
        return loteNumber;
    }

    /**
     * Sets lote number.
     *
     * @param loteNumber the lote number
     */
    public void setLoteNumber(String loteNumber) {
        this.loteNumber = loteNumber;
    }

    /**
     * Gets schedule date.
     *
     * @return the schedule date
     */
    public String getScheduleDate() {
        return scheduleDate;
    }

    /**
     * Sets schedule date.
     *
     * @param scheduleDate the schedule date
     */
    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    /**
     * Gets schedule time.
     *
     * @return the schedule time
     */
    public String getScheduleTime() {
        return scheduleTime;
    }

    /**
     * Sets schedule time.
     *
     * @param scheduleTime the schedule time
     */
    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    /**
     * Gets arrival time.
     *
     * @return the arrival time
     */
    public String getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets arrival time.
     *
     * @param arrivalTime the arrival time
     */
    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * Gets administration date.
     *
     * @return the administration date
     */
    public String getAdministrationDate() {
        return administrationDate;
    }

    /**
     * Sets administration date.
     *
     * @param administrationDate the administration date
     */
    public void setAdministrationDate(String administrationDate) {
        this.administrationDate = administrationDate;
    }

    /**
     * Gets administration time.
     *
     * @return the administration time
     */
    public String getAdministrationTime() {
        return administrationTime;
    }

    /**
     * Sets administration time.
     *
     * @param administrationTime the administration time
     */
    public void setAdministrationTime(String administrationTime) {
        this.administrationTime = administrationTime;
    }


    /**
     * Gets leaving time.
     *
     * @return the leaving time
     */
    public String getLeavingTime() {
        return leavingTime;
    }

    /**
     * Sets leaving time.
     *
     * @param leavingTime the leaving time
     */
    public void setLeavingTime(String leavingTime) {
        this.leavingTime = leavingTime;
    }

    /**
     * Check sns user number.
     *
     * @param snsUserNumber the sns user number
     */
    public void checkSNSUserNumber(String snsUserNumber) {
        if (StringUtils.isBlank(snsUserNumber)) {
            throw new IllegalArgumentException("SNS user number cannot be blank.");
        }
        if (snsUserNumber.length() != 9 || !snsUserNumber.matches("[0-9]+")) {
            throw new IllegalArgumentException("SNS user number isn't valid.");
        }
    }

    /**
     * Check if sns user number exists boolean.
     *
     * @param snsUserNumber the sns user number
     * @return the boolean
     */
    public boolean checkIfSnsUserNumberExists (String snsUserNumber){
       if( this.company.getSNSUserDataStore().getBySNSUserCardNumberExists(snsUserNumber)){
           return true;
       }
       return false;
    }


    /**
     * Check date format string.
     *
     * @param date the date
     * @return the string
     * @throws ParseException the parse exception
     */
    public String checkDateFormat(String date) throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat formatter2= new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = formatter.parse(date);
        return (formatter2.format(date1));
    }


    /**
     * Check hour format string.
     *
     * @param hour the hour
     * @return the string
     * @throws ParseException the parse exception
     */
    public String checkHourFormat (String hour) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        Date date1 = formatter.parse(hour);
        return (formatter.format(date1));
    }
    /**
     * Validate dose boolean.
     *
     * @param Dose the dose
     * @return the boolean
     */
    public boolean validateDose (String Dose) {
        if (dose.toUpperCase(Locale.ROOT).equals("First")){
            return true;
        }
        if (dose.toUpperCase(Locale.ROOT).equals("Second")) {
            return true;
        }
        if (dose.toUpperCase(Locale.ROOT).equals("Third")) {
            return true;
        }
        if (dose.toUpperCase(Locale.ROOT).equals("Fourth")) {
            return true;
        }
        if (dose.toUpperCase(Locale.ROOT).equals("Primeira")) {
            return true;
        }
        if (dose.toUpperCase(Locale.ROOT).equals("Segunda")) {
            return true;
        }
        if (dose.toUpperCase(Locale.ROOT).equals("Terceira")) {
            return true;
        }
        if (dose.toUpperCase(Locale.ROOT).equals("Quarta")) {
            return true;
        }
        return dose.toUpperCase(Locale.ROOT).equals("F");
    }

    @Override
    public String toString() {
        return "snsUserNumber: " + snsUserNumber + '\'' +
                ", vacineType: " + vacineType + '\'' +
                ", dose: " + dose + '\'' +
                ", loteNumber: " + loteNumber + '\'' +
                ", scheduleDate: " + scheduleDate + '\'' +
                ", scheduleTime: " + scheduleTime + '\'' +
                ", arrivalDate:" + arrivalDate + '\'' +
                ", arrivalTime: " + arrivalTime + '\'' +
                ", administrationDate:" + administrationDate + '\'' +
                ", administrationTime:" + administrationTime + '\'' +
                ", leavingTime:" + leavingTime + '\'' +
                ", leavingDate:" + leavingDate + '\'' +
                '}';
    }
}

