package app.domain.model.domain.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

public class User {
    private Email id;
    private Password password;
    private String name;
    private Set<UserRole> roles = new HashSet();

    public User(Email id, Password pwd, String name) {
        if (ObjectUtils.allNotNull(new Object[]{id, pwd}) && !StringUtils.isBlank(name)) {
            this.id = id;
            this.password = pwd;
            this.name = name.trim();
        } else {
            throw new IllegalArgumentException("User cannot have an id, password or name as null/blank.");
        }
    }

    public Email getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public boolean hasId(Email id) {
        return this.id.equals(id);
    }

    public boolean hasPassword(String pwd) {
        return this.password.checkPassword(pwd);
    }

    public boolean addRole(UserRole role) {
        return role != null ? this.roles.add(role) : false;
    }

    public boolean removeRole(UserRole role) {
        return role != null ? this.roles.remove(role) : false;
    }

    public boolean hasRole(UserRole role) {
        return this.roles.contains(role);
    }

    public boolean hasRole(String roleId) {
        Iterator var2 = this.roles.iterator();

        UserRole role;
        do {
            if (!var2.hasNext()) {
                return false;
            }

            role = (UserRole)var2.next();
        } while(!role.hasId(roleId));

        return true;
    }

    public List<UserRole> getRoles() {
        List<UserRole> list = new ArrayList();
        Iterator var2 = this.roles.iterator();

        while(var2.hasNext()) {
            UserRole role = (UserRole)var2.next();
            list.add(role);
        }

        return Collections.unmodifiableList(list);
    }

    public boolean changeName(String newName) {
        if (StringUtils.isBlank(newName)) {
            return false;
        } else {
            this.name = newName.trim();
            return true;
        }
    }

    public boolean changeRoles(List<UserRole> roles) {
        this.roles.clear();
        this.roles.addAll(roles);
        return this.roles.containsAll(roles);
    }

    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.id.hashCode();
        return hash;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o == null) {
            return false;
        } else if (this.getClass() != o.getClass()) {
            return false;
        } else {
            auth.domain.model.User obj = (auth.domain.model.User)o;
            return Objects.equals(this.id, obj.id);
        }
    }

    public String toString() {
        return String.format("%s - %s", this.id.toString(), this.name);
    }
}
