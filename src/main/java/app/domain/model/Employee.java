package app.domain.model;

import app.domain.store.OrganizationRolesStore;
import auth.domain.model.UserRole;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.regex.Pattern;


public class Employee {
    private String name;
    private String address;
    private String emailAddress;
    private String phoneNumber;
    private String socCode;
    private String id;
    private String NIF;
    private String role;


    /**
     * Object employee is created and the attributes are sent to verification
     *
     * @param name          - Data entered by Administrator.
     * @param address       - Data entered by Administrator.
     * @param emailAddress  - Data entered by Administrator.
     * @param phoneNumber   - Data entered by Administrator.
     * @param socCode       - Data entered by Administrator.
     * @param id            - Data entered by Administrator.
     * @param NIF           - Data entered by Administrator.
     * @param role          - Data entered by Administrator.
     */

    public Employee(String role, String name, String address, String emailAddress, String phoneNumber, String socCode, String id, String NIF) {
        checkAttributes(role, name, address,emailAddress,phoneNumber,socCode,id,NIF);
        this.name = name;
        this.address = address;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.socCode = socCode;
        this.id = id;
        this.NIF = NIF;
        this.role = role;

    }

    public Employee(){

    }

    /**
     * Return the employee name
     * @return - Name in a form of a String
     */
    public String getName() {
        return name;
    }

    /**
     * Return the employee address
     * @return - Address in a form of a String
     */
    public String getAddress() {
        return address;
    }

    /**
     * Return the employee email address
     * @return - Email Address in a form of a String
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Return the employee phone number
     * @return - Phone Number in a form of a String
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Return the employee standard occupational classification code
     * @return - Standard occupational classification in a form of a String
     */
    public String getSocCode() {
        return socCode;
    }

    /**
     * Return the employee ID
     * @return - Employee ID in a form of a String
     */
    public String getId() {
        return id;
    }

    /**
     * Return the employee tax identification
     * @return - Tax identification in a form of a String
     */
    public String getNIF() {
        return NIF;
    }

    /**
     * Return the employee role
     * @return - Role in a form of a String
     */
    public String getRole() {
        return role;
    }

    public void setName(String name) {
        this.name = name;
        checkName(name);
    }

    public void setAddress(String address) {
        this.address = address;
        checkAddress(address);
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        checkEmailAddress(emailAddress);
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        checkPhoneNumber(phoneNumber);
    }

    public void setSocCode(String socCode) {
        this.socCode = socCode;
        checkSocCode(socCode);
    }

    public void setId(String id) {
        this.id = id;
        checkID(id);
    }

    public void setNIF(String NIF) {
        this.NIF = NIF;
        checkNIF(NIF);
    }

    public void setRole(String role) {
        this.role = role;
        checkRole(role);
    }

    /**
     *
     * @param name          - Data entered by the Administrator send to verification
     * @param address       - Data entered by the Administrator send to verification
     * @param emailAddress  - Data entered by the Administrator send to verification
     * @param phoneNumber   - Data entered by the Administrator send to verification
     * @param socCode       - Data entered by the Administrator send to verification
     * @param id            - Data entered by the Administrator send to verification
     * @param NIF           - Data entered by the Administrator send to verification
     * @param role          - Data entered by the Administrator send to verification
     */
    public void checkAttributes(String role, String name, String address, String emailAddress, String phoneNumber, String socCode, String id, String NIF) {
        checkName(name);
        checkAddress(address);
        checkEmailAddress(emailAddress);
        checkPhoneNumber(phoneNumber);
        checkSocCode(socCode);
        checkID(id);
        checkNIF(NIF);
        checkRole(role);
    }

    /**
     * Name gets checked with the parameter established by the client
     * @param name - Data entered by the Administrator send to verification
     */
    public void checkName(String name){
        if(StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Name cannot be blank.");
        }

        if(name.replaceAll(" ", "").length() > 35 && !name.replaceAll(" ", "").matches("[a-zA-Z]+")) {
            throw new IllegalArgumentException(("Name cannot have more than 35 chars nor contain numbers."));
        }

        if(name.replaceAll(" ","").length() > 35) {
            throw new IllegalArgumentException("Name cannot have more than 35 chars.");
        }

        if(!name.replaceAll(" ","").matches("[a-zA-Z]+")) {
            throw new IllegalArgumentException("Name cannot contain numbers.");
        }
    }

    /**
     * Address gets checked with the parameter established by the client
     * @param address - Data entered by the Administrator send to verification
     */
    public void checkAddress(String address){
        if(StringUtils.isBlank(address)){
            throw new IllegalArgumentException("Address cannot be blank.");
        }

        if(address.replaceAll(" ","").length() > 35) {
            throw new IllegalArgumentException("Address cannot have more than 35 chars.");
        }
    }

    /**
     * Email address gets checked with the parameter established by the client
     * @param emailAddress - Data entered by the Administrator send to verification
     */
    public void checkEmailAddress(String emailAddress){
        if(StringUtils.isBlank(emailAddress)){
            throw new IllegalArgumentException("Email Address cannot be blank.");
        }
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (!pat.matcher(emailAddress).matches()) {
            throw new IllegalArgumentException("Email format is invalid.");
        }
    }

    /**
     * Phone Number gets checked with the parameter established by the client
     * @param phoneNumber - Data entered by the Administrator send to verification
     */
    public void checkPhoneNumber(String phoneNumber){
        if(StringUtils.isBlank(phoneNumber)){
            throw new IllegalArgumentException("Phone Number cannot be blank.");
        }

        if(phoneNumber.replaceAll(" ","").length() != 9 && !phoneNumber.replaceAll(" ", "").matches("[0-9]+")){
            throw new IllegalArgumentException("Phone Number must have 9 numbers and cannot contain non numeric chars.");
        }

        if(phoneNumber.replaceAll(" ","").length() != 9){
            throw new IllegalArgumentException("Phone Number must have 9 numbers.");
        }

    }

    /**
     * Standard occupational classification code gets checked with the parameter established by the client
     * @param socCode - Data entered by the Administrator send to verification
     */
    public void checkSocCode(String socCode){
        if (StringUtils.isBlank(socCode)) {
            throw new IllegalArgumentException("Standard occupational classification code cannot be blank.");
        }

        boolean format = socCode.trim().charAt(2) == '-';
        boolean length = socCode.trim().length() == 7;
        boolean components = socCode.matches(".*[a-z].*");

        if (!length && !format && components) {
            throw new IllegalArgumentException("Standard occupational classification code has a invalid format, cannot have non numeric chars and must have 7 chars");
        }

        if (!format && components) {
            throw new IllegalArgumentException("Standard occupational classification code has a invalid format and cannot have non numeric chars");
        }

        if (!format && !length) {
            throw new IllegalArgumentException("Standard occupational classification code has an invalid format and must have 7 chars.");
        }

        if (components && !length) {
            throw new IllegalArgumentException("Standard occupational classification code cannot have non numeric chars and must have 7 chars.");
        }

        if (!format) {
            throw new IllegalArgumentException("Standard occupational classification code has a invalid format");
        }

        if (components) {
            throw new IllegalArgumentException("Standard occupational classification code cannot contain non numeric chars.");
        }

        if (!length) {
            throw new IllegalArgumentException("Standard occupational classification code must have 7 chars.");
        }
    }

    /**
     * ID gets checked with the parameter established by the client
     * @param id - Data entered by the Administrator send to verification
     */
    public void checkID(String id){
        if(StringUtils.isBlank(id)){
            throw new IllegalArgumentException("Id cannot be blank.");
        }

        if(id.replaceAll(" ","").length() != 7 && !id.replaceAll(" ", "").matches("[0-9]+")) {
            throw new IllegalArgumentException("ID must have 7 numbers and cannot contain non numeric chars.");
        }

        if(id.replaceAll(" ","").length() != 7){
            throw new IllegalArgumentException("ID must have 7 numbers.");
        }

    }

    /**
     * Tax Identification gets checked with the parameter established by the client
     * @param NIF - Data entered by the Administrator send to verification
     */
    public void checkNIF(String NIF){
        if (StringUtils.isBlank(NIF))
            throw new IllegalArgumentException("NIF cannot be blank.");

        if (NIF.replaceAll(" ", "").length() != 9 && !NIF.replaceAll(" ", "").matches("[0-9]+")) {
            throw new IllegalArgumentException("NIF must have 11 numbers and cannot contain non numeric chars.");
        }
        if (NIF.replaceAll(" ", "").length() != 9) {
            throw new IllegalArgumentException("Phone number must have 11 numbers.");
        }

    }

    /**
     * Role gets checked with the parameter established by the client
     * @param role - Data entered by the Administrator send to verification
     */
    public void checkRole(String role){
        if(StringUtils.isBlank(role)){
            throw new IllegalArgumentException("Typed role cannot be blank.");
        }
        List<UserRole> OrganizationRolesList = new OrganizationRolesStore().getOrganizationRolesList();
        boolean roleExists = false;
        for(UserRole re : OrganizationRolesList){
            if(role.equalsIgnoreCase(re.getId())){
                roleExists = true;
            }
        }
        if(!roleExists){
            throw new IllegalArgumentException("Typed role must be from the available roles list.");
        }

    }
}
