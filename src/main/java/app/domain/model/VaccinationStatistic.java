package app.domain.model;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * The type Vaccination statistic.
 */
public class VaccinationStatistic {
    private String date;
    private String numberFullVaccinated;

    /**
     * Instantiates a new Vaccination statistic.
     *
     * @param date                 the date
     * @param numberFullVaccinated the number full vaccinated
     */
    public VaccinationStatistic (String date, String numberFullVaccinated) {
        this.date = date;
        this.numberFullVaccinated = numberFullVaccinated;
    }

    /**
     * Instantiates a new Vaccination statistic.
     */
    public VaccinationStatistic () {

    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(String date) {
        this.date = date;
        checkDate(date);
    }

    /**
     * Gets number full vaccinated.
     *
     * @return the number full vaccinated
     */
    public String getNumberFullVaccinated() {
        return numberFullVaccinated;
    }

    /**
     * Sets number full vaccinated.
     *
     * @param numberFullVaccinated the number full vaccinated
     */
    public void setNumberFullVaccinated(String numberFullVaccinated) {
        this.numberFullVaccinated = numberFullVaccinated;
    }


    /**
     * Validate date boolean.
     *
     * @param date the date
     * @return the boolean
     */
    public boolean validateDate (String date) {
        LocalDateTime ldt;
        String[] separated = date.split("/");
        if (separated.length==3) {
            if (separated[0].length()==1 && separated[1].length()==2) {
                date = "0" + separated[0] + "/" + separated[1]+"/"+separated[2];
                this.date = date;
            }
            if (separated[0].length()==2 && separated[1].length()==1) {
                date = separated[0] + "/0" + separated[1]+"/"+separated[2];
                this.date = date;
            }
            if (separated[0].length()==1 && separated[1].length()==1) {
                date = "0" + separated[0] + "/0" + separated[1]+"/"+separated[2];
                this.date = date;
            }
        }
        DateTimeFormatter fomatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        try {
            ldt = LocalDateTime.parse(date, fomatter);
            String result = ldt.format(fomatter);
            return result.equals(date);
        } catch (DateTimeParseException e) {
            try {
                LocalDate ld = LocalDate.parse(date, fomatter);
                String result = ld.format(fomatter);
                return result.equals(date);
            } catch (DateTimeParseException exp) {
                try {
                    LocalTime lt = LocalTime.parse(date, fomatter);
                    String result = lt.format(fomatter);
                    return result.equals(date);
                } catch (DateTimeParseException e2) {
                    //Debugging purposes
                    //e2.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * Check date.
     *
     * @param date the date
     */
    public void checkDate (String date) {
        if (StringUtils.isBlank(date)) {
            throw new IllegalArgumentException("Birth date cannot be blank.");
        }
        if (!validateDate(date)) {
            throw new IllegalArgumentException("Invalid birth date format.");
        }
    }


    @Override
    public String toString () {
        return (date + "; " + numberFullVaccinated);
    }

}
