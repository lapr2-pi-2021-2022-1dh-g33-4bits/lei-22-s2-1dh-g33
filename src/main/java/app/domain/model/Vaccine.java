package app.domain.model;

import app.domain.store.VaccineStore;
import org.apache.commons.lang3.StringUtils;

import java.util.InputMismatchException;
import java.util.Objects;

/**
 * The type Vaccine.
 */
public class Vaccine {

    private VaccineStore storeVaccine;
    private String ID;
    private String name;
    private String brand;
    private String vaccineType;
    private String ageGroup;
    private String doseNumber;
    private String vaccineDosage;
    private String  timeBetweenDoses;

    /**
     * Instantiates a new Vaccine.
     *
     * @param ID               the id
     * @param name             the name
     * @param brand            the brand
     * @param vaccineType      the vaccine type
     * @param ageGroup         the age group
     * @param doseNumber       the dose number
     * @param vaccineDosage    the vaccine dosage
     * @param timeBetweenDoses the time between doses
     */
    public Vaccine (String ID, String name, String brand, String vaccineType, String ageGroup, String doseNumber, String vaccineDosage, String timeBetweenDoses) {
        this.ID = ID;
        this.name = name;
        this.brand = brand;
        this.vaccineType = vaccineType;
        this.ageGroup = ageGroup;
        this.doseNumber = doseNumber;
        this.vaccineDosage = vaccineDosage;
        this.timeBetweenDoses = timeBetweenDoses;
        checkInputData(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
    }

    /**
     * Instantiates a new Vaccine.
     */
    public  Vaccine () {
    }

    @Override
    public String toString () {
        return "Vaccine:" +
                "ID='" + ID + '\'' +
                ", name='" + name + '\'' +
                ", brand=" + brand +
                ", vaccine type=" + vaccineType +
                ", age group='" + ageGroup + '\'' +
                ", dose number=" + doseNumber +
                ", vaccine dosage='" + vaccineDosage + '\'' +
                ", time between doses (days)=" + timeBetweenDoses +
                '}';
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getID() {
        return ID;
    }

    /**
     * Sets id.
     *
     * @param ID the id
     */
    public void setID(String ID) {
        this.ID = ID;
        checkID(ID);
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
        checkName(name);
    }

    /**
     * Gets brand.
     *
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets brand.
     *
     * @param brand the brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
        checkBrand(brand);
    }

    /**
     * Gets vaccine type.
     *
     * @return the vaccine type
     */
    public String getVaccineType() {
        return vaccineType;
    }

    /**
     * Sets vaccine type.
     *
     * @param vaccineType the vaccine type
     */
    public void setVaccineType(String vaccineType) {
        this.vaccineType = vaccineType;
        checkVaccineType(vaccineType);
    }

    /**
     * Gets age group.
     *
     * @return the age group
     */
    public String getAgeGroup() {
        return ageGroup;
    }

    /**
     * Sets age group.
     *
     * @param ageGroup the age group
     */
    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
        checkAgeGroup(ageGroup);
    }

    /**
     * Gets dose number.
     *
     * @return the dose number
     */
    public String getDoseNumber() {
        return doseNumber;
    }

    /**
     * Sets dose number.
     *
     * @param doseNumber the dose number
     */
    public void setDoseNumber(String doseNumber) {
        this.doseNumber = doseNumber;
        checkDoseNumber(doseNumber);
    }

    /**
     * Gets vaccine dosage.
     *
     * @return the vaccine dosage
     */
    public String getVaccineDosage() {
        return vaccineDosage;
    }

    /**
     * Sets vaccine dosage.
     *
     * @param vaccineDosage the vaccine dosage
     */
    public void setVaccineDosage(String vaccineDosage) {
        this.vaccineDosage = vaccineDosage;
        checkVaccineDosage(vaccineDosage);
    }

    /**
     * Gets time between doses.
     *
     * @return the time between doses
     */
    public String getTimeBetweenDoses() {
        return timeBetweenDoses;
    }

    /**
     * Sets time between doses.
     *
     * @param timeBetweenDoses the time between doses
     */
    public void setTimeBetweenDoses(String timeBetweenDoses) {
        this.timeBetweenDoses = timeBetweenDoses;
        checkTimeBetweenDoses(timeBetweenDoses);
    }


    /**
     * Check input data.
     *
     * @param ID               the id
     * @param name             the name
     * @param brand            the brand
     * @param vaccineType      the vaccine type
     * @param ageGroup         the age group
     * @param doseNumber       the dose number
     * @param vaccineDosage    the vaccine dosage
     * @param timeBetweenDoses the time between doses
     */
    public void checkInputData (String ID, String name, String brand, String vaccineType, String ageGroup, String doseNumber, String vaccineDosage, String timeBetweenDoses) {
        checkID(ID);
        checkName(name);
        checkBrand(brand);
        checkVaccineType(vaccineType);
        checkAgeGroup(ageGroup);
        checkDoseNumber(doseNumber);
        checkVaccineDosage((vaccineDosage));
        checkTimeBetweenDoses(timeBetweenDoses);
    }

    /**
     * Validate age group boolean.
     *
     * @param ageGroup the age group
     * @return the boolean
     */
    public boolean validateAgeGroup (String ageGroup) {
        int check = 0;
        String[] ageGroupArray = ageGroup.split(";");

        for (String a : ageGroupArray) {
            if ( !(a.matches("\\d{2}-\\d{2}")) ) {
                check++;
            }
        }
        return check != 0;
    }

    /**
     * Validate vaccine dosage boolean.
     *
     * @param vaccineDosage the vaccine dosage
     * @return the boolean
     */
    public boolean validateVaccineDosage (String vaccineDosage) {
        int check = 0;
        String[] vaccineDosageArray = vaccineDosage.split(";");
        for (String a : vaccineDosageArray) {
            if ( !(a.matches("^\\d+\\.\\d+")) && (Integer.parseInt(a)<=0 || Integer.parseInt(a)>=1)) {
                check++;
            }
            if (vaccineDosageArray.length != 2) {
                check++;
            }
        }
        return check != 0;
    }

    /**
     * Check id.
     *
     * @param ID the id
     */
    public void checkID (String ID) {
        if (StringUtils.isBlank(ID)) {
            throw new IllegalArgumentException("ID cannot be blank.");
        } /*
        if (Integer.parseInt(ID)>99999 || Integer.parseInt(ID)<10000) {
            throw new IllegalStateException("Insert a valid ID.");
        } */
        if (!ID.matches("[0-9]+")) {
            throw new InputMismatchException("Insert a valid ID.");
        }
    }

    /**
     * Check name.
     *
     * @param name the name
     */
    public void checkName (String name) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Name cannot be blank.");
        }
    }

    /**
     * Check brand.
     *
     * @param brand the brand
     */
    public void checkBrand (String brand) {
        if (StringUtils.isBlank(brand)) {
            throw new IllegalArgumentException("Brand cannot be blank.");
        }
    }

    /**
     * Check vaccine type.
     *
     * @param vaccineType the vaccine type
     */
    public void checkVaccineType (String vaccineType) {
        if (StringUtils.isBlank(vaccineType)) {
            throw new IllegalArgumentException("Vaccine Type cannot be blank.");
        }
    }

    /**
     * Check age group.
     *
     * @param ageGroup the age group
     */
    public void checkAgeGroup (String ageGroup) {
        if (StringUtils.isBlank(ageGroup)) {
            throw new IllegalArgumentException("Age group cannot be blank.");
        }
        if (validateAgeGroup(ageGroup)) {
            throw new IllegalStateException("Insert an age group following the example: i.e., 18-30;50-80");
        }
    }

    /**
     * Check dose number.
     *
     * @param doseNumber the dose number
     */
    public void checkDoseNumber (String doseNumber) {
        if (StringUtils.isBlank(doseNumber)) {
            throw new IllegalArgumentException("Dose number cannot be blank.");
        }
        if (!doseNumber.matches("[0-9]+")) {
            throw new InputMismatchException("Insert a valid dose number.");
        }
    }

    /**
     * Check vaccine dosage.
     *
     * @param vaccineDosage the vaccine dosage
     */
    public void checkVaccineDosage (String vaccineDosage) {
        if (StringUtils.isBlank(vaccineDosage)) {
            throw new IllegalArgumentException("Vaccine dosage cannot be blank.");
        }
        /*
        if (validateVaccineDosage(vaccineDosage)) {
            throw new IllegalStateException("Insert a valid vaccine dosage");
        }
         */
        if (!vaccineDosage.matches("[0-9.]+")) {
            throw new IllegalArgumentException("Invalid vaccine dosage format");
        }
    }

    /**
     * Check time between doses.
     *
     * @param timeBetweenDoses the time between doses
     */
    public void checkTimeBetweenDoses (String timeBetweenDoses) {
        if (StringUtils.isBlank(timeBetweenDoses)) {
            throw new IllegalArgumentException("Time between doses cannot be blank.");
        }
        if (!timeBetweenDoses.matches("[0-9]+")) {
            throw new InputMismatchException("Insert a valid time frame (number of days).");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vaccine)) return false;
        Vaccine vaccine = (Vaccine) o;
        return Objects.equals(ID, vaccine.ID) && Objects.equals(name, vaccine.name) && Objects.equals(brand, vaccine.brand) && Objects.equals(vaccineType, vaccine.vaccineType) && Objects.equals(ageGroup, vaccine.ageGroup) && Objects.equals(vaccineDosage, vaccine.vaccineDosage) && Objects.equals(timeBetweenDoses, vaccine.timeBetweenDoses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
    }

}
