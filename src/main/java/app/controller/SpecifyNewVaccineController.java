package app.controller;

import app.domain.model.Company;
import app.domain.model.Vaccine;
import app.domain.store.VaccineStore;

public class SpecifyNewVaccineController {
    private final Company company;
    private Vaccine vaccine;
    private VaccineStore storeVaccine;

    public SpecifyNewVaccineController() { this(App.getInstance().getCompany()); }

    public SpecifyNewVaccineController(Company company) {
        this.company = company;
        this.vaccine = null;
    }

    /*
    public void SpecifyNewVaccine(int ID, String name, String brand, String vaccineType, String ageGroup, int doseNumber, String vaccineDosage, int timeBetweenDoses) {
        this.store = this.company.getVaccineStore();
        //this.vaccine = this.store.SpecifyNewVaccine(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
    }
     */
    public void SpecifyVaccine (String ID, String name, String brand, String vaccineType, String ageGroup, String doseNumber, String vaccineDosage, String timeBetweenDoses) {
        this.storeVaccine = this.company.getVaccineStore();
        this.vaccine = this.storeVaccine.SpecifyNewVaccine(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
    }

    /**
     * Validates the data entered by the administrator according to acceptance criteria
     * @param ID               - Data entered by Administrator.
     * @param name             - Data entered by Administrator.
     * @param brand            - Data entered by Administrator.
     * @param vaccineType      - Data entered by Administrator.
     * @param ageGroup         - Data entered by Administrator.
     * @param doseNumber       - Data entered by Administrator.
     * @param vaccineDosage    - Data entered by Administrator.
     * @param timeBetweenDoses - Data entered by Administrator.
     */
    public void validateVaccine(String ID, String name, String brand, String vaccineType, String ageGroup, String doseNumber, String vaccineDosage, String timeBetweenDoses){
        this.storeVaccine.validateNewVaccine(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
    }

    public String VaccineToString() {
        return this.vaccine.toString();
    }
    public void saveNewVaccine() {
        this.storeVaccine.addVaccine(this.vaccine);
    }

    /*
    public boolean saveVaccine() throws IOException {
        if(!storeVaccine.sameVaccine(vaccine)){
            //crea(employee.getName(), employee.getEmailAddress(), employee.getRole());
            return this.storeVaccine.saveNewVaccine(vaccine);
        }
        return false;
    }
     */

    /*
    public boolean createVaccineAuth(int ID, String name, String brand, String vaccineType, String ageGroup, int doseNumber, String vaccineDosage, int timeBetweenDoses) {
        AuthFacade af = company.getAuthFacade();


        return af.a(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
    }

     */

/*
    public boolean SpecifyNewVaccine (int ID, String name, String brand, String vaccineType, String ageGroup, int doseNumber, String vaccineDosage, int timeBetweenDoses) {
        this.vaccine = this.store.SpecifyNewVaccine(ID, name, brand, vaccineType, ageGroup, doseNumber, vaccineDosage, timeBetweenDoses);
        System.out.println(vaccine.toString());
        return this.store.validateNewVaccine(this.vaccine);
    }

 */

    public String VaccineAuthToString() {
        return("\nVaccine " + vaccine.getID() + "registered with the attribute: \nName:" + vaccine.getName() + "\nBrand: " + vaccine.getBrand() + "\nVaccine Type: " + vaccine.getVaccineType() + "\nAge group: " + vaccine.getAgeGroup() + "\nDose number: " + vaccine.getDoseNumber() + "\nVaccine dosage: " + vaccine.getVaccineDosage() + "\nTime between doses: " + vaccine.getTimeBetweenDoses());
    }

    /**
     * Return the vaccine ID
     * @return - ID in int
     */
    public String getID() { return vaccine.getID(); }

    /**
     * Return the vaccine name
     * @return - Name in a form of string
     */
    public String getName() { return vaccine.getName(); }

    /**
     * Return the vaccine brand
     * @return - Brand in a form of string
     */
    public String getBrand() { return vaccine.getBrand(); }

    /**
     * Return the vaccine type
     * @return - Vaccine type in a form of string
     */
    public String getVaccineType() { return vaccine.getVaccineType(); }

    /**
     * Return the vaccine age group
     * @return - Age group in a form of string
     */
    public String getAgeGroup() { return vaccine.getAgeGroup(); }

    /**
     * Return the vaccine dose number
     * @return - Dose number in int
     */
    public String getDoseNumber() { return  vaccine.getDoseNumber(); }

    /**
     * Return the vaccine dosage
     * @return - Vaccine dosage in a form os string
     */
    public String getVaccineDosage() { return vaccine.getVaccineDosage(); }

    /**
     * Return the vaccine time between doses
     * @return - Time between doses in int
     */
    public String getTimeBetweenDoses() { return vaccine.getTimeBetweenDoses(); }

}