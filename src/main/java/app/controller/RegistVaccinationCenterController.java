package app.controller;

import app.domain.model.Company;
import app.domain.model.VaccinationCenter;
import app.domain.store.VaccinationCenterStore;
import app.domain.store.WaitingRoomStore;


public class RegistVaccinationCenterController {
    private final Company company;
    private VaccinationCenter vac;
    private VaccinationCenterStore stores;


    private WaitingRoomStore waitingRoomStore;
    private String password;

    public RegistVaccinationCenterController() {
        this(App.getInstance().getCompany());
    }

    public RegistVaccinationCenterController(Company company) {
        this.company = company;
    }

    public void registerVaccinationCenter(String name, String address, String phoneNumber, String area, String mailAddress, String faxNumber, String webAddress, String openingHour, String closingHour, String slotDuration, String maxVacSlot) {
        this.stores = this.company.getVaccinationCenterStore();
        this.vac = this.stores.registerNewVaccinationCenter(name, address, phoneNumber, area, mailAddress, faxNumber, webAddress, openingHour, closingHour, slotDuration, maxVacSlot);
    }

    public void validateVaccinationCenter(String name, String address, String phoneNumber, String area, String mailAddress, String faxNumber, String webAddress, String openingHour, String closingHour, String slotDuration, String maxVacSlot){
        this.stores.validateNewVaccinationCenter(name, address, phoneNumber, area, mailAddress, faxNumber, webAddress, openingHour, closingHour, slotDuration, maxVacSlot);

    }

    public void saveNewVaccinationCenter (){
        this.stores.addVaccinationCenter(this.vac);
        this.company.getWaitingRoomStore().addWaitingRoom();
        this.company.getRecoveryRoomStore().addRecoveryRoom();
        this.company.getRecordVaccineStore().addVaccinationCenter();
    }


   /* public boolean saveVaccinationCenter() throws IOException {
        if (stores.sameVaccinationCenter(vac)) {
            RegistVaccinationCenterAuth(vac.getName(), vac.getPhoneNumber(), vac.getAddress());
            return this.stores.saveVaccinationCenter(vac);
        }
        return false;
    }*/

  /*  public boolean RegistVaccinationCenterAuth(String name, String phoneNumber, String address) throws IOException {
        AuthFacade af = company.getAuthFacade();

        this.password = af.generatePassword();

        af.FileWriter(name, phoneNumber, this.password);

        return af.addUserWithRole(name, phoneNumber, password, address);
    }*/

   /* public void setStores(VaccinationCenterStore stores) {
        this.stores = stores;
    }*/
}
