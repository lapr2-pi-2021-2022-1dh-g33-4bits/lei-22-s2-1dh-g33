package app.controller;

import app.domain.model.Company;
import app.domain.model.VaccineSchedule;
import app.domain.store.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Objects;

public class VaccineScheduleController {
    private final Company company;
    private VaccineSchedule vacSched;
    private VaccineScheduleStore vaccineSchedulestore;
    private VaccinationHistoryStore vaccinationHistoryStore;
    private RecordVaccineStore recStore;
    private VaccineStore vaccineStore;
    private VaccinationCenterStore vaccinationCenterStore;


    public VaccineScheduleController(){
        this(App.getInstance().getCompany());
    }

    public Company getCompany() {
        return this.company;
    }


    public VaccineScheduleController(Company company) {
        this.company = company;
        this.vaccineSchedulestore = this.company.getVaccineScheduleStore();
        this.vaccineStore = this.company.getVaccineStore();
        this.recStore = this.company.getRecordVaccineStore();
        this.vaccinationHistoryStore = this.company.getVaccinationHistoryStore();
        this.vaccinationCenterStore = this.company.getVaccinationCenterStore();
    }

    public void vaccineSchedule(String snsUserNumber,String vaccinationCenterName,String date,String time,String vacType){
        this.vaccineSchedulestore = this.company.getVaccineScheduleStore();
        this.vacSched = this.vaccineSchedulestore.scheduleVaccine(snsUserNumber,vaccinationCenterName,date,time,vacType);
    }

    public void verifyDateAndTimeBetweenDoses (String date, String snsUserNumber) throws ParseException {
        for (int vc = 0; vc < vaccinationCenterStore.getVaccinationCenterList().size(); vc++) {
            for (int i = 0; i < this.recStore.getRecordVaccineList(vc).size(); i++) {
                if (this.recStore.getRecordVaccineList(vc).get(i).getSnsUserNumber().equals(snsUserNumber)) {
                    for (int j = 0; j < this.vaccineStore.getVaccineList().size(); j++) {
                        if (Objects.equals(this.recStore.getRecordVaccineList(vc).get(i).getVacName(), this.vaccineStore.getVaccineList().get(j).getName())) {
                            String time = this.vaccineStore.getVaccineList().get(j).getTimeBetweenDoses();
                            if (!validateTimeDose(time, date, snsUserNumber, vc)) {
                                throw new InputMismatchException("Insert a date following the example: i.e., 18/12/2022");
                            }
                        }
                    }
                }
            }
        }
    }

    public boolean validateTimeDose(String time, String date,String snsUserNumber, int vc) throws ParseException {
        for (int i = 0; i < this.recStore.getRecordVaccineList(vc).size(); i++){
            if (this.recStore.getRecordVaccineList(vc).get(i).getSnsUserNumber().equals(snsUserNumber)){
                for (int j = 0; j < this.vaccinationHistoryStore.getVaccineHistoryList().size(); j++){
                    if (this.vaccinationHistoryStore.getVaccineHistoryList().get(j).getSnsUserNumber().equals(snsUserNumber)){
                        String datePrim = this.vaccinationHistoryStore.getVaccineHistoryList().get(j).getDate();
                        Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(datePrim);
                        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                        LocalDate lDate = LocalDate.parse(date,dtf);
                        LocalDate local = lDate.minusDays(Long.parseLong(time));
                        String strDate = local.format(dtf);
                        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
                        if (date1.after(date2)){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }


    public void validateNewVaccineSchedule(String snsUserNumber,String vaccinationCenterName,String date,String time,String vacType){
        this.vaccineSchedulestore.validateScheduleVaccine(snsUserNumber,vaccinationCenterName,date,time,vacType);
    }


    public void saveVaccineSchedule (){
        this.vaccineSchedulestore.addVaccineSchedule(this.vacSched);
    }

    public void removeVaccineSchedule (VaccineSchedule vacSched){
        this.vaccineSchedulestore.removeVaccineSchedule(vacSched);
    }

    public void saveVaccineHistory (){
        this.vaccinationHistoryStore.addVaccineHistory(this.vacSched);
    }

    public String showData (String snsUserNumber, String vaccinationCenterName, String date, String time, String vacType){

        return ("\n-------------------------")+
                ("\n| SNS User Number: " + snsUserNumber)+
                ("\n| Vaccination Center Name: " + vaccinationCenterName)+
                ("\n| Date: " + date)+
                ("\n| Time: " + time)+
                ("\n| Vaccination Type: " + vacType)+
                ("\n-------------------------\n");

    }

}

