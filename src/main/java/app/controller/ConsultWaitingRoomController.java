package app.controller;

import app.domain.model.Company;
import app.domain.store.WaitingRoomStore;
import app.mappers.SNSUserMapper;
import app.mappers.dto.SNSUserDTO;

import java.util.List;

public class ConsultWaitingRoomController {
    private final SNSUserMapper snsUserMapper;
    private Company company;
    WaitingRoomStore waitingRoomStore;
    private List<SNSUserDTO> waitingRoom;

    public ConsultWaitingRoomController () {this(App.getInstance().getCompany());}

    public ConsultWaitingRoomController (Company company) {
        this.company = company;
        this.snsUserMapper = new SNSUserMapper();
        this.waitingRoomStore = new WaitingRoomStore();
    }
    
    /**
     * Gets the waiting rooms stored in the WaitingRoomStore
     *
     * @return - A list of existing waiting rooms in WaitingRoomStore
     */
    public List<SNSUserDTO> getWaitingRoom (int vc) {
        this.waitingRoomStore = this.company.getWaitingRoomStore();
        this.waitingRoom = this.waitingRoomStore.getWaitingRoomList(vc);
        return waitingRoom;
    }

}
