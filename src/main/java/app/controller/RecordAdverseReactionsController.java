package app.controller;

import app.domain.model.AdverseReaction;
import app.domain.model.Company;
import app.domain.store.AdverseReactionsStore;
import app.domain.store.SNSUserDataStore;
import app.mappers.dto.SNSUserDTO;

/**
 * The type Record adverse reaction+s controller.
 */
public class RecordAdverseReactionsController {
    private final Company company;
    private SNSUserDTO snsUser;
    private AdverseReaction adverseReaction;
    private SNSUserDataStore snsUserDataStore = new SNSUserDataStore();
    private AdverseReactionsStore adverseReactionsStore = new AdverseReactionsStore();

    /**
     * Instantiates a new Record adverse reaction's controller.
     */
    public RecordAdverseReactionsController() {
        this(App.getInstance().getCompany());
    }

    /**
     * Instantiates a new Record adverse reaction's controller.
     *
     * @param company the company
     */
    public RecordAdverseReactionsController (Company company) {
        this.company = company;
    }

    /**
     * Check if sns user exists sns user.
     *
     * @param snsUserNumber     the sns user number
     * @return the sns user
     */
    public SNSUserDTO checkIfSNSUserExists (String snsUserNumber) { //if the SNS User exists the method will return that SNSUser, or else the method returns null.
        this.snsUserDataStore = this.company.getSNSUserDataStore();
        return this.snsUserDataStore.checkIfSNSUserExists(snsUserNumber);
    }

    /**
     * New adverse reaction.
     *
     * @param snsUserNumber       the sns user number
     * @param adverseReactionText the adverse reaction text
     */
    public void newAdverseReaction (String snsUserNumber, String adverseReactionText) {
        this.adverseReactionsStore = this.company.getAdverseReactionsStore();
        this.adverseReaction = this.adverseReactionsStore.newAdverseReaction(snsUserNumber, adverseReactionText);
    }

    /**
     * Saves adverse reaction.
     */
    public void saveAdverseReaction () {
        this.adverseReactionsStore.addAdverseReaction(this.adverseReaction);
    }

}
