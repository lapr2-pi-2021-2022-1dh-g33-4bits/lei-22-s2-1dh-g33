package app.controller;

import app.domain.model.Company;
import app.domain.model.RecordVaccine;
import app.domain.store.*;
import app.mappers.dto.SNSUserDTO;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

/**
 * The type Record vaccination controller.
 */
public class RecordVaccinationController {

    private final Company company;
    private RecordVaccine recVac;
    private RecordVaccineStore store;
    private final String filePath = "config.properties";
    private final String recoveryPeriodTime = "recoveryPeriod";
    private AdverseReactionsStore adStore = new AdverseReactionsStore();
    private VaccineScheduleStore vaccineScheduleStore = new VaccineScheduleStore();
    private WaitingRoomStore waitingRoomStore = new WaitingRoomStore();
    private RecoveryRoomStore recoveryRoomStore = new RecoveryRoomStore();
    private VaccinationHistoryStore vaccinationHistoryStore;
    private VaccineStore vStore = new VaccineStore();

    /**
     * Instantiates a new Record vaccination controller.
     */
    public RecordVaccinationController(){
        this(App.getInstance().getCompany());
    }

    /**
     * Instantiates a new Record vaccination controller.
     *
     * @param company the company
     */
    public RecordVaccinationController(Company company){
        this.company = company;
        this.vaccineScheduleStore = this.company.getVaccineScheduleStore();
        this.store = this.company.getRecordVaccineStore();
        this.recoveryRoomStore = this.company.getRecoveryRoomStore();
        this.waitingRoomStore = this.company.getWaitingRoomStore();
        this.vStore = this.company.getVaccineStore();
        this.adStore = this.company.getAdverseReactionsStore();
        this.vaccinationHistoryStore = this.company.getVaccinationHistoryStore();
    }

    /**
     * Record vaccine.
     *
     * @param vacType       the vac type
     * @param vacName       the vac name
     * @param lotNumber     the lot number
     * @param snsUserNumber the sns user number
     */
    public void recordVaccine(String vacType, String vacName, String lotNumber, String snsUserNumber){
        this.store = this.company.getRecordVaccineStore();
        this.recVac = this.store.recordVaccine(vaccineScheduleStore.getVaccineScheduleList().get(vaccineScheduleStore.findIndex(vacType,snsUserNumber)).getDate(), vacType, vacName, lotNumber,snsUserNumber);
    }

    /**
     * Save record vaccine.
     *
     * @param vc the vc
     */
    public void saveRecordVaccine(int vc){
        this.store.addRecordVaccine(this.recVac, vc);
    }

    /**
     * Turn birth date to age int.
     *
     * @param birthdate the birthdate
     * @return the int
     */
    public int  turnBirthDateToAge(String birthdate){
         return this.store.turnBirthDateToAge(birthdate);
    }

    /**
     * Check adverse reaction string.
     *
     * @param snsUserNumber the sns user number
     * @return the string
     */
    public String checkAdverseReaction (String snsUserNumber){
        for (int i = 0; i<this.adStore.getAdverseReactionsList().size(); i++){
            if (this.adStore.getAdverseReactionsList().get(i).getSnsUserNumber().equals(snsUserNumber)){
                return this.adStore.getAdverseReactionsList().get(i).getAdverseReaction();
            }
        }
        return null;
    }

    /**
     * Check age group boolean.
     *
     * @param ageGroup the age group
     * @param years    the years
     * @return the boolean
     */
    public boolean checkAgeGroup (String ageGroup,int years){
        String[] ageGroupArray = ageGroup.split("-");
        if (Integer.parseInt(ageGroupArray[0])<= years && Integer.parseInt(ageGroupArray[1]) >= years){
            return true;
        }
        return false;
    }

    /**
     * Get respective dose int.
     *
     * @param snsUserNumber the sns user number
     * @param j             the j
     * @return the int
     */
    public int getRespectiveDose (String snsUserNumber,int j){
        int cont = 0;

        for (int i = 0; i<this.vaccinationHistoryStore.getVaccineHistoryList().size(); i++){
            if (this.vaccinationHistoryStore.getVaccineHistoryList().get(i).getSnsUserNumber().equals(snsUserNumber)){
                cont++;
            }
        }
        if (cont<=Integer.parseInt(this.vStore.getVaccineList().get(j).getDoseNumber())){
           return cont;
        }else{
            return 0;
        }
    }

    /**
     * Gets recovery period.
     *
     * @return the recovery period
     * @throws IOException the io exception
     */
    public String getRecoveryPeriod() throws IOException {
        Properties properties = new Properties();
        InputStream in = new FileInputStream(filePath);
        properties.load(in);
        String recoveryPeriod = properties.getProperty(recoveryPeriodTime);
        in.close();
        return recoveryPeriod;
    }

    /**
     * Add sns user to recovery room.
     *
     * @param vacType    the vac type
     * @param vacName    the vac name
     * @param lotNumber  the lot number
     * @param snsUserDTO the sns user dto
     * @param vc         the vc
     * @throws IOException               the io exception
     * @throws ClassNotFoundException    the class not found exception
     * @throws InvocationTargetException the invocation target exception
     * @throws NoSuchMethodException     the no such method exception
     * @throws InstantiationException    the instantiation exception
     * @throws IllegalAccessException    the illegal access exception
     */
    public void addSNSUserToRecoveryRoom (String vacType, String vacName, String lotNumber, SNSUserDTO snsUserDTO, int vc) throws IOException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        this.recoveryRoomStore.addSNSUserToRecoveryRoom(snsUserDTO, vc);
    }

    /**
     * Remove sns user from waiting room.
     *
     * @param snsUserDTO the sns user dto
     * @param vc         the vc
     */
    public void removeSNSUserFromWaitingRoom (SNSUserDTO snsUserDTO,int vc){
        this.waitingRoomStore.removeSNSUserFromWaitingRoom(snsUserDTO,vc);
    }

}



