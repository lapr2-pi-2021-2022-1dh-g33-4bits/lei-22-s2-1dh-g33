package app.controller;

import app.domain.model.Company;
import app.domain.store.SNSUserDataStore;
import app.domain.store.VaccineScheduleStore;
import app.domain.store.WaitingRoomStore;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;


/**
 * The type Regist arrival controller.
 */
public class RegistArrivalController {
    private Company company;
    private String snsUserNumber;
    private String date;
    private String time;

    /**
     * The Store.
     */
    SNSUserDataStore store;

    /**
     * The Waiting room store.
     */
    WaitingRoomStore waitingRoomStore;
    /**
     * The Sns user data store.
     */
    SNSUserDataStore snsUserDataStore;

    /**
     * The Vaccine schedule store.
     */
    VaccineScheduleStore vaccineScheduleStore;


    /**
     * Instantiates a new Regist arrival controller.
     */
    public RegistArrivalController() {this(App.getInstance().getCompany());}

    /**
     * Instantiates a new Regist arrival controller.
     *
     * @param company the company
     */
    public RegistArrivalController(Company company) {this.company = company;
        this.vaccineScheduleStore = this.company.getVaccineScheduleStore();
        this.waitingRoomStore = this.company.getWaitingRoomStore();
        this.store = this.company.getSNSUserDataStore();
        this.snsUserDataStore = this.company.getSNSUserDataStore();
    }


    /*
    public boolean saveRegister(String snsUserNumber, int vc) throws IOException {
        return  this.waitingRoomStore.saveRegister(this.store.getBySNSUserCardNumber(snsUserNumber), vc);

    }*/

/*
    public boolean saveRegister(String snsUserNumber, String date, String time) throws IOException {
        return this.waitingRoomStore.saveRegister(this.store.getBySNSUserCardNumber(snsUserNumber), date, time);
    }
*/


    /**
     * Search for sns user.
     *
     * @param vc            the vc
     * @param date          the date
     * @param time          the time
     * @param snsUserNumber the sns user number
     */
    public void searchForSnsUser(int vc, String date, String time, String snsUserNumber) {
        for (int i = 0; i<this.vaccineScheduleStore.getVaccineScheduleList().size(); i++){
            if (this.vaccineScheduleStore.getVaccineScheduleList().get(i).getSnsUserNumber().equals(snsUserNumber)){
                waitingRoomStore.addSNSUserToWaitingRoom(this.store.getBySNSUserCardNumber(snsUserNumber), vc);
                System.out.println("Registered arrival with Sns User Number: " + this.vaccineScheduleStore.getVaccineScheduleList().get(i).getSnsUserNumber());
            }
        }
    }


    /**
     * Validate sns user number.
     *
     * @param snsUserNumber the sns user number
     */
    public void validateSNSUserNumber(String snsUserNumber){
        if (StringUtils.isBlank(snsUserNumber)) {
            throw new IllegalArgumentException("SNS User Number cannot be blank.");
        }
        if (!vaccineScheduleStore.validateSNSUserNumber(snsUserNumber)){
            throw new IllegalStateException("SNS User didn't schedule a vaccine");
        }
    }

    /**
     * Check time.
     *
     * @param time the time
     */
    public void checkTime(String time) {
        if (StringUtils.isBlank(time)) {
            throw new IllegalArgumentException("Time cannot be blank.");
        }
        if (!validateTime(time)) {
            throw new IllegalStateException("Insert a time following the example: i.e., 18:03");
        }
    }

    /**
     * Check date.
     *
     * @param date the date
     */
    public void checkDate(String date) {
        if (StringUtils.isBlank(date)) {
            throw new IllegalArgumentException("Date cannot be blank.");
        }
        if (!validateDate(date)) {
            throw new IllegalStateException("Insert a date following the example: i.e., 18/12/2022");
        }
    }

    /**
     * Validate date boolean.
     *
     * @param date the date
     * @return true if date is validated, false if not
     */
    public boolean validateDate(String date) {

        LocalDateTime ldt;
        String[] separated = date.split("/");
        if (separated.length==3) {
            if (separated[0].length()==1 && separated[1].length()==2) {
                date = "0" + separated[0] + "/" + separated[1]+"/"+separated[2];
            }
            if (separated[0].length()==2 && separated[1].length()==1) {
                date = separated[0] + "/0" + separated[1]+"/"+separated[2];
            }
            if (separated[0].length()==1 && separated[1].length()==1) {
                date = "0" + separated[0] + "/0" + separated[1]+"/"+separated[2];
            }
        }
        DateTimeFormatter fomatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        try {
            ldt = LocalDateTime.parse(date, fomatter);
            String result = ldt.format(fomatter);
            return result.equals(date);
        } catch (DateTimeParseException e) {
            try {
                LocalDate ld = LocalDate.parse(date, fomatter);
                String result = ld.format(fomatter);
                return result.equals(date);
            } catch (DateTimeParseException exp) {
                try {
                    LocalTime lt = LocalTime.parse(date, fomatter);
                    String result = lt.format(fomatter);
                    return result.equals(date);
                } catch (DateTimeParseException e2) {
                    //Debugging purposes
                    //e2.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * Save register boolean.
     *
     * @param snsUserNumber the sns user number
     * @param vc            the vc
     * @return true if save register, false if not
     */
    public boolean saveRegister(String snsUserNumber, int vc) {
        if (!waitingRoomStore.sameRegister(snsUserNumber, vc)) {
            return true;
        }
        return false;
    }

    /**
     * Check if sns user exists boolean.
     *
     * @param snsUserNumber the sns user number
     * @return true if users exists, false if not
     */
    public boolean checkIfSNSUserExists (String snsUserNumber) {
        if (!snsUserDataStore.getBySNSUserCardNumberExists(snsUserNumber)) {
            return true;
        }
        return false;
    }

    /**
     * Validate time boolean.
     *
     * @param time the time
     * @return true if time is valid, false if not
     */
    public static boolean validateTime(String time) {
        try {
            String[] timeArray = time.split(":");
            return  Integer.parseInt(timeArray[0]) < 24 && Integer.parseInt(timeArray[1]) < 60;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Gets sns user number.
     *
     * @return the sns user number
     */
    public String getSnsUserNumber() {
        return snsUserNumber;
    }

    /**
     * Sets sns user number.
     *
     * @param snsUserNumber the sns user number
     */
    public void setSnsUserNumber(String snsUserNumber) {
        this.snsUserNumber = snsUserNumber;
    }

    /**
     * Gets time.
     *
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * Sets time.
     *
     * @param time the time
     */
    public void setTime(String time) {
        this.time = time;
    }
}
