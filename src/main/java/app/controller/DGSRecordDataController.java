package app.controller;

import app.domain.model.Company;
import app.domain.model.VaccinationCenter;
import app.domain.model.VaccineSchedule;
import app.domain.store.RecoveryRoomStore;
import app.domain.store.VaccinationCenterStore;
import app.domain.store.VaccineScheduleStore;
import app.mappers.SNSUserMapper;
import app.mappers.dto.SNSUserDTO;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Properties;


public class DGSRecordDataController {
    private final String time = "time";
    private final String filePath ="config.properties";
    private final SNSUserMapper snsUserMapper;

    private Company company;

    VaccineScheduleStore vaccineScheduleStore;

    VaccinationCenterStore vaccinationCenterStore;

    RecoveryRoomStore recoveryRoomStore;
    private List<VaccinationCenter> vs;
    private List<VaccineSchedule> vaccineSchedules;

    private List<SNSUserDTO> recoveryRoom;


    public DGSRecordDataController() {this(App.getInstance().getCompany());}

    public DGSRecordDataController(Company company) {
        this.company = company;
        this.snsUserMapper = new SNSUserMapper();
        this.vaccineScheduleStore = this.company.getVaccineScheduleStore();
        this.vaccinationCenterStore = this.company.getVaccinationCenterStore();
        this.recoveryRoomStore = this.company.getRecoveryRoomStore();
    }

    public int vaccinationCenterNumber(){
        return this.vaccinationCenterStore.getVaccinationCenterList().size();
    }
    /**
     * Gets the Vaccination Center stored in the VaccinationCenterStore
     *
     * @return - A list of existing waiting rooms in VaccinationCenterStore
     */
    public List<VaccinationCenter> getVaccinationCenterStore (){
        this.vaccinationCenterStore= this.company.getVaccinationCenterStore();
        this.vs = this.vaccinationCenterStore.getVaccinationCenterList();
        return vs;
    }

    /**
     * Gets the Vaccination Schedule stored in the VaccinationScheduleStore
     *
     * @return - A list of existing waiting rooms in VaccinationScheduleStore
     */
    public List<VaccineSchedule> getVaccinationScheduleStore() {
        this.vaccineScheduleStore = this.company.getVaccineScheduleStore();
        this.vaccineSchedules = this.vaccineScheduleStore.getVaccineScheduleList();
        return vaccineSchedules;
    }

    public List<SNSUserDTO> getRecoveryRoomStore(int vaccinationCenter) {
        this.recoveryRoomStore = this.company.getRecoveryRoomStore();
        this.recoveryRoom = this.recoveryRoomStore.getRecoveryRoomList(vaccinationCenter);
        return recoveryRoom;
    }

    /**
     * Gets the Date of administration in a vaccinationCenter
     * @param vaccinationCenter
     * @return Date
     */
    public String getDate(int vaccinationCenter){
        return getVaccinationScheduleStore().get(vaccinationCenter).getDate();
    }


    /**
     * Gets the name of the vaccination center
     * @param vaccinationCenter
     * @return Name of vaccination center
     */
    public String getName(int vaccinationCenter){
        return getVaccinationCenterStore().get(vaccinationCenter).getName();
    }

    /**
     * Gets the total number of users vaccinated in the vaccination center
     * @param vaccinationCenter
     * @return Total Number of Users vaccinated
     */

    public String getTotalNumberRecoveryRoom(int vaccinationCenter){
        return Integer.toString(this.company.getRecoveryRoomStore().getRecoveryRoomList(vaccinationCenter).size());
    }

    public String getHours() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Properties properties = new Properties();
        InputStream in = new FileInputStream(filePath);
        properties.load(in);
        String timer = properties.getProperty(time);

        in.close();
        return timer;
    }

}
