package app.controller;

import app.domain.model.Company;
import app.domain.model.Employee;
import app.domain.store.EmployeeStore;
import app.domain.store.OrganizationRolesStore;
import auth.AuthFacade;
import auth.domain.model.UserRole;

import java.io.IOException;
import java.util.List;

public class CreateEmployeeController {
    private Employee employee;
    private EmployeeStore storeEmployee = new EmployeeStore();
    private Company company;
    private OrganizationRolesStore rolesStore;
    private List<UserRole> organizationRoles;
    private String password;

    public Company getCompany() {
        return company;
    }

    public CreateEmployeeController() {this(App.getInstance().getCompany());}

    public CreateEmployeeController(Company company) { this.company = company;}

    /**
     * Add list of roles existent in OrganizationRolesStore to the system for usage
     */
    public void addOrganizationRoles(){
        this.rolesStore = this.company.getOrganizationRolesStore();
        this.organizationRoles = this.rolesStore.getOrganizationRolesList();
        rolesStore.addRoles();
    }

    /**
     * Gets the list organizationRoles stored in the OrganizationRolesStore
     *
     * @return - A list with existent roles
     */
    public List<UserRole> getOrganizationRoles(){
        this.rolesStore = this.company.getOrganizationRolesStore();
        this.organizationRoles = this.rolesStore.getOrganizationRolesList();
        return organizationRoles;
    }

    /**
     * Gets the id from the organizationRoles list stored in the OrganizaitonRolesStore
     *
     * @param position - Position of the list from which we want the role id
     * @return - Role id in form of a string
     */
    public String getRoles(int position){return organizationRoles.get(position).getId();}

    /**
     * Redirects the data entered by the administrator to create a employee (with exception of the Administrator) to EmployeeStore
     * @param name          - Data entered by Administrator.
     * @param address       - Data entered by Administrator.
     * @param emailAddress  - Data entered by Administrator.
     * @param phoneNumber   - Data entered by Administrator.
     * @param socCode       - Data entered by Administrator.
     * @param id            - Data entered by Administrator.
     * @param NIF           - Data entered by Administrator.
     * @param role          - Data entered by Administrator.
     */
    public boolean CreateEmployee(String role, String name, String address, String emailAddress, String phoneNumber, String socCode, String id, String NIF){
        this.storeEmployee = this.company.getEmployeeStore();
        this.employee = this.storeEmployee.createEmployee(role,name,address,emailAddress,phoneNumber,socCode,id,NIF);
        return this.storeEmployee.validateEmployee(employee);
    }

    public boolean saveEmployee() throws IOException {
        if(storeEmployee.sameEmployee(employee)){
            createEmployeeAuth(employee.getName(), employee.getEmailAddress(), employee.getRole());
            return this.storeEmployee.saveEmployee(employee);
        }
        return false;
    }

    public boolean createEmployeeAuth(String name, String emailAddress, String role) throws IOException {
        AuthFacade af = company.getAuthFacade();
        this.password = af.generatePassword();
        af.FileWriter(name,emailAddress,this.password);
        return af.addUserWithRole(name, emailAddress, password, role);
    }

    public void notSaveEmployee(){
        storeEmployee.notSaveEmployee();
    }

    public String EmployeeAuthToString() {
        return("\nEmployee " + employee.getId() + "registered with the attribute: \nRole:" + employee.getRole() + "\nEmail: " + employee.getEmailAddress());
    }

 /*   public void registerEmployee(String name, String emailAddress, String password,String role){
        this.storeEmployee.registerEmployee(name, emailAddress, password, role);
    } */

    /**
     * Return the employee name
     * @return - Employee name in a form of String
     */
    public String getEmployeeName(){ return employee.getName();}

    /**
     * Return the employee address
     * @return - Employee address in a form of String
     */
    public String getEmployeeAddress(){ return employee.getAddress();}

    /**
     * Return the employee email address
     * @return - Employee email address in a form of String
     */
    public String getEmployeeEmailAddress(){ return employee.getEmailAddress();}

    /**
     * Return the employee phone number
     * @return - Employee phone number in a form of String
     */
    public String getPhoneNumber(){ return employee.getPhoneNumber();}

    /**
     * Return the employee standard occupational classification code
     * @return - Standard occupational classification in a form of a String
     */
    public String getSocCode() {
        return employee.getSocCode();
    }

    /**
     * Return the employee ID
     * @return - Employee ID in a form of a String
     */
    public String getId() {
        return employee.getId();
    }

    /**
     * Return the employee tax identification
     * @return - Tax identification in a form of a String
     */
    public String getNIF() {
        return employee.getNIF();
    }

    /**
     * Return the employee role
     * @return - Role in a form of a String
     */
    public String getRole() {
        return employee.getRole();
    }

}
