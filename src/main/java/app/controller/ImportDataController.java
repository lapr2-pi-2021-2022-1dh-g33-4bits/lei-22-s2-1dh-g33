package app.controller;

import app.domain.model.Company;
import app.domain.model.VaccinationData;
import app.domain.model.sortingAlgorithm.SortAlgorithm;
import app.domain.store.VaccinationDataStore;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
public class ImportDataController {
    private final String sortClass = "sort";
    private final String filePath ="config.properties";
    private final Company company;
    private final VaccinationDataStore vaccinationDataStore;
    private VaccinationData vaccinationData;
    private List <VaccinationData> importedData = new ArrayList<>();

    public  ImportDataController() { this(App.getInstance().getCompany()); }

    public ImportDataController(Company company) {
        this.company = company;
        this.vaccinationDataStore = this.company.getVaccinationDataStore();

    }

    public void LoadFile(String userNumber, String vacineType, String dose, String loteNumber,String scheduleDate, String scheduleTime, String arrivalDate, String arrivalTime,String administrationDate, String administrationTime, String leavingDate, String leavingTime) throws ParseException {
        this.vaccinationDataStore.add(userNumber,vacineType,dose,loteNumber,scheduleDate,scheduleTime, arrivalDate, arrivalTime,administrationDate, administrationTime,leavingDate,leavingTime);
        this.importedData.add(new VaccinationData(userNumber,vacineType,dose,loteNumber,scheduleDate,scheduleTime, arrivalDate, arrivalTime,administrationDate, administrationTime,leavingDate,leavingTime));
    }

    public boolean verifySnsUser (String snsUserNumber){
       if (company.getSNSUserDataStore().getBySNSUserCardNumberExists(snsUserNumber)){
           return true;
       }
       return false;
    }

    public String getUserName (String snsUserNumber){
        return company.getSNSUserDataStore().getNameBySnsUserCard(snsUserNumber);
    }


    public List <VaccinationData> sortArrivalDate() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Properties properties = new Properties();
        InputStream in = new FileInputStream(filePath);
        properties.load(in);
        Class <?> apiClass = Class.forName(properties.getProperty(sortClass));
        SortAlgorithm sortClass = (SortAlgorithm) apiClass.getDeclaredConstructor().newInstance();
        in.close();

        List<VaccinationData> list =  importedData;
        List<String> sortedString = sortClass.sortInitialDate(list);
        List<VaccinationData> sortedList = new ArrayList<>();

        for (String s : sortedString) {
            for (VaccinationData data : list) {
                if (s.equals(data.getArrivalDateAndTime())) {
                    sortedList.add(data);
                }
            }
        }
        return sortedList;

    }

    public List <VaccinationData> sortLeavingDate() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Properties properties = new Properties();
        InputStream in = new FileInputStream(filePath);
        properties.load(in);
        Class <?> apiClass = Class.forName(properties.getProperty(sortClass));
        SortAlgorithm sortClass = (SortAlgorithm) apiClass.getDeclaredConstructor().newInstance();
        in.close();

        List<VaccinationData> list =  importedData;
        List<String> sortedString = sortClass.sortLeftDate(list);
        List<VaccinationData> sortedList = new ArrayList<>();

        for (String s : sortedString) {
            for (VaccinationData data : list) {
                if (s.equalsIgnoreCase(data.getLeavindDateandTime())) {
                    sortedList.add(data);
                }
            }
        }
        return sortedList;

    }
}
