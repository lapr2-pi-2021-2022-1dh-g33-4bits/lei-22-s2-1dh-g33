package app.controller;

import app.domain.model.Company;
import app.domain.shared.Constants;
import app.domain.store.OrganizationRolesStore;
import app.domain.store.SNSUserDataStore;
import app.mappers.dto.SNSUserDTO;
import auth.AuthFacade;
import auth.domain.model.UserRole;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * The type Load csv file controller.
 */
public class LoadCSVFileController {
    private final Company company;
    private SNSUserDTO snsUser;
    private SNSUserDataStore SNSUserDataStore = new SNSUserDataStore();
    private String password;

    private OrganizationRolesStore rolesStore;
    private List<UserRole> organizationRoles;


    /**
     * Instantiates a new Load csv file controller.
     */
    public LoadCSVFileController() { this(App.getInstance().getCompany()); }

    /**
     * Instantiates a new Load csv file controller.
     *
     * @param company the company
     */
    public LoadCSVFileController (Company company) {
        this.company = company;
        this.snsUser = null;
    }

    /**
     * Add list of roles existent in OrganizationRolesStore to the system for usage
     */
    public void addOrganizationRoles(){
        this.rolesStore = this.company.getOrganizationRolesStore();
        this.organizationRoles = this.rolesStore.getOrganizationRolesList();
        rolesStore.addRoles();
    }

    /**
     * Gets the list organizationRoles stored in the OrganizationRolesStore
     *
     * @return - A list with existent roles
     */
    public List<UserRole> getOrganizationRoles(){
        this.rolesStore = this.company.getOrganizationRolesStore();
        this.organizationRoles = this.rolesStore.getOrganizationRolesList();
        return organizationRoles;
    }

    /**
     * Load csv file.
     *
     * @param name              the name
     * @param sex               the sex
     * @param birthDate         the birth date
     * @param address           the address
     * @param phoneNumber       the phone number
     * @param email             the email
     * @param snsUserNumber     the sns user number
     * @param citizenCardNumber the citizen card number
     */
    public void LoadCSVFile (String name, String sex, String birthDate, String address, String phoneNumber, String email, String snsUserNumber, String citizenCardNumber) {
        this.SNSUserDataStore = this.company.getSNSUserDataStore();
        this.snsUser = this.SNSUserDataStore.createSNSUser(name, sex, birthDate, address, phoneNumber, email, snsUserNumber, citizenCardNumber);
    }

    /**
     * Validate new sns user.
     *
     * @param snsUser the sns user
     */
    public void validateNewSNSUser (SNSUserDTO snsUser) {
        this.SNSUserDataStore.validateSNSUser(snsUser);
    }

    /**
     * Save sns user loading list.
     *
     * @param snsUserLoadingList the sns user loading list
     * @param confirm            the confirm
     * @throws IOException the io exception
     */
    public void saveSNSUserLoadingList (List <SNSUserDTO> snsUserLoadingList, String confirm) throws IOException {
        for (SNSUserDTO a: snsUserLoadingList) {
           /* if (Objects.equals(confirm.toUpperCase(Locale.ROOT), "Y")) {
                System.out.println(a);
            } */
            //createSNSUserAuth(a.getName(), a.getName().replaceAll(" ", "").toLowerCase(Locale.ROOT) + "@lei.sem2.pt", Constants.ROLE_SNSUSER);
            createSNSUserAuth(a.getName(), a.getEmail(), Constants.ROLE_SNSUSER);
            this.SNSUserDataStore.saveSNSUser(a);
        }
    }

    /**
     * Create sns user auth boolean.
     *
     * @param name         the name
     * @param emailAddress the email address
     * @param role         the role
     * @return the boolean
     * @throws IOException the io exception
     */
    public boolean createSNSUserAuth (String name, String emailAddress, String role) throws IOException {
        AuthFacade af = company.getAuthFacade();
        this.password = af.generatePassword();
        af.FileWriter(name,emailAddress,this.password);
        return af.addUserWithRole(name, emailAddress, password, role);
    }


    /**
     * Save new sns user.
     */
    public void saveNewSNSUser () {
        this.SNSUserDataStore.saveSNSUser(snsUser);
    }

}
