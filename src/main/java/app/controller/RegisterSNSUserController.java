package app.controller;

import app.domain.model.Company;
import app.domain.store.SNSUserDataStore;
import app.mappers.dto.SNSUserDTO;
import auth.AuthFacade;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Register sns user controller.
 */
public class RegisterSNSUserController {

    private final Company company;
    private SNSUserDTO snsUser;
    private SNSUserDataStore store;
    private String password;
    private List<SNSUserDTO> snsUserList = new ArrayList<>();

    /**
     * Instantiates a new Register sns user controller.
     */
    public RegisterSNSUserController(){
        this(App.getInstance().getCompany());
    }

    /**
     * Instantiates a new Register sns user controller.
     *
     * @param company the company
     */
    public RegisterSNSUserController(Company company){
        this.company = company;
        this.store = this.company.getSNSUserDataStore();
    }

    /**
     * Create sns user.
     *
     * @param name              the name
     * @param sex               the sex
     * @param birthDate         the birth date
     * @param address           the address
     * @param phoneNumber       the phone number
     * @param email             the email
     * @param snsUserNumber     the sns user number
     * @param citizenCardNumber the citizen card number
     */
    public void createSNSUser(String name, String sex, String birthDate, String address, String phoneNumber, String email, String snsUserNumber, String citizenCardNumber){
        this.store = this.company.getSNSUserDataStore();
        this.snsUser = this.store.createSNSUser(name, sex, birthDate, address, phoneNumber, email, snsUserNumber, citizenCardNumber);
    }

    /**
     * Validate new sns user.
     *
     * @param snsUser the sns user
     */
    public void validateNewSNSUser (SNSUserDTO snsUser){
        this.store.validateSNSUser(snsUser);
    }

    /**
     * Create employee auth boolean.
     *
     * @param name         the name
     * @param emailAddress the email address
     * @param role         the role
     * @return true if user is created, false if not
     * @throws IOException the io exception
     */
    public boolean createEmployeeAuth(String name, String emailAddress, String role) throws IOException {
        AuthFacade af = company.getAuthFacade();
        this.password = af.generatePassword();
        af.FileWriter(name,emailAddress,this.password);
        return af.addUserWithRole(name, emailAddress, password, role);
    }

    /**
     * Save sns user.
     *
     * @throws IOException the io exception
     */
    public void saveSNSUser() throws IOException {
        this.store.saveSNSUser(this.snsUser);
        createEmployeeAuth(snsUser.getName(), snsUser.getEmail(),"SNS User");
    }

    /**
     * Gets name with sns user number.
     *
     * @param snsUserNumber the sns user number
     * @return the name with sns user number
     */
    public String getNameWithSNSUserNumber (String snsUserNumber) {
        snsUserList = this.store.getSNSUserList();
        for (int i = 0; i < snsUserList.size(); i++) {
            if (snsUserNumber.equals(snsUserList.get(i).getSnsUserNumber())) {
                return snsUserList.get(i).getName();
            }
        }
        return null;
    }

}
