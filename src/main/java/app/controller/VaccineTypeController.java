package app.controller;

import app.domain.model.Company;
import app.domain.model.VaccineType;
import app.domain.store.VaccineTypesStore;

public class VaccineTypeController {
    private final Company company;
    private VaccineType vacType;
    private VaccineTypesStore store;

    public VaccineTypeController(){
        this(App.getInstance().getCompany());
    }

    public VaccineTypeController(Company company) {
        this.company = company;
        this.store = this.company.getVaccineTypesStore();
    }

    public void registVaccineType(String vaccineType){
        this.store = this.company.getVaccineTypesStore();
        this.vacType = this.store.registVaccineType(vaccineType);
    }

    public void validateNewVaccineType(String vaccineType){
        this.store.validateVaccineType(vaccineType);
    }

    public void saveVaccineType (){
        this.store.addVaccineType(this.vacType);
    }
}
