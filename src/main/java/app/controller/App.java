package app.controller;

import app.domain.model.*;
import app.domain.shared.Constants;
import auth.AuthFacade;
import auth.UserSession;
import app.mappers.dto.SNSUserDTO;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class App {

    private Company company;
    private AuthFacade authFacade;
    private Object SNSUser;

    private App()
    {
        Properties props = getProperties();
        this.company = new Company(props.getProperty(Constants.PARAMS_COMPANY_DESIGNATION));
        this.authFacade = this.company.getAuthFacade();
        bootstrap();
    }

    public Company getCompany()
    {
        return this.company;
    }

    public UserSession getCurrentUserSession()
    {
        return this.authFacade.getCurrentUserSession();
    }

    public boolean doLogin(String email, String pwd)
    {
        return this.authFacade.doLogin(email,pwd).isLoggedIn();
    }

    public void doLogout()
    {
        this.authFacade.doLogout();
    }

    private Properties getProperties()
    {
        Properties props = new Properties();

        // Add default properties and values
        props.setProperty(Constants.PARAMS_COMPANY_DESIGNATION, "DGS/SNS");

        // Read configured values
        try
        {
            InputStream in = new FileInputStream(Constants.PARAMS_FILENAME);
            props.load(in);
            in.close();
        }
        catch (IOException ex)
        {

        }
        return props;
    }

    private void bootstrap()
    {
        this.authFacade.addUserRole(Constants.ROLE_ADMIN,Constants.ROLE_ADMIN);
        this.authFacade.addUserRole(Constants.ROLE_NURSE,Constants.ROLE_NURSE);
        this.authFacade.addUserRole(Constants.ROLE_RECEPTIONIST,Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserRole(Constants.ROLE_CENTERCOORDINATOR,Constants.ROLE_CENTERCOORDINATOR);
        this.authFacade.addUserRole(Constants.ROLE_SNSUSER,Constants.ROLE_SNSUSER);

        this.authFacade.addUserWithRole("Main Administrator", "admin@lei.sem2.pt","123456", Constants.ROLE_ADMIN);
        this.authFacade.addUserWithRole("Nurse", "nurse@lei.sem2.pt","123456", Constants.ROLE_NURSE);
        this.authFacade.addUserWithRole("Receptionist", "recep@lei.sem2.pt","123456", Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserWithRole("Center Coordinator", "ccoord@lei.sem2.pt","123456", Constants.ROLE_CENTERCOORDINATOR);
        this.authFacade.addUserWithRole("SNS User", "snsuser@lei.sem2.pt","123456", Constants.ROLE_SNSUSER);

        VaccinationCenter vaccinationCenteraguas = new VaccinationCenter("Aguas","Rua dos Campos 1 ","919672347896","Porto","aguascenter@gmail.com","211543","www.aguascenter.pt","3","4","5","6");
        VaccinationCenter vaccinationCenter = new VaccinationCenter("Pedras","Rua dos Campos","919672347876","Porto","pedrascenter@gmail.com","21543","www.pedrascenter.pt","3","4","5","6");
        SNSUserDTO Maria1 = new SNSUserDTO("maria1", "FEMALE", "12/12/1940", "maia", "123456799","mariamaia1@gmail.com", "113412341", "11345678" );
        SNSUserDTO Mario = new SNSUserDTO("mario", "M", "12/12/1990", "maia", "123456999","mari0maia1@gmail.com", "111412341", "11145678" );

        VaccinationCenter vaccinationCenter2 = new VaccinationCenter("Porto","Rua dos Campos","919672347876","Porto","pedrascenter@gmail.com","21543","www.pedrascenter.pt","3","4","5","6");

        SNSUserDTO Maria = new SNSUserDTO("maria", "FEMALE", "12/12/1940", "maia", "123456789","mariamaia@gmail.com", "123412341", "12345678" );

        VaccineSchedule vaccineSchedule1 = new VaccineSchedule("123412341","Pedras","12/12/2022","12:22","COVID-19");
        VaccineSchedule vaccineSchedule2 = new VaccineSchedule("111412341","Pedras","12/10/2022","22:26","COVID-19");
        RecordVaccine recordVaccine = new RecordVaccine("19/05/2022","COVID-19","Astra","12356-74","111412341");
        Vaccine vac = new Vaccine("91301","Moderna","Moderna","COVID-19","10-85","2","1.5","25");
        Vaccine vac2 = new Vaccine("91302","Astra","Astra","COVID-19","10-85","1","1.5","25");



        this.company.getVaccinationCenterStore().addVaccinationCenter(vaccinationCenter);
        this.company.getWaitingRoomStore().addWaitingRoom();
        this.company.getRecoveryRoomStore().addRecoveryRoom();
        this.company.getRecordVaccineStore().addVaccinationCenter();

        // this.company.getWaitingRoomStore().getWaitingRoomList(0).add(Mario);

        this.company.getVaccinationCenterStore().addVaccinationCenter(vaccinationCenteraguas);
        this.company.getWaitingRoomStore().addWaitingRoom();
        this.company.getRecoveryRoomStore().addRecoveryRoom();
        this.company.getRecordVaccineStore().addVaccinationCenter();

        this.company.getVaccineScheduleStore().addVaccineSchedule(vaccineSchedule1);
        this.company.getVaccineScheduleStore().addVaccineSchedule(vaccineSchedule2);
        this.company.getRecordVaccineStore().addRecordVaccine(recordVaccine, 0);
        this.company.getVaccineStore().addVaccine(vac);
        this.company.getVaccineStore().addVaccine(vac2);

        this.company.getSNSUserDataStore().addSNSUser(Maria1);
        this.company.getSNSUserDataStore().addSNSUser(Mario);;

        this.company.getRecoveryRoomStore().addSNSUserToRecoveryRoom(Maria, 0);

        VaccineType vaccineType = new VaccineType("COVID-19");
        this.company.getVaccineTypesStore().addVaccineType(vaccineType);
    }

    // Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
    private static App singleton = null;
    public static App getInstance()
    {
        if(singleton == null)
        {
            synchronized(App.class)
            {
                singleton = new App();
            }
        }
        return singleton;
    }
}
