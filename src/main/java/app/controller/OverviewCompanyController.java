package app.controller;


import app.domain.model.Company;
import app.domain.model.VaccinationData;
import app.domain.store.VaccinationDataStore;
import app.subSequenceAlgorithm.Intervals;
import app.subSequenceAlgorithm.SubSequenceAlgorithm;
import org.apache.commons.lang3.StringUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * The type Overview company controller.
 */
public class OverviewCompanyController {
    private final String algorithm= "algorithm";
    private final String filePath ="config.properties";
    /**
     * The Company.
     */
    public final Company company;
    private Intervals intervals;
    private VaccinationDataStore vaccinationDataStore;
    private List<VaccinationData> vaccinationDataList = new ArrayList<>();

    /**
     * Gets instance of company
     */
    public OverviewCompanyController() {this(App.getInstance().getCompany());}

    /**
     * Contrutor
     *
     * @param company - instance of Company
     */
    public OverviewCompanyController(Company company) {
        this.company = company;
        this.vaccinationDataStore = company.getVaccinationDataStore();
    }

    /**
     * Get vaccination data list list.
     *
     * @param date the date
     * @return the list
     */
    public List<VaccinationData> getVaccinationDataList(String date){
        vaccinationDataList.clear();
        for(int i = 0; i < this.vaccinationDataStore.getDataList().size(); i++){
            if(date.equals(this.vaccinationDataStore.getDataList().get(i).getArrivalDate())){
                vaccinationDataList.add(vaccinationDataStore.getDataList().get(i));
            }
        }
        return vaccinationDataList;
    }


    /**
     * Set interval int [ ].
     *
     * @param minutes             the minutes
     * @param vaccinationDataList the vaccination data list
     * @return the int [ ]
     * @throws ParseException the parse exception
     */
    public int[] setInterval(String minutes, List <VaccinationData> vaccinationDataList) throws ParseException {
      Intervals interval = new Intervals(minutes,vaccinationDataList);
      int [] sequence = interval.createSequence(vaccinationDataList);
        System.out.println("************* Input List *************");
        System.out.print( " [ ");
      for ( int i = 0 ; i < sequence.length; i++) {
          System.out.print(sequence[i]);
          if (i != sequence.length-1) {
              System.out.print(", ");
          }
      }
        System.out.print(" ]");
        System.out.println();
      return sequence;
    }


    /**
     * Check date.
     *
     * @param date the date
     */
    public void checkDate(String date) {
        if (StringUtils.isBlank(date)) {
            throw new IllegalArgumentException("Date cannot be blank.");
        }
        if (!validateDate(date)) {
            throw new IllegalStateException("Insert a date following the example: i.e., 18/12/2022");
        }
    }

    /**
     * Validate date boolean.
     *
     * @param date the date
     * @return true if date is validated, false if not
     */
    public boolean validateDate(String date) {

        LocalDateTime ldt;
        String[] separated = date.split("/");
        if (separated.length==3) {
            if (separated[0].length()==1 && separated[1].length()==2) {
                date = "0" + separated[0] + "/" + separated[1]+"/"+separated[2];
            }
            if (separated[0].length()==2 && separated[1].length()==1) {
                date = separated[0] + "/0" + separated[1]+"/"+separated[2];
            }
            if (separated[0].length()==1 && separated[1].length()==1) {
                date = "0" + separated[0] + "/0" + separated[1]+"/"+separated[2];
            }
        }
        DateTimeFormatter fomatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        try {
            ldt = LocalDateTime.parse(date, fomatter);
            String result = ldt.format(fomatter);
            return result.equals(date);
        } catch (DateTimeParseException e) {
            try {
                LocalDate ld = LocalDate.parse(date, fomatter);
                String result = ld.format(fomatter);
                return result.equals(date);
            } catch (DateTimeParseException exp) {
                try {
                    LocalTime lt = LocalTime.parse(date, fomatter);
                    String result = lt.format(fomatter);
                    return result.equals(date);
                } catch (DateTimeParseException e2) {
                    //Debugging purposes
                    //e2.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * Sum int [ ].
     *
     * @param sequence the sequence
     * @return the int [ ]
     * @throws IOException               the io exception
     * @throws ClassNotFoundException    the class not found exception
     * @throws NoSuchMethodException     the no such method exception
     * @throws InvocationTargetException the invocation target exception
     * @throws InstantiationException    the instantiation exception
     * @throws IllegalAccessException    the illegal access exception
     */
    public int [] sum ( int [] sequence) throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
     Properties properties = new Properties();
     InputStream in = new FileInputStream(filePath);
     properties.load(in);
     Class <?> apiClass = Class.forName(properties.getProperty(algorithm));
     SubSequenceAlgorithm algorithm = (SubSequenceAlgorithm) apiClass.getDeclaredConstructor().newInstance();
     in.close();
     return algorithm.sum(sequence);
    }

    /**
     * Sum sequence int.
     *
     * @param sequence the sequence
     * @return the int
     */
    public int sumSequence (int [] sequence) {
        int sum = 0;
        for (int i = 0 ; i < sequence.length; i++) {
            sum = sum + sequence[i];
        }
        return sum;
    }

    /**
     * Gets first hour.
     *
     * @param sequence    the sequence
     * @param subsequence the subsequence
     * @param minutes     the minutes
     * @return the first hour
     * @throws ParseException the parse exception
     */
    public String getFirstHour (int [] sequence, int [] subsequence, int minutes) throws ParseException {
        int cont = 0;
        String hours = "08:00";
        for (int i = 0; i < sequence.length; i++){
                if (sequence[i] == subsequence [0]) {
                    for (int j = 0; j < cont; j++) {
                        hours = intervals.addXMinutes(hours, minutes);
                    }
                }
        }
        return hours;
    }

    /**
     * Gets last hour.
     *
     * @param subsequence the subsequence
     * @param minutes     the minutes
     * @param hours       the hours
     * @return the last hour
     * @throws ParseException the parse exception
     */
    public String getLastHour (int [] subsequence, int minutes, String hours) throws ParseException {
        for ( int cont = 0; cont < subsequence.length; cont++){
            Intervals intervals = new Intervals();
            hours = intervals.addXMinutes(hours,minutes);
        }
        return hours;
    }
}
