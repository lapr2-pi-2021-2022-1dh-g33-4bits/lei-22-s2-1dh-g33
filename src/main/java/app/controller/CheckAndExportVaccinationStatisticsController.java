package app.controller;

import app.domain.model.Company;
import app.domain.model.RecordVaccine;
import app.domain.model.VaccinationStatistic;
import app.domain.model.Vaccine;
import app.domain.store.RecordVaccineStore;
import app.domain.store.VaccinationHistoryStore;
import app.domain.store.VaccinationStatisticsStore;
import app.domain.store.VaccineStore;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * The type Check and export vaccination statistics controller.
 */
public class CheckAndExportVaccinationStatisticsController {

    private final Company company;
    private VaccinationHistoryStore vaccinationHistoryStore;
    private VaccinationStatisticsStore vaccinationStatisticsStore;
    private RecordVaccineStore recordVaccineStore;
    private VaccineStore vaccineStore;
    private List<RecordVaccine> recordVaccineList;
    private List<Vaccine> vaccineList;
    private List<VaccinationStatistic> vaccinationStatistics;
    private List<String> fullyVaccinatedDays = new ArrayList<>();

    private Date biggestDate;
    private Date smallestDate;
    private int vc;


    /**
     * Instantiates a new Check and export vaccination statistics controller.
     */
    public CheckAndExportVaccinationStatisticsController() { this(App.getInstance().getCompany()); }

    /**
     * Instantiates a new Check and export vaccination statistics controller.
     *
     * @param company the company
     */
    public CheckAndExportVaccinationStatisticsController(Company company) {
        this.company = company;
        this.vc = vc;
        this.vaccinationHistoryStore = this.company.getVaccinationHistoryStore();
        this.vaccinationStatisticsStore = this.company.getVaccinationStatisticsStore();
        this.recordVaccineStore = this.company.getRecordVaccineStore();
        this.vaccineStore = this.company.getVaccineStore();
    }

    /**
     * Check if date exists on record boolean.
     *
     * @param date the date
     * @param vc   the vc
     * @return the boolean
     * @throws ParseException the parse exception
     */
    public boolean checkIfDateExistsOnRecord (String date, int vc) throws ParseException {
        return !this.recordVaccineStore.checkIfDateExistsOnRecord(date, vc);
    }

    /**
     * Create vaccination statistics long.
     *
     * @param smallestDateSTRING the smallest date string
     * @param biggestDateSTRING  the biggest date string
     * @param vc                 the vc
     * @return the long
     * @throws ParseException the parse exception
     */
    public long createVaccinationStatistics (String smallestDateSTRING, String biggestDateSTRING, int vc) throws ParseException {
        vaccinationStatisticsStore.clearVaccinationStatistics();
        recordVaccineList = recordVaccineStore.getRecordVaccineList(vc);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate ld1 = LocalDate.parse(smallestDateSTRING, dateTimeFormatter);
        LocalDate ld2 = LocalDate.parse(biggestDateSTRING, dateTimeFormatter);
        long numberOfDays = ChronoUnit.DAYS.between(ld1, ld2);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();

        String date = smallestDateSTRING;

        for (int i=0; i <= numberOfDays; i++) {

            calendar.setTime(sdf.parse(date));
            if (i>0) {
                calendar.add(Calendar.DATE, 1);
            } else {
                calendar.add(Calendar.DATE, 0);
            }
            date = sdf.format(calendar.getTime());
            vaccinationStatisticsStore.addVaccinationStatisticsDay(vaccinationStatisticsStore.newVaccinationStatistic(date, getFullVaccinatedDay(date,vc)));
        }
        return numberOfDays;
    }

    /**
     * Gets full vaccinated day.
     *
     * @param date the date
     * @param vc   the vc
     * @return the full vaccinated day
     * @throws ParseException the parse exception
     */
    public String getFullVaccinatedDay (String date, int vc) throws ParseException {

        recordVaccineList = recordVaccineStore.getRecordVaccineList(vc);
        String snsUserNumber, vacName, doseNumber, lastDateVaccination = date;
        int fullyVaccinatedINT=0, administratedDoses=0, j;
        SimpleDateFormat sdformat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();

        for (int i=0; i < recordVaccineList.size(); i++) {

            if (Objects.equals(recordVaccineList.get(i).getDate(), date)) {
                snsUserNumber = recordVaccineList.get(i).getSnsUserNumber();
                vacName = recordVaccineList.get(i).getVacName();
                doseNumber = findRespectiveDoseNumber(vacName);

                if (checkIfIsLastDose(date, snsUserNumber, vacName, vc)) {
                    fullyVaccinatedINT++;
                }
            }
        }
        return String.valueOf(fullyVaccinatedINT);
    }

    /**
     * Check vaccination statistics.
     *
     * @param numberOfDays the number of days
     * @param initialDate  the initial date
     * @throws ParseException the parse exception
     */
    public void checkVaccinationStatistics (long numberOfDays, String initialDate) throws ParseException {
        vaccinationStatistics = this.company.getVaccinationStatisticsStore().getVaccinationStatisticsList();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();

        for (int j = 0; j <= numberOfDays; j++) {

            calendar.setTime(sdf.parse(initialDate));
            if (j>0) {
                calendar.add(Calendar.DATE, 1);
            } else {
                calendar.add(Calendar.DATE, 0);
            }
            initialDate = sdf.format(calendar.getTime());

            vaccinationStatisticsStore.checkVaccinationStatisticsToString(initialDate);
        }

    }

    /**
     * Export vaccination statistics.
     *
     * @param fileName     the file name
     * @param numberOfDays the number of days
     * @param initialDate  the initial date
     * @throws FileNotFoundException the file not found exception
     * @throws ParseException        the parse exception
     */
    public void exportVaccinationStatistics (String fileName, long numberOfDays, String initialDate) throws FileNotFoundException, ParseException {
        PrintWriter printWriter = new PrintWriter(fileName + ".txt");
        vaccinationStatistics = this.company.getVaccinationStatisticsStore().getVaccinationStatisticsList();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        printWriter.println("date; numberOfFullyVaccinatedUsers");

        for (int j = 0; j <= numberOfDays; j++) {

            calendar.setTime(sdf.parse(initialDate));
            if (j>0) {
                calendar.add(Calendar.DATE, 1);
            } else {
                calendar.add(Calendar.DATE, 0);
            }
            initialDate = sdf.format(calendar.getTime());

            vaccinationStatisticsStore.exportVaccinationStatisticsToString(initialDate, printWriter);
        }
        printWriter.close();
    }

    /**
     * Check if is last dose boolean.
     *
     * @param date          the date
     * @param snsUserNumber the sns user number
     * @param vacName       the vac name
     * @param vc            the vc
     * @return the boolean
     * @throws ParseException the parse exception
     */
    public boolean checkIfIsLastDose (String date, String snsUserNumber, String vacName, int vc) throws ParseException {
        recordVaccineList = this.company.getRecordVaccineStore().getRecordVaccineList(vc);
        SimpleDateFormat sdformat = new SimpleDateFormat("dd/MM/yyyy");

        for (int i=0; i < recordVaccineList.size(); i++) {
            Date d1 = sdformat.parse(date);
            Date d2 = sdformat.parse(recordVaccineList.get(i).getDate());

            if (d2.compareTo(d1) > 0 && Objects.equals(recordVaccineList.get(i).getVacName(), vacName) && Objects.equals(recordVaccineList.get(i).getSnsUserNumber(), snsUserNumber)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Find respective dose number string.
     *
     * @param vaccineName the vaccine name
     * @return the string
     */
    public String findRespectiveDoseNumber (String vaccineName) {
        vaccineList = vaccineStore.getVaccineList();
        for (int i = 0; i < vaccineList.size(); i++) {
            if (vaccineList.get(i).getName().equals(vaccineName)) {
                return vaccineList.get(i).getDoseNumber();
            }
        }
        return "0";
    }

}
