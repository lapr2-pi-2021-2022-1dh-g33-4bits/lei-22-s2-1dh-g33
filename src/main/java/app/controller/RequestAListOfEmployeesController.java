package app.controller;

import app.domain.model.Company;
import app.domain.model.Employee;
import app.domain.store.EmployeeStore;
import app.domain.store.OrganizationRolesStore;
import auth.domain.model.UserRole;

import java.io.PrintWriter;
import java.util.List;

public class RequestAListOfEmployeesController {
    private Company company;
    private OrganizationRolesStore rolesStore;
    private List<UserRole> organizationRoles;
    private EmployeeStore employeeStore;
    private List<Employee> employeeList;


    public RequestAListOfEmployeesController() {this(App.getInstance().getCompany());

    }

    public RequestAListOfEmployeesController (Company company) {this.company = company;}


    /**
     * Gets the list organizationRoles stored in the OrganizationRolesStore
     *
     * @return - A list with existent roles
     */

    public List<UserRole> getOrganizationRoles(){
        this.rolesStore = this.company.getOrganizationRolesStore();
        this.organizationRoles = this.rolesStore.getOrganizationRolesList();
        return organizationRoles;
    }

    /**
     * Gets the id from the organizationRoles list stored in the OrganizaitonRolesStore
     *
     * @param position - Position of the list from which we want the role id
     * @return - Role id in form of a string
     */

    public String getRoles(int position){ return organizationRoles.get(position).getId();}

    /**
     * Gets the list EmployeeList stored in the EmployeeStore
     *
     * @return - A list with existent Employee
     */

    public List<Employee> getEmployeeList(){
        this.employeeStore = this.company.getEmployeeStore();
        this.employeeList = this.employeeStore.getEmployeeStore();
        return employeeList;
    }

    /**
     * Creates a list with the employees with a specific role
     */

    public int searchEmployeesRoles(String employeeRole, List<Employee> list, List<String> fill) {
        int cont1=0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getRole().equals(employeeRole)) {
                fill.add("Name: " + list.get(i).getName() + " | Adress: " + list.get(i).getAddress() + " | Email Adress: " + list.get(i).getEmailAddress() + " | Phone Number: " + list.get(i).getPhoneNumber() + " | SocCode: " + list.get(i).getSocCode() + " | ID: " + list.get(i).getId() + " | NIF: " + list.get(i).getNIF());
                cont1++;
            }
        }
        return cont1;
    }

    /**
     * Saves the list in a text file
     */

    public void saveList (List<String> list, PrintWriter print, String employeeRole) {
        print.println("List of " + employeeRole);
        for (int i = 0; i < list.size(); i++) {
            print.println(list.get(i));
        }
    }
}