package app.controller;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;

public class SendConfirmationMessageController {

    public static final String ACCOUNT_SID = ("AC6161a9a6fd8d13b5744e095535023fdf");
    public static final String AUTH_TOKEN = ("437b5516092db339882cf44f4e3d5345");

    public void sendMessage (String name, String snsUserNumber, String vaccinationCenterName, String date, String time, String vacType) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(
                        new com.twilio.type.PhoneNumber("+351934552781"),
                        new com.twilio.type.PhoneNumber("+12517147305"),
                        "Dear " + name + ", here are your vaccination schedule details. SNS User Number: "+ snsUserNumber + " Vaccination Center: " + vaccinationCenterName + " Date: " + date + " Time: " + time + " Vaccine type: " + vacType)

                .create();
    }
}