package app.controller;
import app.mappers.dto.SNSUserDTO;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;

public class SendRecoveryPeriodMessageController {

    public static final String ACCOUNT_SID = ("AC6161a9a6fd8d13b5744e095535023fdf");
    public static final String AUTH_TOKEN = ("437b5516092db339882cf44f4e3d5345");

    public void sendMessage (String vacType, String vacName, String lotNumber, SNSUserDTO snsUserDTO) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(
                        new com.twilio.type.PhoneNumber("+351934552781"),
                        new com.twilio.type.PhoneNumber("+12517147305"),
                        "Dear " + snsUserDTO.getName() + ", your recovery period as ended, you can leave the vaccination center. Here are your vaccination details: Vaccine Type: " + vacType + " Vaccine Name/Brand: "+ vacName + " Lot Number: " + lotNumber)
                .create();
    }
}