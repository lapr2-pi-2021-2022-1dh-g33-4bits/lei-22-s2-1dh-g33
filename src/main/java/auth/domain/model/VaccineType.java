package auth.domain.model;


import org.apache.commons.lang3.StringUtils;

import java.util.Objects;


public class VaccineType {
    private String type;

    public VaccineType(String type) {
        if (!StringUtils.isBlank(type)) {
            this.type = this.extract(type);
        } else {
            throw new IllegalArgumentException("Vaccine type cannot be blank");
        }
    }

    private String extract (String type) {
        return type.trim().toUpperCase();
    }

    public String getType() {
        return this.type;
    }

}
