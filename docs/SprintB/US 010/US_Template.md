03# US XXX - XXXX XXXX

## 1. Requirements Engineering

*In this section, it is suggested to capture the requirement description and specifications as provided by the client as well as any further clarification on it. It is also suggested to capture the requirements acceptance criteria and existing dependencies to other requirements. At last, identify the involved input and output data and depicted an Actor-System interaction in order to fulfill the requirement.*


### 1.1. User Story Description

*As an administrator, I want to register an Employee.*

### 1.2. Customer Specifications and Clarifications 

* From the client clarifications:
 > **Question:** If the role is not an employee attribute, how can I know which role should the employee take? Does the user specify which role will he take or is there another way to determine it?
 > **Answer:** No response yet.
 >
 > **Link:** https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16094#p20672
 > 
 > **Question:** Besides a password and a user name, what other (if any) information should the Admin use to register a new employee? Are any of them optional?
 > **Answer:** Every Employee has only one role (Coordinator, Receptionist, Nurse).
 Employee attributes: Id (automatic), Name, address, phone number, e-mail and Citizen Card number.
 All attributes are mandatory.
 >
 > **Link:** https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=15696#p20326
 >
 > **Question:** You have already specified that the password for a user should be automatically generated. Is it the same case for an employee? How will the system user know their password, via email or a different way?
 > **Answer:** 1-Yes. I already answered this question. 2- I will not answer this question during Sprint B. I will not answer questions that require knowledge that will be introduced later, in the next sprints.
 > 
 > **Link:** https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=15982#p20526
 >
 > **Question:** [1] What is the correct format for the employee's phone number and cc? Should we consider that these follow the portuguese format? [2] Is the password generated automatically, or is it specified by the user operating the system?"
 > **Answer:** 1- Consider that these two attributes follow the portuguese format; 2- The password should be generated automatically.
 >
 > **Link:** https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16041#p20608
 >
 > **Question:** [1] What is the correct format for the employee's phone number and cc? Should we consider that these follow the portuguese format? [2] Is the password generated automatically, or is it specified by the user operating the system?"
 > **Answer:** 1- Consider that these two attributes follow the portuguese format; 2- The password should be generated automatically.
 >
 > **Link:** https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16041#p20608
  

### 1.3. Acceptance Criteria

*Each user must have a single role defined in the system. The "auth" component available on the repository must be reused (without modifications).*

### 1.4. Found out Dependencies

*There aren't any dependencies so far*

### 1.5 Input and Output Data
Input Data:
* Typed data: 
 > Name. 
 > Address.
 > Email Address.
 > Standard occupational classification (SOC) code.  
 > Phone Number.
 > NIF 
 > AdminIndexNumber (occasionally). *

* Selected data 
 > Organization Role.

Output Data:
 > (In)Success of the operation. 
### 1.6. System Sequence Diagram (SSD)

*Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered.*

![US10_SSD](US10_SSD.svg)


### 1.7 Other Relevant Remarks

*Special requirements
>- Only the administrator has the attribute "Administrator Index Number".
>
>- An administrator needs to be registered when the application starts for first time.
>
>- Name: A string with no more than 20 characters.
>
>- Address: A string with no more than 30 characters.
>
>- Phone Number: 11-digits number.
>
>- SOC code: 7 characters string (00-0000).
>
>- Id and Administrator Index Number: 7-digits number. 


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 
*In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement.* 

![US10_MD](US10_MD.svg)

### 2.2. Other Remarks

**Organization Roles**
>- Receptionist
>
>- Nurse
>
>- Center coordinator
>
>- SNS User
>
>- Administrator 



## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID                                         | Question: Which class is responsible for...     | Answer                              | Justification (with patterns)                                                                          |
|:-------------------------------------------------------|:------------------------------------------------|:------------------------------------|:-------------------------------------------------------------------------------------------------------|
| Step 1 | ... interacting with the administrator?	        | CreateEmployeeUI							             |Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model.             |                                                                                                        |
| Step 2 | ... coordinating the US? 		                     | 	CreateEmployeeController						     |Controller.             |                                                                                                        |
| Step 3 | ... knowing the available roles? 		             | 	OrganizationRolesStore						       | IE: knows all roles.            |                                                                                                        |
| Step 4 | ...saving the inputted data? 		                 | 	EmployeeStore						                | IE: object created has its own data.            |                                                                    |
| Step 5 | ...saving the inputted data? 		                 | 	EmployeeStore						                | IE: object created has its own data.            |                                                                    |
| Step 6 | ... validating all data (local validation)? 		  | 	Employee (and Administrator)						 | IE: owns its data.            |                                                                                                        |              
| Step 7 | ... validating all data (global validation)? 		 | 	EmployeeStore						                | IE: knows all employees.            |                                                                                |
| Step 8 | ... saving the created employee? 		             | 	EmployeeStore						                | IE: stores all employees            |                                                                                |
| Step 9 | ... informing operation success? 		             | 	CreateEmployeeUI						             | IE: is responsible for user interactions.            |                                                               |



### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Employee
 * Administrator
 * OrganizationRolesStore

Other software classes (i.e. Pure Fabrication) identified: 
 * createEmployeeUI  
 * createEmployeeController
 * EmployeeStore

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.* 

![US10_SD](US10_SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US10_CD](US10_CD.svg)

# 4. Tests 
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.* 

# 6. Integration and Demo 

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





