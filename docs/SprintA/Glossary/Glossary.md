# Glossary

| **_TEA_** (EN)  | **_TEA_** (PT) | **_Description_** (EN)                                           |                                       
|:------------------------|:-----------------|:--------------------------------------------|
| **Administrator** | **Administrador** | Provides office support to either an individual or team and is vital for the smooth-running of a business. |
| **AGES** | **AGES** | Acronym that means Health Center Groupings. |
| **Algorithm** | **Algoritmo** | Set of well-defined instructions designed to perform a specific task. |
| **Alphanumeric Character** | **Caracter Alfanumérico** |A term that encompasses all of the letters and numbers in a given language set.|
| **Application** | **Aplicação** | Set of programs and data structures that execute tasks and ensure necessary operating features.|
| **ARS** | **ARS** | Acronym that means Health Regional Administration. |
| **Brute-force Algorithm** | **Algoritmo de força bruta** | Brute-force, also known as generate and test, is a very general problem-solving technique and algorithmic paradigm that consists of systematically enumerating all possible candidates for the solution and checking whether each candidate satisfies the problem's statement. |
| **Client** | **Cliente** | A person or organization using the services of a professional person or company. |
| **Covid-19** | **Covid-19** | Coronavirus disease (COVID-19) is an infectious disease caused by the SARS-CoV-2 virus. |
| **DGS** | **DGS** | Acronym of The Directorate-General for Health or DGS is the health authority of the Portuguese government, which functions as a service of the Ministry of Health.|
| **E-mail** | **E-mail** | Messages distributed by electronic means from one computer user to one or more recipients via a network. |
| **EU COVID Digital Certificate** | **Certificado Digital Covid EU** | A digital document proving that a person has a full vaccination against COVID-19 |
| **FIFO** | **FIFO** | Acronym that stands for a low-overhead algorithm (first-in, first-out) that requires little bookkeeping on the part of the operating system. |
| **Healthcare Center** | **Centro de Saúde** | Community-based and patient-directed organizations that deliver comprehensive, culturally competent, high-quality primary health care services to the nation's most vulnerable individuals and families.|
| **IntelliJ IDEA** | **IntelliJ IDEA** |IntelliJ IDEA is an integrated development environment (IDE) written in Java for developing computer software.  |
| **Immunization**|**Imunização**| A process by which a person becomes protected against a disease through vaccination.|
| **JavaFX** | **JavaFX** | A software platform for creating and delivering desktop applications, as well as rich web applications that can run across a wide variety of devices.|
| **JUnit** | **JUnit** | JUnit is a unit testing framework for the Java programming language.|
| **Pandemic**| **Pandemia** | An outbreak of a disease that occurs over a wide geographic area (such as multiple countries or continents) and typically affects a significant proportion of the population. |
| **Population**|**População**| All the inhabitants of a particular place.|
| **Notification** | **Notificação** | A message that is automatically sent to your mobile phone or computer. |
| **Nurse** | **Enfermeira** | A person trained to care for the sick or infirm, especially in a hospital. |
| **Receptionist** | **Rececionista** | A receptionist is an employee taking an office or administrative support position. |
| **Recovery Period**|**Período de Recuperação**|The later stage of an infectious disease or illness when the patient recovers and returns to previous health. |
| **Report** | **Relatório** | Short, sharp and concise document which is written for a particular purpose and audience. |
| **SMS** | **SMS** | Acronym that means short message service or SMS is a service available on digital cell phones that allows the sending of short messages between these devices and other handheld devices, and even between landlines, popularly known as text messages. |
| **SNS** | **SNS** | Acronym of The National Health Service or SNS is a structure through which the Portuguese State guarantees the right to health for all citizens of Portugal. |
| **SVG Format** | **Formato SVG** | A web-friendly vector file format.|
| **User**|**Utilizador**| A person who uses or operates something.|
| **User Manual**| **Manual de Utilizador**| A user manual is intended to assist users in using a particular product, service or application. It's usually written by a technician, product developer, or a company's customer service staff.|
| **The JaCoCo Plugin** | **JaCoCo Plugin** |The JaCoCo plugin adds a JacocoTaskExtension extension to all tasks of type Test.|
| **Vaccine** | **Vacina** | A substance used to stimulate the production of antibodies and provide immunity against one or several diseases. |
| **Vaccination** | **Vacinação** | Treatment with a vaccine to produce immunity against a disease. |
| **Vaccination Center** | **Centro de Vacinação** | A place where people get vaccinated by nurses or health assistants.  |
| **Vaccination Center Coordinator** | **Coordenador do Centro de Vacinação** | A person that's responsible for ensuring all vaccines are stored and handled correctly.|
