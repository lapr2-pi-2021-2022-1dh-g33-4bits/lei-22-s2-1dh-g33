# Supplementary Specification (FURPS+)

## Functionality

_Specifies functionalities that:_

- _are common across several US/UC;_
- _are not related to US/UC, namely: Audit, Reporting and Security._

- Localization
  > "The application must support, at least, the Portuguese and the English languages."

- E-mail
  > "The system should be able to notify (eg.:SMS or email) the user that his/her recovery periodod has ended."

- Help
  > "The time complexity analysis of the algorithms should be properly documented in the application user manual (in the annexes) that must be delivered with the application".
  
- Security 
  > "All those who wish to use the application must be authenticated with a password holding seven alphanumeric characters, including three capital letters and two digits".

 
## Usability 

_Evaluates the user interface. It has several subcategories,
among them: error prevention; interface aesthetics and design; help and
documentation; consistency and standards._



## Reliability
_Refers to the integrity, compliance and interoperability of the software. The requirements to be considered are: frequency and severity of failure, possibility of recovery, possibility of prediction, accuracy, average time between failures._



## Performance
_Evaluates the performance requirements of the software, namely: response time, start-up time, recovery time, memory consumption, CPU usage, load capacity and application availability._



## Supportability
 
 - testability 
   > " The unit testes should be implemented using the JUnit 5 framework "
   




## +

### Design Constraints

_Specifies or constraints the system design process. Examples may include: programming languages, software process, mandatory standards/patterns, use of development tools, class library, etc._
  
    > " The application graphical interface is to be developed in JavaFX 11. " 


### Implementation Constraints

_Specifies or constraints the code or construction of a system such
such as: mandatory standards/patterns, implementation languages,
database integrity, resource limits, operating system._

    > " The application must be developed in Java language using the IntelliJ IDE or NetBeans. "
    > " (...) adopt best practices for identifying requirements, and for OO software analysis and design (...) "
    > " (...) adopt recognized coding standards (e.g.,CamelCase) (...)" 
    > " (...) use Javadoc to generate useful documentation for Java code.(..)" 
    > " The development team must implement unit tests for all methods, except for methods that implement Input/Output operations. "
    > " The unit tests should be implemented using the JUnit 5 framework. "
    > " The JaCoCo plugin should be used to generate the coverage report. "
    > "The application should use object serialization to ensure data persistence between two runs of the application."


### Interface Constraints
_Specifies or constraints the features inherent to the interaction of the
system being developed with other external systems._


    > "All the images/figures produced during the software development process should be recorded in SVG format." 

### Physical Constraints

_Specifies a limitation or physical requirement regarding the hardware used to house the system, as for example: material, shape, size or weight._
