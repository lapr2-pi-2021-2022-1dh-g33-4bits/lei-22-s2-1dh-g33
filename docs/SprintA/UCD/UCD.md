# Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US).**

However, be aware, this is a pedagogical simplification. On further projects and curricular units might also exist _1 to N **and/or** N to 1 relationships between US and UC.

**Use Case Diagram**

![Use Case Diagram](Use Case Diagram.svg)


**For each UC/US, it must be provided evidences of applying main activities of the software development process (requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a link as suggested below.**

# Use Cases / User Stories
| UC/US  | Description                                                               |                   
|:----|:------------------------------------------------------------------------|
| US 001 | As a User, I want to schedule my vaccine. (US001.md)|
| US 002 | As a User, I want to request the vaccination certificate. (US002.md) |
| US 003 | As a Receptionist, I want to register the arrival of a user. (US003.md)|
| US 004 | As a Receptionist, I want to confirm the scheduling date. (US004.md)|
| US 005 | As a Receptionist, I want to help older people schedule their vaccine. (US005.md)|
| US 006 | As a Nurse, I want to check the present users. (US006.md)|
| US 007 | As a Nurse, I want to check the user's info. (US007.md)|
| US 008 | As a Nurse, I want to register a user's vaccination data(register the event). (US008.md)|
| US 009 | As a Nurse, I want to register adverse reactions. (US009.md)|
| US 010 | As a Center Coordinator, I want to monitor the vaccination process. (US010.md)|
| US 011 | As a Center Coordinator, I want to evaluate the vaccination performance. (US011.md)|
| US 012 | As a Center Coordinator, I want to consult statistics and charts. (US012.md)|
| US 013 | As a Center Coordinator, I want to generate reports. (US013.md)|
| US 014 | As a Center Coordinator, I want to analyze data from other centers. (US014.md)|
| US 015 | As an Administrator, I want to register centers. (US015.md) |
| US 016 | As an Administrator, I want to register SNS users. (US016.md) |
| US 017 | As an Administrator, I want to register employees. (US017.md) |
| US 018 | As an Administrator, I want to consult the list of employees. (US018.md) |
| US 019 | As an Administrator, I want to register vaccines. (US019.md) |
| US 020 | As an Administrator, I want to register vaccine types. (US020.md) |

| ... | ...|
| US 326 | [ShortNameOfUS326](US326.md)|

