# Use Case Diagram (UCD)

**In the scope of this project, there is a direct relationship of _1 to 1_ between Use Cases (UC) and User Stories (US).**

However, be aware, this is a pedagogical simplification. On further projects and curricular units might also exist _1 to N **and/or** N to 1 relationships between US and UC.

**Insert below the Use Case Diagram in a SVG format**

![Use Case Diagram](UCD.svg)


**For each UC/US, it must be provided evidences of applying main activities of the software development process (requirements, analysis, design, tests and code). Gather those evidences on a separate file for each UC/US and set up a link as suggested below.**

# Use Cases / User Stories
| UC/US  | Description                                                               |                   
|:----|:------------------------------------------------------------------------|
| US 001 | As a nurse, I want to issue and deliver the vaccination certificate. (US001.md) |
| US 002 | As an administrator, I want to configurate and manage the core information required for this application. (US002.md) |
| US 003 | As a user, I want to schedule my vaccination. (US003.md)|
| US 004 | As a receptionist, I want to register the arrival of a user. (US004.md)|
| US 005 | As a nurse, I want to consult the application data. (US005.md)|
| US 006 | As a nurse, I want to register a user's vaccination data. (US006.md)|
| US 007 | As a Center Coordinator, I want to manage the Covid- 19 vaccination process (US007.md)|
| US 008 | As an Administrator, I want to administer the application. (US008.md) |
| ... | ...|
| US 326 | [ShortNameOfUS326](US326.md)|
